var fadeStart=400; // 100px scroll or less will equiv to 1 opacity
var fadeUntil=800; // 200px scroll or more will equiv to 0 opacity
var fading = $('.fading');


$(window).bind('scroll', function(){
    var offset = $(document).scrollTop();
    var opacity=0;
        
    if( offset<=fadeStart ){
        opacity=1;
    }else if( offset<=fadeUntil ){
        opacity=1-offset/fadeUntil;
    }
    fading.css('opacity',opacity);
});