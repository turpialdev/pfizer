<div  class="container-fluid padding-vacunas-interno">
       
            
                <!-- <h1>HEPATITIS B</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>hepatitis B</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> La hepatitis B es una infección del hígado causada por un virus.

                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h4>¿De qué manera la hepatitis B puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                              En general, la hepatitis B no presenta síntomas.
                            </li>
                            <li>
                               Cuando se presentan síntomas, estos incluyen:
                               <ul>
                                    <li>Dolor de estómago</li>
                                    <li>Pérdida de apetito</li>
                                    <li>Náuseas y vómitos</li>
                                    <li>Color amarillento de la piel y la parte blanca de los ojos</li>
                               </ul>
                            </li>
                            <li>En muchas personas, la hepatitis B es una infección crónica que dura toda la vida. </li>
                            <li>A veces, la hepatitis B puede ocasionar cáncer de hígado y otras enfermedades hepáticas varios años después de la infección.</li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La hepatitis B se transmite a través del contacto con la sangre y otros líquidos corporales.</li>
                            <li>La hepatitis B también se puede transmitir de una madre embarazada a su bebé en gestación.</li>
                            <li>Las personas con hepatitis B pueden transmitir el virusaunque no presenten síntomas.</li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
