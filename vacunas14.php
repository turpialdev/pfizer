<div  class="container-fluid padding-vacunas-interno">
     
            
                <!-- <h1>TÉTANOS</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>tétanos</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> El tétanos es una infección bacteriana que también se denomina “trismo”.
                            </li>
                            <li>
                                La bacteria que causa el tétanos libera una fuerte toxina que provoca síntomas neurológicos (del sistema nervioso).
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h4>¿De qué manera el tétanos puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                            El tétanos ataca los músculos, la médula espinal y el cerebro, y puede ser mortal.
                            </li>
                            <li>Los síntomas incluyen rigidez muscular, espasmos, fiebre, dificultad para respirar, pulso acelerado y cambios en la presión 
                                arterial.</li>
                            <li>La recuperación del tétanos puede llevar meses y requerir estadías prolongadas en el hospital. </li>
                                
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                           <li>Una persona puede contraer tétanos a través de una herida en la piel, por lo general, una 
                            herida punzante causada por clavos de construcción, alambre de púas o mordidas de animales.
                            </li>
                            <li>
                                El tétanos no se puede transmitir directamente de persona a persona.
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
