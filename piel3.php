<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    
    <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/piel-banner.jpg" >
     <?php include 'mainnav.php' ?>
        <div class="container title">
         <h1 class="heading-interno">PIEL Y ARTICULACIONES</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
<div  class="container-fluid white pad-30">
        <div id="vida"></div>
        <div class="container">
            <ol class="breadcrumb">
            <li><a href="piel.php">Piel y Articulaciones</a></li>
            <li class="active">Espondilitis Anquilosante</li>
          </ol>
                <!-- <h1>DIFTERIA</h1> -->
            <div class="row info">
                <div class="col-md-6 que-es-vacunas justificar menos_espacio">
                    <h2>¿Qué es la Espondilitis Anquilosante?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        La EA es un trastorno inflamatorio crónico y doloroso que afecta principalmente la columna vertebral. A menudo existe 
                        rigidez gradual y disminución del movimiento de la columna vertebral. Algunas veces se encuentran afectadas otras áreas, 
                        como los hombros, caderas y rodillas. En la EA su sistema inmune no funciona correctamente. Las células del sistema inmune 
                        atacan a las articulaciones entre los huesos de su columna vertebral (vértebras) y aquellas entre su columna y la pelvis, 
                        llamadas articulaciones sacroilíacas.
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
               <div class="col-md-6">
                    <img class="img-2" src="img/piel-espondilitis-interior.jpg" />
                </div>
            </div>
           
            <div class="row info hidden-xs">
              <div class="col-md-12">
                    <div class="col-md-5">
                       <div class="row">
                          <div class= "col-md-7 botonera boton_piel21 pf-on">
                            <figure>
                              <img src="img/medical50.png" width="100px" height="100px" />
                            </figure>
                          </div>
                            <div class= "col-md-7 botonera boton_piel22 apagado pf-off">
                            <figure>
                              <img src="img/medical14.png" width="100px" height="100px" />
                            </figure>
                          </div>
                        </div>
                        <div class="row">
                          <div class= "col-md-7 botonera boton_piel23 apagado pf-off">
                            <figure>
                              <img src="img/medical51.png" width="100px" height="100px" />
                            </figure>
                          </div>
                          <div class= "col-md-7 botonera boton_piel24 apagado pf-off">
                            <figure>
                              <img src="img/question1.png" width="100px" height="100px" />
                            </figure>
                          </div>
                        </div>
                  </div>
                  <div class="col-md-6 piel_info info_piel1">
                    <h2>Causas de la Espondilitis Anquilosante</h2>
                    <p>Se desconocen las causas. Nadie sabe por qué el sistema inmune ataca las células de las articulaciones. Un gen particular puede 
                      hacer que algunas personas tengan más posibilidades de padecer EA.</p>
                    <p>
                      Algunas cosas que sabemos bien son:
                      <ul>
                        <li>La genética tiene un papel clave en la EA. Para muchas personas, un gen particular puede hacerlas más propensas a desarrollar 
                          esta enfermedad.</li>
                        <li>Las investigaciones han sugerido que, en las personas propensas a EA las bacterias u otros agentes infecciosos pueden ser 
                          participantes importantes en la actividad de la enfermedad.</li>
                      </ul>
                    </p>
                  </div>
                  <div class="col-md-6 piel_info info_piel2">
                    <h2>Síntomas característicos</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Normalmente, las personas con EA el dolor y el primer aviso de rigidez son en la parte inferior de la espalda y las nalgas, 
                       causado por la artritis en las articulaciones sacroilíacas. Los síntomas generalmente son peor por las mañanas y durante la 
                       noche, y la rigidez puede extenderse hasta la columna vertebral y a veces hasta el cuello.
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6 piel_info info_piel3">
                    <h2>¿Cómo se realiza el diagnóstico?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Para determinar si usted tiene EA, su médico Ie preguntará acerca de sus antecedentes médicos y familiares y sobre sus síntomas 
                       actuales. También si presenta dolor y rigidez que:
                       <ul>
                         <li>Haya durado varios meses.</li>
                         <li>Tome tiempo en aliviarse después de que se despierte en la mañana.</li>
                         <li>Disminuye cuando usted hace ejercicio o con otra actividad física.</li>
                         <li>Disminuye cuando usted toma medicamentos antiinflamatorios no esteroideos (AINEs).</li>
                       </ul>
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6 piel_info info_piel4">
                    <h2>¿Quién puede padecerla?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Parece ser más frecuente en los hombres. Por lo general comienza en las personas entre los 20 y 30 años. Durante mucho tiempo, 
                       los investigadores sospecharon que tiene un componente hereditario ya que alrededor del 20% de las personas con EA también tienen 
                       un familiar con la enfermedad.
                    </p>
                   
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
          </div>
             <div class="visible-xs">
            <div class="boton-wide boton_piel21">
              <figure>
                <img src="img/medical50.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                <h2>Causas de la Espondilitis Anquilosante</h2>
              <p>Se desconocen las causas. Nadie sabe por qué el sistema inmune ataca las células de las articulaciones. Un gen particular puede 
                hacer que algunas personas tengan más posibilidades de padecer EA.</p>
              <p>
                Algunas cosas que sabemos bien son:
                <ul>
                  <li>La genética tiene un papel clave en la EA. Para muchas personas, un gen particular puede hacerlas más propensas a desarrollar 
                    esta enfermedad.</li>
                  <li>Las investigaciones han sugerido que, en las personas propensas a EA las bacterias u otros agentes infecciosos pueden ser 
                    participantes importantes en la actividad de la enfermedad.</li>
                </ul>
              </p>
              </div>
            </div>
            <div class="boton-wide boton_piel22">
              <figure>
                <img src="img/medical14.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
               <h2>Síntomas característicos</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Normalmente, las personas con EA el dolor y el primer aviso de rigidez son en la parte inferior de la espalda y las nalgas, 
                       causado por la artritis en las articulaciones sacroilíacas. Los síntomas generalmente son peor por las mañanas y durante la 
                       noche, y la rigidez puede extenderse hasta la columna vertebral y a veces hasta el cuello.
                    </p>
                    
              </div>
            </div>
            <div class="boton-wide boton_piel23">
              <figure>
                <img src="img/medical51.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                 <h2>¿Cómo se realiza el diagnóstico?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Para determinar si usted tiene EA, su médico Ie preguntará acerca de sus antecedentes médicos y familiares y sobre sus síntomas 
                       actuales. También si presenta dolor y rigidez que:
                       <ul>
                         <li>Haya durado varios meses.</li>
                         <li>Tome tiempo en aliviarse después de que se despierte en la mañana.</li>
                         <li>Disminuye cuando usted hace ejercicio o con otra actividad física.</li>
                         <li>Disminuye cuando usted toma medicamentos antiinflamatorios no esteroideos (AINEs).</li>
                       </ul>
                      </p>
              </div>
            </div>
            <div class="boton-wide boton_piel24">
              <figure>
                <img src="img/question1.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                 <h2>¿Quién puede padecerla?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Parece ser más frecuente en los hombres. Por lo general comienza en las personas entre los 20 y 30 años. Durante mucho tiempo, 
                       los investigadores sospecharon que tiene un componente hereditario ya que alrededor del 20% de las personas con EA también tienen 
                       un familiar con la enfermedad.
                    </p>
              </div>
            </div>
            
        </div>
        <br>
             <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>Pruebas diagnósticas</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Su médico también deberá hacerle una exploración física completa. 
                    </p>
                    <p>Esta incluye el examen de las áreas donde usted dice que presenta </br>
                      dolor a inflamación, determinando 
                      que tanto movimiento de columna</br>
                      tiene usted y observando que tanto puede usted expandir el tórax.</p>
                    <p>Además, su médico puede realizar pruebas diagnósticas que incluyen:
                      <ul>
                        <li>Pruebas en sangre.</li>
                        <li>Rayos X. Se toman radiografías de la espalda para determinar si</br>
                         las articulaciones muestran signos de daño.</li>
                      </ul>
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
        </div>
    </div>
        <?php include 'footer.php' ?>
   
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>
 <script>
     $( ".boton_piel21" ).on( "click", function() {
         $('.info_piel1').css('display', 'block');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'none');
       
         
         
      });
     $( ".boton_piel22" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'block');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'none');
        
         
      });
     $( ".boton_piel23" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'block');
         $('.info_piel4').css('display', 'none');
         
         
      });
    $( ".boton_piel24" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'block');
         
         
      });
   

    </script>
<script>
  $(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            interval: false
        });
    });
});​
</script>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>

<script>
$(document).ready(function(){

    $('.carousel-indicators li').on('click', function(){

        $('.carousel-indicators li').each(function(){

            $(this).removeClass("apagado");

        });

        $(this).addClass("apagado");
    });

    $( "body" ).on('click', '.pf-off', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on").addClass( "apagado" );

        $(this).removeClass( "pf-off" );
        $(".pf-on").addClass( "pf-off" );
        $(".pf-on").removeClass( "pf-on" );
        $(this).addClass( "pf-on" );

    });
});



</script>
</body>
</html>