<div  class="container-fluid padding-vacunas-interno">
      
                <!-- <h1>INFLUENZA (GRIPE)</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>¿Qué es la influenza?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> La influenza (la gripe) es una infección viral sumamente contagiosa que afecta la nariz, la garganta y los pulmones.
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h2>¿De qué manera la gripe puede afectar a mi hijo?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                              Los síntomas de la influenza incluyen fiebre (a veces lo suficientemente alta como para provocar convulsiones), dolores 
                             musculares, pérdida de apetito, cansancio extremo y dolor de cabeza.
                            </li>
                            <li>
                              Algunos casos de influenza son graves y pueden causar neumonía o la muerte.
                            </li>
                            <li>Los niños pequeños tienen mayor probabilidad de ser hospitalizados por gripe que los niños de mayor edad. </li>
                            
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h2>¿Mi hijo está en riesgo?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h2>¿Cómo se transmite?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La gripe se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                            <li>También se puede transmitir al tocar superficies contaminadas con el virus. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
