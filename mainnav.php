﻿<nav class="navbar navbar-default navbar-fixed-top navbar-custom" id="mainnav" name="mainnav">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">
        Pfizer Conmigo
     </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul id="mainnav" class="nav navbar-nav navbar-right ">
        <li class="dropdown dropdown-main "><a class="link_scroll" href="#sintomas">¿Cómo me siento?</a>
                        <ul class="dropdown-menu dropdown-main-menu ">
                            <li><a href="cardio.php">Cardiología</a></li>
                            <li><a href="dolor.php">Dolor</a></li>
                            <li><a href="piel.php">Piel y Articulaciones</a></li>
                            <li><a href="nervioso.php">Sistema Nervioso</a></li>
                            <li><a href="vacunas.php">Vacunas</a></li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-main"><a class="link_scroll" href="#vida">Vida Saludable</a>
                        <ul class="dropdown-menu dropdown-main-menu">
                            <li><a href="nutricion.php">Nutrición</a></li>
                            <li><a href="#">Tu médico te habla</a></li>
                            <li><a href="ejercicios.php">Ejercicios</a></li>
                            
                        </ul>
                    </li>
                    <li><a href="#">Noticias</a></li>
      </ul>
     
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>