<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    <?php include 'mainnav.php' ?>
    <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/nervioso-banner.jpg" >
    <?php include 'mainnav.php' ?> 
     
        <div class="container title">
         <h1 class="heading-interno">SISTEMA NERVIOSO CENTRAL</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
<div  class="white pad-30">
        <div id="vida"></div>
        <div class="container">
          <ol class="breadcrumb">
            <li><a href="#">Sistema Nervioso Central</a></li>
            <li class="active">Depresión</li>
          </ol>
                          <!-- <h1>DIFTERIA</h1> -->
            <div class="row info">
              <div class="col-md-12">
              <h2>¿Cómo saber si se trata de depresión o es algo pasajero?</h2>
            </div>
                <div class="col-md-6 justificar menos_espacio "></br>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                      Ocasionalmente todos tenemos un mal día. Un caso de tristeza. Digamos que tiene problemas 
                      con su pareja, su negocio falla y pierde mucho dinero, ha perdido un ser querido o le diagnostican alguna enfermedad. 
                    </p>
                    <p>
                       Es perfectamente compresible sentirse tenso, triste, abrumado o enojado. Eventualmente, la mayoría de las personas se adaptan 
                       a las consecuencias de estos factores estresantes de la vida, pero algunos no. Ese sentimiento de que “todo está saliendo mal” 
                       lo mantiene al borde de las lágrimas. Desafortunadamente, a veces ese pensamiento persiste y podría indicar una enfermedad 
                       seria.
                    </p>
                    <p>Podría ser de ayuda para usted comprender que la depresión no es una debilidad personal. Así como alguien que sufre de 
                      cualquier enfermedad, las personas con diagnóstico de depresión pueden experimentar una amplia gama de síntomas:
                      <ul>
                        <li>Perder interés en algo o dejar de luchar por ello.</li>
                        <li>Falta de motivación.</li>
                        <li>Tristeza desbordante o sentimiento de que la vida no merece la pena vivirla.</li>
                        <li>Comer más o comer menos y los consecuentes cambios de peso.</li>
                        <li>Dormir más o dormir menos.</li>
                          </div>
                          </br>
                          <div class="col-md-6">
                            <img class="img-2" src="img/nervioso-depresion-interior.jpg" />
                          </div>
                          <div class="col-md-6">
                                </br>
                                  <li>Sentirse inútil o culpable sin razón.</li>
                                  <li>Tener problemas de concentración y de toma de decisiones.</li>
                                  <li>Sentirse irritable y enojado.</li>
                                  <li>Pensar en suicidio o muerte.</li>
                          </div>
                        </ul>
                        </p>
                      <div class="col-md-6 justificar menos_espacio">  
                        <p>
                          Mientras todos podemos tener días en los que experimentamos alguno de estos síntomas, las personas que sufren de depresión 
                          continúan experimentando estos síntomas durante semanas o aún meses.
                        </p> 
                      </div>  
                        <p class="parrafo2">
                            Para hablar de la depresión, los síntomas deberían 
                            estar presentes durante al menos 2 semanas y podrían incluir además de los ya nombrados, dolores de cabeza, dolor abdominal 
                            u otras molestias y dolores. 
                          </p>
                      
                  
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                
            </div>
            <div class="row info">
                <div class="col-md-8  justificar menos_espacio">
                    <h2>¿Cuáles son las causas de la depresión?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                      Las causas exactas de la depresión son desconocidas. Factores genéticos y ambientales, así como el desequilibrio químico 
                      podrían ser factores de riesgo asociados con la depresión. Las mujeres pueden ser más propensas a la depresión debido a 
                      los posibles efectos de los cambios hormonales. La edad también debe ser tomada en cuenta; La depresión clínica puede comenzar
                      a cualquier edad; sin embargo, la edad promedio de instalación está ubicada en la mitad de la década de los 20 años. 
                      La herencia y los episodios previos pueden aumentar el riesgo de desarrollar depresión.
                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row info">
              <div class="col-md-12 ">
                <h2>¿Cómo saber si estoy en riesgo?</h2>
                  <div class="fila-slider">
              <div id="myCarouselPiel4" class="carousel slide pfizer-carousel" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators hidden-xs">
                <li data-target="#myCarouselPiel4" data-slide-to="0"></li>
                <li data-target="#myCarouselPiel4" data-slide-to="1"></li>
                <li data-target="#myCarouselPiel4" data-slide-to="2"></li>
                <li data-target="#myCarouselPiel4" data-slide-to="3"></li>
                <li data-target="#myCarouselPiel4" data-slide-to="4"></li>
                <li data-target="#myCarouselPiel4" data-slide-to="5"></li>
           </ol>

            <div class="col-sm-7 pfizer-slider">
              <div class="carousel-inner" role="listbox">
                    <div class="item active" style="background:#67c9e7;">
                        <!-- <img class="first-slide" src="images/WB26_003.jpg" alt="First slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_2">
                                  <p>
                                  Los parientes de primer grado –padres, hermanos, hijos  de una persona con depresión tienen un riesgo mayor de 
                                    desarrollarla.
                                  </p>
                                
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#54c2e4;">
                        <!-- <img class="second-slide" src="images/WB33_009.jpeg" alt="Second slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_2">
                                <p>
                                <ul>
                                <li >
                                  Muerte y otras pérdidas.
                                </li>
                                <li >
                                  Dificultades en las relaciones.
                                </li>
                                <li >
                                  Eventos importantes en la vida. 
                                </li>
                              </ul>
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#42bce2;">
                        <!-- <img class="third-slide" src="images/CW40_085.jpg" alt="Third slide"> -->
                        
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_2">
                               <p>
                                  Las personas que han sobrevivido a eventos profundamente turbadores del pasado, como el abuso en la infancia.
                               </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#3ba9cb;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_2">
                                 <p>
                                La dependencia del alcohol o las drogas puede incrementar el riesgo de depresión.
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#3496b4;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_2">
                                 <p>
                                  <ul>
                                <li >
                                  Enfermedades relacionadas con las hormonas.
                                </li>
                                <li>
                                  Enfermedad cardíaca.
                                </li>
                                <li>
                                  Ataque cerebral.
                                </li>
                                <li >
                                  Cáncer.
                                </li>
                                <li >
                                  Apnea obstructiva del sueño.
                                </li>
                                <li>
                                  Dolor crónico.
                                </li>
                              </ul>
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#2e839e;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                          <div class="carousel-caption">
                              <div class="carousel-caption-text">
                                  <ul>
                                    <li > 
                                      Tiene baja autoestima.
                                    </li>
                                    <li > 
                                      Es demasiado crítico.
                                    </li>
                                    
                                    <li >
                                      Es habitualmente pesimista.
                                    </li>
                                    <li > 
                                      Se abruma fácilmente por el estrés.
                                    </li>
                                  </ul>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hidden-xs col-md-5 pfizer-slider-indicators" >
              <ol class="carousel-indicators">
                      <li class="botones6a slide18 pf-on" data-target="#myCarouselPiel4" data-slide-to="0"><p>Historia familiar</p></li>
                      <li class="botones6a slide28 apagado pf-off" data-target="#myCarouselPiel4" data-slide-to="1"><p>Situaciones estresantes</p></li>

                      <li class="botones6a slide28 apagado pf-off" data-target="#myCarouselPiel4" data-slide-to="2"><p>Experiencias pasadas</p></li>
                      <li class="botones6a slide28 apagado pf-off" data-target="#myCarouselPiel4" data-slide-to="3"><p>Dependencia química</p></li>

                      <li class="botones6au slide28 apagado pf-off" data-target="#myCarouselPiel4" data-slide-to="4"><p>Condiciones médicas</p></li>
                      <li class="botones6au slide28 apagado pf-off" data-target="#myCarouselPiel4" data-slide-to="5"><p>Aspectos psicológicos</p></li>
              </ol>
            </div>
            <a class="left carousel-control visible-xs" href="#myCarouselPiel4" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left visi" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control visible-xs" href="#myCarouselPiel4" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
      </div>
    </div>
                        <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                        <!-- <p>
                          <ul>
                            <li>
                              :
                                <ul>
                                  <li class="lista-sin-estilo">
                                    
                                  </li>
                                </ul>
                            </li>
                            <li>
                              : 
                              
                            </li>
                            <li>
                              :
                              <ul>
                                <li class="lista-sin-estilo">
                                 
                                </li>
                              </ul>
                            </li>
                            <li>
                              : 
                              <ul>
                                <li class="lista-sin-estilo">
                                    
                                </li>
                              </ul>
                            </li>
                            <li>
                              :
                              
                            </li>
                            <li>
                             
                             
                            </li>
                          </ul>
                        </p> -->
                       <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
            </div>
        </div>
            <div class="row info">
                <div class="col-md-8 ">
                    <h2>Depresión mayor</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                     La depresión mayor es la forma más frecuente de depresión. Se caracteriza por un cambio en el estado de ánimo que dura más de 
                     dos semanas e incluye uno o ambos de los signos <br>primarios de depresión: 
                     <ul>
                       <li>Sentimientos abrumadores de tristeza o pesar.</li>
                       <li>Pérdida del interés o placer en las actividades que habitualmente disfrutas. </li>
                     </ul>
                    </p>
                    <p>
                      La persona que tiene depresión mayor, presenta también por lo menos cuatro de los siguientes signos y síntomas regularmente, 
                      o todos los días:
                      <ul>
                        <li>Tristeza</li>
                        <li>Pérdida o aumento significativo de peso.</li>
                        <li>Alteración del sueño.</li>
                        <li>Movimientos lentos o inquietud.</li>
                        <li>Fatiga o falta de energía.</li>
                        <li>Baja autoestima o sentimientos de culpa inapropiados.</li>
                        <li>Sentimiento de inutilidad o culpa.</li>
                        <li>Sentimiento de impotencia o desesperanza. </li>
                        <li>Dificultad para pensar o concentrarse.</li>
                        <li>Pérdida del deseo sexual.</li>
                        <li>Pensamiento recurrente de muerte o suicidio.</li>
                      </ul>
                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
        <div class="row info hidden-xs">
          <div class="col-xs-12 ">
            <div class="col-lg-4 col-sm-5 botones-info">
                   <div class="row">
                      <div class= "col-sm-6 botonera boton_nervio1 pf-on1">
                        <figure>
                          <img src="img/medical51.png" width="100%" height="100%" />
                        </figure>
                      </div>
                        <div class= "col-sm-6 botonera boton_nervio2 apagado pf-off1">
                        <figure>
                          <img src="img/pill11.png" width="100%" height="100%" />
                        </figure>
                      </div>
                    </div>
                    <div class="row">
                      <div class= "col-sm-6 botonera boton_nervio3 apagado pf-off1">
                        <figure>
                          <img src="img/question1.png" width="100%" height="100%" />
                        </figure>
                      </div>
                      <div class= "col-sm-6 botonera boton_nervio4 apagado pf-off1">
                        <figure>
                          <img src="img/message30.png" width="100%" height="100%" />
                        </figure>
                      </div>
                    </div>
            </div>
            <div class="col-lg-8 col-sm-7 nervioso_info nervioso_info1">
                <h2>Diagnóstico</h2>
                  <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                  <p>
                      Si los síntomas son leves, el médico familiar o general puede tratar la enfermedad, pero si los síntomas son graves, si la 
                      depresión interfiere con la capacidad para funcionar en la vida diaria o si el tratamiento actual no parece estar 
                      funcionando, debe ver a un especialista: Psiquiatra o Psicólogo.
                  </p>
            </div>
            <div class="col-lg-8 col-sm-7 nervioso_info nervioso_info2 ">
                    <h2>Evite la auto medicación</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                     Si usted cree tener alguno de estos síntomas, evite la ingesta de bebidas alcohólicas o de algún tratamiento sin prescripción 
                     médica. Esto podría, eventualmente y de manera temporal, hacerlo sentir mejor pero también hace más difícil para usted, 
                     alcanzar la meta de mejorar a largo plazo. Regresar de una experiencia con alcohol podría aumentar su sentimiento de culpa o 
                     fatiga y podría continuar sintiendo que es una carga para las personas que lo rodean.
                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
            </div>
            <div class="col-lg-8 col-sm-7 nervioso_info nervioso_info3 ">
                    <h2>¿Qué puede usted hacer?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                     Acudir a su médico de confianza. El probablemente le prescribirá algún tratamiento. El tratamiento de la depresión puede 
                     incluir el uso de antidepresivos y/o dialogar. Se cree que la depresión causa cambios en la química cerebral o 
                     neurotransmisores. Se piensa que los antidepresivos revierten algunos de estos cambios. 
                    </p>
                    <p>
                      Algunos datos que usted debería conocer sobre los antidepresivos:
                      <ul>
                        <li>
                          Puede tardar de 4 a 8 semanas en lograr su efecto completo.
                        </li>
                        <li>
                          No se deben suspender de manera abrupta.
                        </li>
                        <li>
                          Usted podría necesitar probar diferentes antidepresivos (diferentes medicamentos son efectivos en diferentes personas, 
                          a cada persona le es útil distintos así como dosis distintas).
                        </li>
                        <li>
                          Podrían tener efectos colaterales, los cuales frecuentemente disminuyen luego de las primeras semanas de uso.
                        </li>
                      </ul>
                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
            </div>
            <div class="col-lg-8 col-sm-7 nervioso_info nervioso_info4 ">
                <h2>Dialogar</h2>
                <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                <p>
                 Conjuntamente con la medicación, su médico podría sugerir que dialogue con una persona de confianza, un psicólogo, un 
                 psiquiatra, un amigo o algún otro médico. El elemento más importante es encontrar una persona con quien usted se sienta 
                 cómodo(a). No se rinda si usted consulta a alguien y no resulta satisfactorio para usted. Busque otra persona a quien 
                 hablarle.

                </p>
                <p>
                  Hablar con alguien puede ayudarle a encontrar maneras de resolver algunos de los problemas de su vida o aprender a verlos de manera 
                  diferente. Los estudios han demostrado que para pacientes con depresión severa, la psicoterapia utilizada conjuntamente con 
                  antidepresivos también puede ayudar.
                </p>

               <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
            </div>
          </div>
        </div>
      </div>
        <div class="visible-xs">
            <div class="boton-wide boton_nervio1">
              <figure>
                <img src="img/medical51.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                <h2>Diagnóstico</h2>
                  <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                  <p>
                      Si los síntomas son leves, el médico familiar o general puede tratar la enfermedad, pero si los síntomas son graves, si la 
                      depresión interfiere con la capacidad para funcionar en la vida diaria o si el tratamiento actual no parece estar 
                      funcionando, debe ver a un especialista: Psiquiatra o Psicólogo.
                  </p>
              </div>
            </div>
            <div class="boton-wide boton_nervio2">
              <figure>
                <img src="img/pill11.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                <h2>Evite la auto medicación</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                     Si usted cree tener alguno de estos síntomas, evite la ingesta de bebidas alcohólicas o de algún tratamiento sin prescripción 
                     médica. Esto podría, eventualmente y de manera temporal, hacerlo sentir mejor pero también hace más difícil para usted, 
                     alcanzar la meta de mejorar a largo plazo. Regresar de una experiencia con alcohol podría aumentar su sentimiento de culpa o 
                     fatiga y podría continuar sintiendo que es una carga para las personas que lo rodean.
                    </p>
              </div>
            </div>
            <div class="boton-wide boton_nervio3">
              <figure>
                <img src="img/question1.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                <h2>¿Qué puede usted hacer?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                     Acudir a su médico de confianza. El probablemente le prescribirá algún tratamiento. El tratamiento de la depresión puede 
                     incluir el uso de antidepresivos y/o dialogar. Se cree que la depresión causa cambios en la química cerebral o 
                     neurotransmisores. Se piensa que los antidepresivos revierten algunos de estos cambios. 
                    </p>
                    <p>
                      Algunos datos que usted debería conocer sobre los antidepresivos:
                      <ul>
                        <li>
                          Puede tardar de 4 a 8 semanas en lograr su efecto completo.
                        </li>
                        <li>
                          No se deben suspender de manera abrupta.
                        </li>
                        <li>
                          Usted podría necesitar probar diferentes antidepresivos (diferentes medicamentos son efectivos en diferentes personas, 
                          a cada persona le es útil distintos así como dosis distintas).
                        </li>
                        <li>
                          Podrían tener efectos colaterales, los cuales frecuentemente disminuyen luego de las primeras semanas de uso.
                        </li>
                      </ul>
                    </p>
              </div>
            </div>
            <div class="boton-wide boton_nervio4">
              <figure>
                <img src="img/message30.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                <h2>Dialogar</h2>
                <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                <p>
                 Conjuntamente con la medicación, su médico podría sugerir que dialogue con una persona de confianza, un psicólogo, un 
                 psiquiatra, un amigo o algún otro médico. El elemento más importante es encontrar una persona con quien usted se sienta 
                 cómodo(a). No se rinda si usted consulta a alguien y no resulta satisfactorio para usted. Busque otra persona a quien 
                 hablarle.

                </p>
                <p>
                  Hablar con alguien puede ayudarle a encontrar maneras de resolver algunos de los problemas de su vida o aprender a verlos de manera 
                  diferente. Los estudios han demostrado que para pacientes con depresión severa, la psicoterapia utilizada conjuntamente con 
                  antidepresivos también puede ayudar.
                </p>
              </div>
            </div>
        </div>
        <div class="container">
            <div class="row info">
                <div class="col-md-8 ">
                    <h2>Otros datos de utilidad</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                     Es importante recordar que la depresión es una enfermedad. Sin embargo, muchas personas no mejoran. Además de cumplir su plan de tratamiento 
                     prescrito, pregúntele a su médico si los siguientes datos pueden también ayudar.
                      <ul>
                        <li>Adhiérase a una rutina (coma y duerma regularmente).</li>
                        <li>
                          Trate de hacer cosas que usted disfruta o acostumbraba disfrutar.
                        </li>
                        <li>
                          Fíjese pequeñas metas para sí mismo cada día y trate de alcanzarlas.
                        </li>
                        <li>
                          Hable con personas en quienes usted confía, sobre cómo se siente. 
                        </li>
                      </ul>
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
        </div>
    </div>
<?php include 'footer.php' ?>
   
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>
<script>
     $( ".boton_nervio1" ).on( "click", function() {
         $('.nervioso_info1').css('display', 'block');
         $('.nervioso_info2').css('display', 'none');
         $('.nervioso_info3').css('display', 'none');
         $('.nervioso_info4').css('display', 'none');
      });
     $( ".boton_nervio2" ).on( "click", function() {
         $('.nervioso_info1').css('display', 'none');
         $('.nervioso_info2').css('display', 'block');
         $('.nervioso_info3').css('display', 'none');
         $('.nervioso_info4').css('display', 'none');
      });
     $( ".boton_nervio3" ).on( "click", function() {
         $('.nervioso_info1').css('display', 'none');
         $('.nervioso_info2').css('display', 'none');
         $('.nervioso_info3').css('display', 'block');
         $('.nervioso_info4').css('display', 'none');
      });
    $( ".boton_nervio4" ).on( "click", function() {
         $('.nervioso_info1').css('display', 'none');
         $('.nervioso_info2').css('display', 'none');
         $('.nervioso_info3').css('display', 'none');
         $('.nervioso_info4').css('display', 'block');
      });
    </script>
    </script>
<script>
  $(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            interval: false
        });
    });
});​
</script>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script>
$(document).ready(function(){
    $( "body" ).on('click', '.pf-off', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on").addClass( "apagado" );

        $(this).removeClass( "pf-off" );
        $(".pf-on").addClass( "pf-off" );
        $(".pf-on").removeClass( "pf-on" );
        $(this).addClass( "pf-on" );    });

    $( "body" ).on('click', '.pf-off1', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on1").addClass( "apagado" );

        $(this).removeClass( "pf-off1" );
        $(".pf-on1").addClass( "pf-off1" );
        $(".pf-on1").removeClass( "pf-on1" );
        $(this).addClass( "pf-on1" );    
      });
});
</script>
</body>
</html>
    