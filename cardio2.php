<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    
    <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/cardio-banner.jpg" >
     <?php include 'mainnav.php' ?>
        <div class="container title">
         <h1 class="heading-interno">CARDIOLOGÍA</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
  <div class="container-fluid white pad-30">
    <div id="vida"></div>
      <div class="container">
          <ol class="breadcrumb">
            <li><a href="cardio.php">Cardiología</a></li>
            <li class="active">Triglicéridos</li>
          </ol>
                <!-- <h1>DIFTERIA</h1> -->
            <div class="row info">
                <div class="col-md-6 justificar">
                    <h2>¿Qué son los triglicéridos?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Al igual que el colesterol, los triglicéridos son un tipo de grasa muy necesaria para nuestro cuerpo. Nuestro organismo almacena 
                       energía en forma de triglicéridos. El problema ocurre cuando la cantidad de triglicéridos en sangre es excesivamente alta.
                    </p>
                    <br>
                    <table class="col-md-offset-4 col-xs-offset-3 table-condensed table table-bordered texto-principio tabla-pequena">
                        <th colspan="2" class="texto-principio col-md-6" style="color: rgb(66, 188, 226)">Trigliceridos en plasma (mg/d)</th>
                        <tr>
                          <td class="col-md-5">Normal</td>
                          <td class="col-md-5">< 150</td>
                        </tr>
                        <tr>
                          <td class="col-md-5">Alto</td>
                          <td class="col-md-5">151-499</td>
                        </tr>
                        <tr>
                          <td class="col-md-5">Muy Alto</td>
                          <td class="col-md-5">> 500</td>
                        </tr>
                    </table>
                    <br>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6">
                  <img class="img-2" src="img/cardio-trigliceridos-interior.jpg" />
                </div>
                <div class="col-md-6 justificar">
                  <p>
                       Cuando ocurre el sobrepeso y la obesidad los triglicéridos se acumulan en el tejido adiposo que está  bajo la piel, llamado 
                       subcutáneo y alrededor de las vísceras abdominales llamada grasa visceral. Los azucares consumidos en exceso (hidratos de carbona) 
                       promueven la síntesis de trigliceridos.
                    </p>
                    <p>La circunferencia abdominal. La grasa acumulada dentro del abdomen es un indicador de riesgo de infarto de miocardio, de resistencia 
                      a la insulina y de síndrome metabólico.
                    </p>
                </div>
                <div class="col-md-6">    
                    <p></br>
                      Por eso los médicos miden el perímetro o circunferencia abdominal para comprobar que es menor 
                      de 80 cm  en la mujer y menor de 90 cm en los hombres venezolanos.
                    </p>
                    <p></br>
                      En algunas personas, los triglicéridos pueden aumentar por razones genéticas, pero esto no  es lo más frecuente en los pacientes.
                </div>
          </div>
            <div class="row fila-slider">
              <div class="col-md-12">
              <div id="myCarouselCardio2" class="carousel slide pfizer-carousel" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators hidden-xs">
                    <li data-target="#myCarouselCardio2" data-slide-to="0"></li>
                    <li data-target="#myCarouselCardio2" data-slide-to="1"></li>
                    <li data-target="#myCarouselCardio2" data-slide-to="2"></li>
                </ol>
                <div class="col-sm-7 pfizer-slider">
                  <div class="carousel-inner" role="listbox">
                      <div class="item active" style="background:#33D7F2;">
                          <!-- <img class="first-slide" src="images/WB26_003.jpg" alt="First slide"> -->
                          <div class="container">
                              <div class="carousel-caption">
                                <div class="carousel-caption-text cardio_text_4">
                                     <table>
                                        <tr>
                                          <td>
                                            <ul>
                                              <li>Azúcar</li>
                                              <li>Pan</li>
                                              <li>Pasta</li>
                                              <li>Tortas</li>
                                              <li>Arepas</li>
                                              <li>Dulces caseros y de pastelería</li>
                                              <li>Golosinas, mermeladas</li>
                                              <li>Pasapalos en sobres</li>
                                             
                                              <li>Tubérculos ricos en almidón como yuca, ocumo, ñame</li>
                                            </ul>
                                          </td>
                                          <td>
                                            <ul>
                                              <li>Caramelos</li>
                                              <li>Refrescos con azúcar (excepto los dietéticos)</li>
                                              <li>Bebidas alcohó1icas (más  de dos capas)</li>
                                              <li>Papas</li>
                                               <li>Arroz</li>
                                               <li>Chicha</li>
                                              <li>Salsas espesadas con almidón o con harina</li>
                                              
                                            </ul>
                                          </td>
                                        </tr>
                                      </table>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div class="item" style="background:#25BBCA;">
                          <!-- <img class="second-slide" src="images/WB33_009.jpeg" alt="Second slide"> -->
                          <div class="container">
                              <div class="carousel-caption">
                                <div class="carousel-caption-text cardio_text_1">
                                 <p>Si los triglicéridos aumentan en  sangre por  periodos prolongados de meses o años, es afectado el sistema cardiovascular, 
                                    porque estas grasas favorecen la formación de ateromas obstructivos en las arterias coronarias, con riesgo aumentado de sufrir
                                     angina de pecho o infarto de miocardio.
                                   </p>
                                   <br>
                                   <p>
                                     Este aumento de triglicéridos se asocia generalmente con la disminución del HDL (o colesterol "buena") y con aumento del LDL 
                                     (o colesterol "malo").
                                   </p>

                                </div>
                              </div>
                          </div>
                      </div>
                      <div class="item" style="background:#2394A4;">
                          <!-- <img class="third-slide" src="images/CW40_085.jpg" alt="Third slide"> -->
                          
                          <div class="container">
                              <div class="carousel-caption">
                                <div class="carousel-caption-text">
                                  <p>Cuando comemos en  cantidades  superiores a  las  necesarias, las  comidas con mucho contenido de azucares, producirán un aumento 
                                    de triglicéridos.
                                   </p>
                                   <br>
                                   <p>
                                     La ingesta de alcohol en exceso es una fuente de energía que incrementara nuestros triglicéridos. El sobrepeso y la obesidad, 
                                     el sedentarismo (falta de ejercicio) inducen un excesivo depósito de grasa porque los triglicéridos se almacenan en el tejido 
                                     adiposo.
                                   </p>
                                   <br>
                                    <p>
                                      La diabetes, por  el  déficit y resistencia a la insulina, puede  facilitar el aumento de triglicéridos en sangre.
                                    Ciertos medicamentos,  enfermedades  endocrinológicas,  renales y  hepáticas pueden provocar aumento de triglicéridos.</p>
                                    
                                    </p>
                                  
                                </div>
                              </div>
                          </div>
                      </div> 
                  </div>
                </div>
                <div class="hidden-xs col-sm-5 pfizer-slider-indicators" >
                    <ol class="carousel-indicators">
                        <li class="botones3" data-target="#myCarouselCardio2" data-slide-to="0" style="background:#33D7F2;"><p>Alimentos que aumentan los triglicéridos</p></li>
                        <li class="botones3" ata-target="#myCarouselCardio2" data-slide-to="1" style="background:#25BBCA;"><p>Consecuencias de los triglicéridos elevados</p></li>
                        <li class="botones3u" data-target="#myCarouselCardio2" data-slide-to="2" style="background:#2394A4;"><p>¿Por qué aumentan los triglicéridos?</p></li>
                        </ol>
                </div>
                <a class="left carousel-control visible-xs" href="#myCarouselCardio2" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left visi" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control visible-xs" href="#myCarouselCardio2" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
              </div>
              </div>
            </div>
            
        </div>
    </div>
 <?php include 'footer.php' ?>
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>
<script>
  $(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            interval: false
        });
    });
});​
</script>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script>
$(document).ready(function(){

    $('.carousel-indicators li').on('click', function(){

        $('.carousel-indicators li').each(function(){

            $(this).removeClass("apagado");

        });

        $(this).addClass("apagado");
    });

    $( "body" ).on('click', '.pf-off', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on").addClass( "apagado" );

        $(this).removeClass( "pf-off" );
        $(".pf-on").addClass( "pf-off" );
        $(".pf-on").removeClass( "pf-on" );
        $(this).addClass( "pf-on" );

    });
});

</script>
</body>
</html>
    