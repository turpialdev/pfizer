 <div class="row row-interno " role="tabpanel tab-interno">
          <!-- Nav tabs -->
              <ul class="nav nav-tabs tabs-custom-interno" role="tablist">
                <li role="presentation" class="active"><a href="#piel1" aria-controls="piel1" role="tab" data-toggle="tab">Psoriasis</a></li>
                <li role="presentation"><a href="#piel2" aria-controls="piel2" role="tab" data-toggle="tab">Artritis Reumatoide</a></li>
               	<li role="presentation"><a href="#piel3" aria-controls="piel3" role="tab" data-toggle="tab">Espondilitis Anquilosante</a></li>
               	<li role="presentation"><a href="#piel4" aria-controls="piel4" role="tab" data-toggle="tab">Artritis Psoriásica</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content tab-content-interno">
                <div role="tabpanel" class="tab-pane active" id="piel1"><?php include 'piel1.php' ?></div>
                <div role="tabpanel" class="tab-pane" id="piel2"><?php include 'piel2.php' ?></div>
                 <div role="tabpanel" class="tab-pane" id="piel3"><?php include 'piel3.php' ?></div>
                 <div role="tabpanel" class="tab-pane" id="piel4"><?php include 'piel4.php' ?></div>
              </div>

  </div>
            