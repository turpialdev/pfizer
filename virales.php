<!DOCTYPE html>
<html>
    <?php include 'head.php' ?>
    <body class="vacunas-pag">
        <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/vacunas-banner.jpg" >
            <?php include 'mainnav.php' ?>
            <div class="container title">
                <h1 class="heading-interno">INMUNIZACIÓN</h1>
            </div>
        </div>
        <?php include 'sintomasnav.php' ?>
        <!--<div> -->
        <div class="container">
            <div id="menu_vacunas2" name="menu_vacunas2" class="row hidden-xs">
                <ol class="breadcrumb">
                    <li><a href="vacunas.php">Vacunas</a></li>
                    <li class="active">Enfermedades Virales Inmunoprevenibles</li>
                </ol>
                <!-- <a class="link_scroll" href="#vacuna10"> -->
                <div class="col-xs-1 botonera_vacunas2 boton_vacuna9">
                    <img src="img/woman.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        PAROTIDITIS
                    </div>
                </div>                    
                <div class="col-xs-1 botonera_vacunas2 boton_vacuna8">
                    <img src="img/vacunas.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        POLIO
                    </div>
                </div>
                <div class="col-xs-1 botonera_vacunas2 boton_vacuna7">
                    <img src="img/medical46.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        INFLUENZA (GRIPE)
                    </div>
                </div>                        
                <div class="col-xs-2 botonera_vacunas2 boton_vacuna1">
                    <img src="img/molecule12.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        VARICELA
                    </div>
                </div>                  
                <!--  </a>
                <a class="link_scroll" href="#vacuna14"> -->
                <div class="col-xs-1 botonera_vacunas2 boton_vacuna2">
                    <img src="img/virus5.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        ROTAVIRUS
                    </div>
                </div>
                <!--  </a>
                <a class="link_scroll" href="#vacuna1"> -->
                <div class="col-xs-1 botonera_vacunas2 boton_vacuna3">
                    <img src="img/boy3.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        RUBEOLA
                    </div>
                </div>
                <!--  </a>
                <a class="link_scroll" href="#vacuna9"> -->
                <div class="col-xs-1 botonera_vacunas2 boton_vacuna4">
                    <img src="img/dna12.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        HEPATITIS B
                    </div>
                </div>
                <!--  </a>
                <a class="link_scroll" href="#vacuna7"> -->
                <div class="col-xs-1 botonera_vacunas2 boton_vacuna5">
                    <img src="img/hospital38.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        HEPATITIS A
                    </div>
                </div>
                <!--Juanchi</a>
                <a class="link_scroll" href="#vacuna2"> -->
                <div class="col-xs-1 botonera_vacunas2 boton_vacuna6">
                    <img src="img/user168.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        SARAMPIÓN
                    </div>
                </div>
                <!--Juanchi </a> -->
            </div>
            <div class="boton-wide boton_vacuna8 visible-xs">
                <figure>
                    <img src="img/vacunas.png" width="100%" height="100%" />
                </figure>
            </div>  
            <div class="boton-wide boton_vacuna9 visible-xs">
                <figure>
                    <img src="img/vacunas.png" width="100%" height="100%" />
                </figure>
            </div>
            <div id="vacuna8" name="vacuna8" class="row vacuna vacuna8">
                <div class="col-md-6">
                    <h2>parotiditis</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li class="lista-sin-estilo"> La parotiditis es una infección viral que puede hacer que las glándulas salivales se hinchen.</li>
                        </ul>
                    </p>
                    <h4>¿De qué manera la parotiditis  puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                                La parotiditis puede provocar:
                                <ul>
                                    <li>Dolor de oídos y sensibilidad debajo de la mandíbula.</li>
                                    <li>Pancreatitis</li>
                                    <li>Meningitis con o sin síntomas. La meningitis causada por la parotiditis, por lo general, no ocasiona una enfermedad grave. </li>
                                </ul>
                            </li>
                            <li>
                                La parotiditis también puede provocar inflamación de ovarios en las niñas o de testículos en los niños; en raras ocasiones, esto puede causar infertilidad.
                            </li>
                            <li>
                                En casos infrecuentes, la parotiditis puede ocasionar complicaciones graves como: encefalitis o pérdida de la audición, por lo general, en los adultos.
                            </li>                      
                        </ul>
                    </p>
                </div> 
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La  parotiditis se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                            <li>También se puede transmitir por contacto directo con saliva o gotitas infectadas.</li>
                        </ul>
                    </p>
                </div> 
                <!-- Juanchi
                <a class="link_scroll" href="#menu_vacunas2">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </div>
                </a>  
                -->
            </div>
            <div class="boton-wide boton_vacuna1 visible-xs">
                <figure>
                    <img src="img/virus5.png" width="100%" height="100%" />
                </figure>
            </div>
            <div id="vacuna11" name="vacuna11" class="row vacuna vacuna11">
                <div class="col-md-6">
                    <h2>polio</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li class="lista-sin-estilo"> La polio es una infección viral sumamente contagiosa que puede provocar parálisis. Antes, la polio era más frecuente, pero hoy en día es muy infrecuente gracias a la vacunación.
                            </li>
                        </ul>
                    </p>
                    <h4>¿De qué manera la polio puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La polio puede provocar: 
                                <ul>
                                    <li>Parálisis permanente, por lo general de una pierna. </li>
                                    <li>Debilidad muscular permanente. </li>
                                    <li>Discapacidad de por vida. </li>
                                    <li>Meningitis, aunque esto es infrecuente.</li>
                                </ul>
                            </li>
                        </ul>
                    </p>
                </div>     
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La polio se encuentra en la materia fecal de las personas infectadas y se transmite fácilmente a las manos y los objetos.</li>
                            <li>Las personas pueden transmitir el virus aunque no presenten síntomas.</li>
                        </ul>
                    </p>
                </div>     
                <!--Juanchi 
                <a class="link_scroll" href="#menu_vacunas2">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                --> 
            </div>
            <div id="vacuna5" name="vacuna5" class="row vacuna vacuna5">
                <div class="col-md-6">
                    <h2>influenza (gripe)</h2>
                    <p>
                        La influenza (la gripe) es una infección viral sumamente contagiosa que afecta la nariz, la garganta y los pulmones.
                    </p>
                    <h4>¿De qué manera puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>Los síntomas de la influenza incluyen fiebre (a veces lo suficientemente alta como para provocar convulsiones), dolores musculares, pérdida de apetito, cansancio extremo y dolor de cabeza.
                            </li>
                            <li>Algunos casos de influenza son graves y pueden causar neumonía o la muerte.</li>
                            <li>Los niños pequeños tienen mayor probabilidad de ser hospitalizados por gripe que los niños de mayor edad.</li>    
                        </ul>
                    </p>
                </div> 
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>Todos los niños se encuentran en riesgo.</li>
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La gripe se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                            <li>También se puede transmitir al tocar superficies contaminadas con el virus. </li>
                        </ul>
                    </p> <!--Juanchi -->
                </div> 
                <!--Juanchi
                <a class="link_scroll" href="#menu_vacunas2">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                -->
            </div>                
            <div id="vacuna10" name="vacuna10" class="row vacuna vacuna10">
                <div class="col-md-6">
                    <h2>varicela</h2>   
                    <p>
                        La varicela es causada por un virus que produce una erupción de ampollas y picazón.
                    </p>
                    <h4>¿De qué manera puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                                Mientras que la mayoría de los casos son leves, la varicela puede, a veces, provocar problemas de salud graves, que incluyen:
                                <ul>
                                    <li>Infecciones en la piel que requieren hospitalización.</li>
                                    <li>Neumonía</li>
                                    <li>Encefalitis (inflamación del cerebro).</li>
                                </ul>
                            </li>
                            <li>La varicela también puede ocasionar infecciones bacterianas secundarias graves, que pueden representar un riesgo para la vida.</li>
                            <li>Las complicaciones graves son más frecuentes en adultos y también pueden ocurrir en lactantes.</li>
                            <li>La varicela también puede dejar cicatrices en la piel.</li>
                        </ul>
                    </p>
                </div> 
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo.</li>
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La varicela se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                            <li>
                                Las personas también pueden contraer la enfermedad al tocar las ampollas de la varicela.
                            </li>
                        </ul>
                    </p>
                </div> 
                <!-- Juanchi
                <a class="link_scroll" href="#menu_vacunas2">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                -->
            </div>                
            <div class="boton-wide boton_vacuna2 visible-xs">
                <figure>
                    <img src="img/virus5.png" width="100%" height="100%" />
                </figure>
            </div>                
            <div id="vacuna14" name="vacuna14" class="row vacuna vacuna14">
                <div class="col-md-6">
                    <h2>rotavirus</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li class="lista-sin-estilo">  El rotavirus es un virus que puede provocar diarrea intensa en lactantes y niños pequeños.</li>
                        </ul>
                    </p>
                    <h4>¿De qué manera el rotavirus puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>Los síntomas de una infección por rotavirus incluyen: 
                                <ul>
                                    <li>Diarrea, vómitos, fiebre, dolor de estómago y deshidratación.</li>
                                    <li>En algunos casos, el rotavirus puede provocar la muerte debido a una deshidratación intensa.</li>
                                </ul>
                            </li>
                        </ul>
                    </p>
                </div> 
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>El rotavirus se transmite por contacto de persona a persona o por contacto con objetos infectados.</li>
                            <li>El rotavirus puede vivir en superficies durante semanas, si las mismas no se desinfectan bien.</li>
                        </ul>
                    </p>
                </div> 
                <!--Juanchi
                <a class="link_scroll" href="#menu_vacunas2">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                --> 
            </div>
            <div class="boton-wide boton_vacuna3 visible-xs">
                <figure>
                    <img src="img/boy3.png" width="100%" height="100%" />
                </figure>
            </div>
            <div id="vacuna1" name="vacuna1" class="row vacuna vacuna1">
                <div class="col-md-6">
                    <h2>rubeola</h2>   
                    <p>
                        La rubeola es una infección viral frecuentemente conocida como “sarampión alemán”.     
                    </p>
                    <h4>¿De qué manera el rotavirus puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La rubeola puede provocar una erupción en la cara y el cuerpo, y fiebre baja.</li>
                            <li>En ocasiones infrecuentes las personas con rubeola desarrollan problemas graves relacionados con la enfermedad </li>
                            <li>Sin embargo, la rubeola es peligrosa para las mujeres embarazadas, especialmente en el primer trimestre, porque provoca defectos congénitos.
                                <ul>
                                    <li>Los lactantes con rubeola pueden transmitir la enfermedad a otras personas, lo que incluye a las mujeres embarazadas.</li>
                                </ul>
                            </li>  
                        </ul>
                    </p>
                </div> 
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>     
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <p>
                            <ul>
                                <li>La rubeola se transmite a través del aire al:
                                    <ul>
                                        <li>Estornudar</li>
                                        <li>Toser</li>
                                        <li>Inhalar gotitas infectadas</li>
                                    </ul>
                                </li>
                            </ul>
                        </p>
                    </p>
                </div> 
                <!--Juanchi
                <a class="link_scroll" href="#menu_vacunas2">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div> 
                -->
            </div>            
            <div class="boton-wide boton_vacuna4 visible-xs">
                <figure>
                    <img src="img/dna12.png" width="100%" height="100%" />
                </figure>
            </div>                
            <div id="vacuna9" name="vacuna9" class="row vacuna vacuna9">
                <div class="col-md-6">
                    <h2>hepatitis B</h2>   
                    <p>
                        La hepatitis B es una infección del hígado causada por un virus.
                    </p>
                    <h4>¿De qué manera la hepatitis B puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>En general, la hepatitis B no presenta síntomas.</li>
                            <li>Cuando se presentan síntomas, estos incluyen:
                                <ul>
                                    <li>Dolor de estómago</li>
                                    <li>Pérdida de apetito</li>
                                    <li>Náuseas y vómitos</li>
                                    <li>Color amarillento de la piel y la parte blanca de los ojos</li>
                                </ul>
                            </li>
                            <li>En muchas personas, la hepatitis B es una infección crónica que dura toda la vida. </li>
                            <li>A veces, la hepatitis B puede ocasionar cáncer de hígado y otras enfermedades hepáticas varios años después de la infección.</li>
                        </ul>
                    </p>
                </div> 
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>     
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La hepatitis B se transmite a través del contacto con la sangre y otros líquidos corporales.</li>
                            <li>La hepatitis B también se puede transmitir de una madre embarazada a su bebé en gestación.</li>
                            <li>Las personas con hepatitis B pueden transmitir el virusaunque no presenten síntomas.</li>
                        </ul>
                    </p>
                </div> 
                <!--Juanchi
                <a class="link_scroll" href="#menu_vacunas2">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                --> 
            </div>
            <div class="boton-wide boton_vacuna5 visible-xs">
                <figure>
                    <img src="img/hospital38.png" width="100%" height="100%" />
                </figure>
            </div>
            <div id="vacuna7" name="vacuna7" class="row vacuna vacuna7">
                <div class="col-md-6">
                    <h2>hepatitis A</h2>   
                    <p>
                        La hepatitis A es una infección del hígado causada por un virus.   
                    </p>
                    <h4>¿De qué manera la hepatitis A puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>Los síntomas de la hepatitis A pueden incluir:
                                <ul>
                                    <li>Fiebre</li>
                                    <li>Náuseas y dolor de estómago.</li>
                                    <li>Pérdida del apetito y cansancio.</li>
                                    <li>Color amarillento de la piel y la parte blanca de los ojos.</li>
                                </ul>
                            </li>
                            <li>En casos aislados, la hepatitis A puede causar la muerte.</li>
                            <li>Los síntomas pueden durar 2 meses.</li>
                            <li>Los niños menores de 6 años, por lo general, no presentan síntomas.</li>
                        </ul>
                    </p>
                </div> 
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo.</li>        
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La hepatitis A se encuentra en la materia fecal de las personas infectadas y se transmite fácilmente a las manos y los 
                                objetos.</li>
                            <li>Comer o beber algo que se haya contaminado con el virus puede provocar la infección.</li>
                            <li>Las personas pueden transmitir la hepatitis A sin que manifiesten síntomas.</li>
                        </ul>
                    </p>
                </div> 
                <!--Juanchi
                <a class="link_scroll" href="#menu_vacunas2">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                --> 
            </div>
            <div class="boton-wide boton_vacuna6 visible-xs">
                <figure>
                    <img src="img/user168.png" width="100%" height="100%" />
                </figure>
            </div>
            <div id="vacuna2" name="vacuna2" class="row vacuna vacuna2">
                <div class="col-md-6">
                    <h2>sarampión</h2>   
                    <p>
                        El sarampión es una infección viral que provoca fiebre, erupción en la cara y en el cuerpo. 
                    </p>
                    <h4>¿De qué manera el sarampión puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>El sarampión puede, a veces, provocar enfermedades graves que incluyen:
                                <ul>
                                    <li>Encefalitis (inflamación del cerebro)</li>
                                    <li>Neumonía</li>
                                </ul>
                            </li>
                        </ul>
                    </p>
                </div> 
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo.</li>        
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>El sarampión se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                            <li>Se puede transmitir antes de que aparezcan los síntomas y después de que estos desaparezcan. </li>
                        </ul>
                    </p>
                </div> 
                <!--
                <a class="link_scroll" href="#menu_vacunas2">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                --> 
            </div>
        </div>
            
        <?php include 'footer.php' ?>
           
        <script type="text/javascript">
            function init() {
                window.addEventListener('scroll', function(e){
                var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                        shrinkOn = 100,
                        header = document.querySelector("nav");
                    if (distanceY > shrinkOn) {
                        classie.add(header,"smaller");
                    } else {
                        if (classie.has(header,"smaller")) {
                            classie.remove(header,"smaller");
                        }
                    }
                });
            }
            window.onload = init();
            $('.dropdown-toggle').dropdown();
            jQuery('ul.nav li.dropdown').hover(function() {
              jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
            }, function() {
              jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('a[href^="#"]').on('click', function(event) {

                    var target = $( $(this).attr('href') );

                    if( target.length ) {
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                    }

                });
            });
        </script>
        <script type="text/javascript">
             
              $(document).ready(function(){
               
                $('.vacunas').css('background-color', 'rgb(66,188,226)');
                $('.vacunas').css('color', '#fff');
                // $('.navbar-brand img').css('height', '55%');
               
              });
        </script>
        <script>
               $( ".boton_vacuna9" ).on( "click", function() {
                $('.vacuna8').css('display', 'block');
                 $('.vacuna11').css('display', 'none');
                 $('.vacuna5').css('display', 'none');
                 $('.vacuna10').css('display', 'none');
                 $('.vacuna14').css('display', 'none');
                 $('.vacuna1').css('display', 'none');
                 $('.vacuna9').css('display', 'none');
                 $('.vacuna7').css('display', 'none');
                 $('.vacuna2').css('display', 'none');
              });
               $( ".boton_vacuna8" ).on( "click", function() {
                $('.vacuna8').css('display', 'none');
                 $('.vacuna11').css('display', 'block');
                 $('.vacuna5').css('display', 'none');
                 $('.vacuna10').css('display', 'none');
                 $('.vacuna14').css('display', 'none');
                 $('.vacuna1').css('display', 'none');
                 $('.vacuna9').css('display', 'none');
                 $('.vacuna7').css('display', 'none');
                 $('.vacuna2').css('display', 'none');
              });
               $( ".boton_vacuna7" ).on( "click", function() {
                $('.vacuna8').css('display', 'none');
                 $('.vacuna11').css('display', 'none');
                 $('.vacuna5').css('display', 'block');
                 $('.vacuna10').css('display', 'none');
                 $('.vacuna14').css('display', 'none');
                 $('.vacuna1').css('display', 'none');
                 $('.vacuna9').css('display', 'none');
                 $('.vacuna7').css('display', 'none');
                 $('.vacuna2').css('display', 'none');
              });
             $( ".boton_vacuna1" ).on( "click", function() {
                $('.vacuna8').css('display', 'none');
                 $('.vacuna11').css('display', 'none');
                 $('.vacuna5').css('display', 'none');
                 $('.vacuna10').css('display', 'block');
                 $('.vacuna14').css('display', 'none');
                 $('.vacuna1').css('display', 'none');
                 $('.vacuna9').css('display', 'none');
                 $('.vacuna7').css('display', 'none');
                 $('.vacuna2').css('display', 'none');
              });
             $( ".boton_vacuna2" ).on( "click", function() {
                $('.vacuna8').css('display', 'none');
                 $('.vacuna11').css('display', 'none');
                $('.vacuna5').css('display', 'none');
                 $('.vacuna10').css('display', 'none');
                 $('.vacuna14').css('display', 'block');
                 $('.vacuna1').css('display', 'none');
                 $('.vacuna9').css('display', 'none');
                 $('.vacuna7').css('display', 'none');
                 $('.vacuna2').css('display', 'none');
              });
             $( ".boton_vacuna3" ).on( "click", function() {
                $('.vacuna8').css('display', 'none');
                 $('.vacuna11').css('display', 'none');
                $('.vacuna5').css('display', 'none');
                 $('.vacuna10').css('display', 'none');
                 $('.vacuna14').css('display', 'none');
                 $('.vacuna1').css('display', 'block');
                 $('.vacuna9').css('display', 'none');
                 $('.vacuna7').css('display', 'none');
                 $('.vacuna2').css('display', 'none');
              });
             $( ".boton_vacuna4" ).on( "click", function() {
                $('.vacuna8').css('display', 'none');
                 $('.vacuna11').css('display', 'none');
                $('.vacuna5').css('display', 'none');
                 $('.vacuna10').css('display', 'none');
                 $('.vacuna14').css('display', 'none');
                 $('.vacuna1').css('display', 'none');
                 $('.vacuna9').css('display', 'block');
                 $('.vacuna7').css('display', 'none');
                 $('.vacuna2').css('display', 'none');
              });
             $( ".boton_vacuna5" ).on( "click", function() {
                $('.vacuna8').css('display', 'none');
                 $('.vacuna11').css('display', 'none');
                $('.vacuna5').css('display', 'none');
                 $('.vacuna10').css('display', 'none');
                 $('.vacuna14').css('display', 'none');
                 $('.vacuna1').css('display', 'none');
                 $('.vacuna9').css('display', 'none');
                 $('.vacuna7').css('display', 'block');
                 $('.vacuna2').css('display', 'none');
              });
             $( ".boton_vacuna6" ).on( "click", function() {
                $('.vacuna8').css('display', 'none');
                 $('.vacuna11').css('display', 'none');
                $('.vacuna5').css('display', 'none');
                 $('.vacuna10').css('display', 'none');
                 $('.vacuna14').css('display', 'none');
                 $('.vacuna1').css('display', 'none');
                 $('.vacuna9').css('display', 'none');
                 $('.vacuna7').css('display', 'none');
                 $('.vacuna2').css('display', 'block');
              });
        </script>
    </body>
</html>