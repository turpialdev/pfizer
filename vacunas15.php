<div  class="container-fluid padding-vacunas-interno">
       
                <!-- <h1>VARICELA</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>varicela</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> La varicela es causada por un virus que produce una erupción de ampollas y picazón.
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h4>¿De qué manera puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                            Mientras que la mayoría de los casos son leves, la varicela puede, a veces, provocar problemas de salud graves, que incluyen:
                            <ul>
                                <li>
                                    Infecciones en la piel que requieren hospitalización.
                                </li>
                                <li>
                                    Neumonía
                                </li>
                                <li>
                                    Encefalitis (inflamación del cerebro).
                                </li>
                            </ul>

                            </li>
                            <li>La varicela también puede ocasionar infecciones bacterianas secundarias graves, que pueden representar un riesgo para la 
                                vida.</li>
                            <li>Las complicaciones graves son más frecuentes en adultos y también pueden ocurrir en lactantes. </li>
                            <li>La varicela también puede dejar cicatrices en la piel.  </li>
                                
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                           <li>La varicela se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                            <li>
                                Las personas también pueden contraer la enfermedad al tocar las ampollas de la varicela.
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
  