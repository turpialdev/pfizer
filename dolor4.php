<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    
     <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/dolor-banner.jpg">
     <?php include 'mainnav.php' ?>
        <div class="container title">
         <h1 class="heading-interno">DOLOR</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
<div  class="container-fluid white pad-30">
        <div id="vida"></div>
        <div class="container">
            <ol class="breadcrumb">
            <li><a href="dolor.php">Dolor</a></li>
            <li class="active">Fibromialgia</li>
          </ol>
                <!-- <h1>FIBROMIALGIA</h1> -->
            
            <div class="row info">
                <div class="col-md-6 justificar menos_espacio">
                    <h2>¿Qué es la fibromialgia?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       La fibromialgia es una enfermedad neurológica que se refiere a un grupo de síntomas caracterizados fundamentalmente 
                       por dolor. Este dolor es crónico y generalizado y se acompaña de fatiga intensa durante el día y dificultad para dormir 
                       por la noche.
                    </p>
                    <p>El nombre de fibromialgia deriva de "fibro", o tejidos fibrosos (tales como tendones y ligamentos). Por lo tanto, suele 
                        ir acompañada de sensación de rigidez de intensidad variable en los músculos, tendones y tejido blando, así como un 
                        amplio rango de otros síntomas. </p>
                    <p>
                        Se considera que la fibromialgia es la causa más frecuente de dolor musculoesquelético generalizado. Durante mucho tiempo 
                        se ha asignado a la fibromialgia el carácter de enfermedad psicológica o psicosomática41. Sin embargo, a través de diversos
                        estudios de resonancia magnética realizados en pacientes con esta enfermedad se comprobó que los dolores son reales y se
                        manifiestan en zonas específicas del cerebro.
                    </p>
                    <p>
                        La fibromialgia no es contagiosa. La padece entre el tres y el seis por ciento de la población mundial y es más frecuente 
                        en personas entre los 20 y 50 años, la mayoría de género femenino.
                    </p>
                </div>
                <div class="col-md-6 image_section">
                    <img src="img/fibro2.png" >
                </div>
                <div class="col-md-6">
                    </br></br></br>
                    <p>
                        La Organización Mundial de la Salud la reconoció como enfermedad desde 1992 y existen claros consensos internacionales 
                        de criterios diagnósticos para un reconocimiento oportuno y apropiado.
                    </p>
                </div>
            </div>
            
            <div class="row info">
                <div class="col-md-12">
                    <h2>¿Cómo se manifiesta?</h2>
                    <div class="row fila-slider">
                        <div class="col-md-12">
              <div id="myCarouselPiel1" class="carousel slide pfizer-carousel" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators hidden-xs">
                <li data-target="#myCarouselPiel1" data-slide-to="0"></li>
                <li data-target="#myCarouselPiel1" data-slide-to="1"></li>
                <li data-target="#myCarouselPiel1" data-slide-to="2"></li>
                <li data-target="#myCarouselPiel1" data-slide-to="3"></li>
                <li data-target="#myCarouselPiel1" data-slide-to="4"></li>
               
            </ol>
            <div class="col-sm-7 pfizer-slider">
                <div class="carousel-inner" role="listbox">
                    <div class="item active" style="background:#33D7F2;">
                        <!-- <img class="first-slide" src="images/WB26_003.jpg" alt="First slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_2">
                                  <p>
                                    Presente en el 100 por ciento de los casos, de localización diversa e intensidad fluctuante. Mayor al despertar pero 
                            no cede con el reposo.
                                  </p>
                                
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#25BBCA;">
                        <!-- <img class="second-slide" src="images/WB33_009.jpeg" alt="Second slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_4">
                                <p>
                                 En casi el 80 por ciento de los pacientes, se exacerba con el esfuerzo y tampoco mejora con el reposo. 
                            La fatiga en grado extremo está presente en todas las actividades que realizan las personas con fibromialgia, por lo que sus 
                            tareas cotidianas se ven inevitablemente limitadas. Dependiendo de la gravedad y de la variación del grado, este cansancio 
                            puede oscilar desde soportable hasta una discapacidad casi infranqueable que limita sus tareas tanto dentro del ámbito 
                            familiar como profesional. Aceptar estas importantes limitaciones es difícil, sobre todo en los inicios de la enfermedad, 
                            y muchos enfermos/as tardan en lograrlo. Unido inseparablemente a este cansancio, como causa que lo aumenta y agrava, es la 
                            mala calidad del sueño de los pacientes, que impide llegar a la fase más profunda de éste, no pudiendo realizarse las 
                            funciones reparadoras normales del organismo, siendo muy difícil el descanso.
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#2394A4;">
                        <!-- <img class="third-slide" src="images/CW40_085.jpg" alt="Third slide"> -->
                        
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_1">
                                <p>Son muy frecuentes. Se presentan como sueño no reparador, ligero e inestable (despertares frecuentes) 
                            y pesadillas. Los pacientes con mala calidad de sueño presentan a su vez mayor sensibilidad al dolor, y síntomas de ansiedad 
                            y depresión. Estas perturbaciones se traducen en una disminución de la calidad de vida.</p>
                                
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#13717D;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                            <div class="carousel-caption ">
                              <div class="carousel-caption-text cardio_text_2">
                                 <p>
                                  Hormigueos de extremidades y mayor sensibilidad a la luz y a los ruidos.
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#0F363D;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                            <div class="carousel-caption ">
                              <div class="carousel-caption-text cardio_text_2">
                                 <p>
                                 Como ansiedad, labilidad emocional, déficit de concentración y memoria.
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="hidden-xs col-sm-5 pfizer-slider-indicators" >
                <ol class="carousel-indicators">
                    <li class="botones5 slide1 pf-on" data-target="#myCarouselPiel1" data-slide-to="0"><p>Dolor</p></li>
                    <li class="botones5 slide2 apagado pf-off" data-target="#myCarouselPiel1" data-slide-to="1"><p>Fatiga o cansancio</p></li>
                    <li class="botones5 slide3 apagado pf-off" data-target="#myCarouselPiel1" data-slide-to="2"><p>Trastornos del sueño</p></li>
                    <li class="botones5 slide4 apagado pf-off" data-target="#myCarouselPiel1" data-slide-to="3"><p>Síntomas sensitivos</p></li>
                    <li class="botones5u slide5 apagado pf-off" data-target="#myCarouselPiel1" data-slide-to="4"><p>Síntomas psicoafectivos</p></li>
                    </ol>

            </div>
            <a class="left carousel-control visible-xs" href="#myCarouselPiel1" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left visi" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control visible-xs" href="#myCarouselPiel1" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
        </div>
            </div>
            </div>
                  
                </div>
            </div>
            <div class="row info">
                <div class="col-md-12">
                <h2>Causas de la enfermedad</h2>
                </div>
                    <div class="col-md-6 justificar menos_espacio">
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        La causa última de la fibromialgia es aún desconocida y objeto de investigación. Los estudios médicos se han orientado a observar si 
                        hay lesiones en los músculos, alteraciones inmunológicas, psicológicas, problemas hormonales,  alteraciones  en  la  fisiología  del  
                        sueño  o  en  los  mecanismos protectores del dolor, pero entre todas estas hipótesis la más aceptada es la de un aumento de la 
                        sensibilidad al dolor de tipo neurológico.
                    </p>
                    <p>
                        Algunos de los casos reconocen un origen, como infecciones virales o bacterianas, accidentes de automóvil, entre otros. La 
                        ansiedad y la depresión causada por: separación  matrimonial,  problemas  con los  hijos,  pérdida de empleo, fracaso profesional, 
                        se puede asociar a la tristeza que se produce por el malestar continuo que provoca la enfermedad.
                    </p>
                </div>
                <div class="col-md-6">    
                    <p>
                        En ocasiones, la fibromialgia acompaña enfermedades que debilitan el organismo como artritis reumatoide, lupus, entre otras, que 
                        alteran la regulación de la capacidad de respuesta a determinados estímulos dolorosos.
                    </p>
                    <p>
                        Como mencionamos, dentro de las teorías actuales sobre la causa de esta enfermedad se encuentran las alteraciones en el 
                        funcionamiento del sistema nervioso y fenómenos de percepción anormal del dolor (sensibilización). De esta manera se integran 
                        teorías que abarcan las diferentes (y complejas) relaciones entre los centros de procesamiento de las vías del dolor (fenómenos 
                        de sensibilización central y periférica del dolor) y el sistema nervioso.
                    </p>
            
                </div>
            </div>
            <div class="row info hidden-xs">
                <div class="col-md-5">
                 
                    <div class= "col-md-6 botonera boton_dolor1 pf-on1">
                      <figure>
                        <img src="img/pin56.png" width="100px" height="100px" />
                      </figure>
                    </div>
                      <div class= "col-md-6 botonera boton_dolor2 apagado pf-off1">
                      <figure>
                        <img src="img/pill11.png" width="100px" height="100px" />
                      </figure>
                    </div>
                  
                  
                    <div class= "col-md-6 botonera boton_dolor3 apagado pf-off1">
                      <figure>
                        <img src="img/medical51.png" width="100px" height="100px" />
                      </figure>
                    </div>
                
                  
                </div>
                <div class="col-md-7 dolor_info info_dolor1 ">
                    <h2>Puntos dolorosos</h2>
                    <p>Es una enfermedad que causa dolor en los músculos, articulaciones, ligamentos y tendones. Existen áreas llamadas puntos 
                        "hipersensibles" o "dolorosos". Los puntos dolorosos comunes se encuentran ubicados en la parte interna de las rodillas, 
                        los codos,  las  articulaciones  de  la  cadera  y alrededor del cuello.</p>

                    </p>
                </div>
                
         

                <div class="col-md-7 dolor_info info_dolor2">
                    <h2>Tratamiento</h2>
                    <p>Como toda enfermedad crónica el abordaje terapéutico es multimodal, es decir que incluya en forma personalizada tanto los 
                        aspectos orgánicos como los psicosociales.
                    </p>
                    <p>
                      Los objetivos terapéuticos son: mitigar el dolor y la fatiga, mejorar el movimiento y la fuerza muscular, controlar el estrés y 
                      los estados de ánimo. La psicoeducación y grupos de autoayuda son esenciales a la hora de esclarecer la naturaleza y manejo de la 
                      enfermedad.
                    </p>
                    <p>
                       Lejos de ser una enfermedad incurable, en la actualidad la fibromialgia debe considerarse como un desafío terapéutico alcanzable 
                       por un equipo experto y dedicado a esta compleja condición. 

                    </p>
                </div>
                <div class="col-md-7 dolor_info info_dolor3">
                    <h2>Diagnóstico y Evolución</h2>
                    <p>El diagnóstico es básicamente clínico, es decir, dolor crónico generalizado de más de tres meses de duración asociado a los 
                        síntomas antes mencionados. Para diagnosticar la fibromialgia, el médico debe valorar la historia clínica, así como los síntomas, 
                        estudios y análisis descartando otras patologías con síntomas similares.</p>
                </div>
            </div>
            <div class="visible-xs">
            <div class="boton-wide boton_dolor1">
              <figure>
                <img src="img/pin56.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                <h2>Puntos dolorosos</h2>
                    <p>Es una enfermedad que causa dolor en los músculos, articulaciones, ligamentos y tendones. Existen áreas llamadas puntos 
                        "hipersensibles" o "dolorosos". Los puntos dolorosos comunes se encuentran ubicados en la parte interna de las rodillas, 
                        los codos,  las  articulaciones  de  la  cadera  y alrededor del cuello.</p>

                    </p>
              </div>
            </div>
            <div class="boton-wide boton_dolor2">
              <figure>
                <img src="img/medical50.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
               <h2>Tratamiento</h2>
                    <p>Como toda enfermedad crónica el abordaje terapéutico es multimodal, es decir que incluya en forma personalizada tanto los 
                        aspectos orgánicos como los psicosociales.
                    </p>
                    <p>
                      Los objetivos terapéuticos son: mitigar el dolor y la fatiga, mejorar el movimiento y la fuerza muscular, controlar el estrés y 
                      los estados de ánimo. La psicoeducación y grupos de autoayuda son esenciales a la hora de esclarecer la naturaleza y manejo de la 
                      enfermedad.
                    </p>
                    <p>
                       Lejos de ser una enfermedad incurable, en la actualidad la fibromialgia debe considerarse como un desafío terapéutico alcanzable 
                       por un equipo experto y dedicado a esta compleja condición. 

                    </p>
              </div>
            </div>
            <div class="boton-wide boton_dolor3">
              <figure>
                <img src="img/pill11.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                 <<h2>Diagnóstico</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Historia clínica: antecedentes personales y familiares. El examen físico puede mostrar una acumulación 
                       de líquido sinovial alrededor de la articulación (derrame).
                    </p>
                    <h2>Pruebas diagnósticas:</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       <ul>
                           <li>
                               Una prueba de factor reumatoide y una de anticuerpos antinucleares, las cuales pueden ayudar a 
                               los médicos a verificar que es efectivamente la enfermedad.
                           </li>
                           <li>
                               Pruebas de sangre como velocidad de sedimentación globular (VSG) y proteína C reactiva (PCR), 
                               las cuales miden el grado de inflamación en su cuerpo.
                           </li>
                           <li>Radiografías de las articulaciones afectadas para ayudar a su médico a determinar el grado del 
                            daño, si existe, y medir el progreso de su enfermedad.</li>
                            <li>Exploración de las articulaciones dolorosas, dedos en forma de salchicha, etc.</li>
                            <li>EI índice de Actividad de la Enfermedad (DAS 28) mide la actividad de la enfermedad en 28 
                                articulaciones. EI DAS 28 facilita a los médicos el diagnóstico y evalúa que tan activa está.</li>
                            <li>El HAQ se enfoca en la incapacidad y la limitación funcional.</li>
                       </ul>
                    </p>
              </div>
            </div>
            
            
        </div>
            <div class="row info">
                <div class="col-md-12">
                <h2>Síntomas</h2>
                </div>
                <div class="col-md-6">
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       La fibromialgia ocasiona dolor músculoesquelético generalizado, debilitamiento intenso y hasta incapacitante, fatiga, trastornos 
                       del sueño, en ocasiones alteraciones del ritmo intestinal, rigidez en las extremidades y muy frecuentemente episodios depresivos 
                       acompañados de crisis de ansiedad.
                    </p>
                    <p>
                        Los sitios frecuentes en los cuales se presenta el dolor de la fibromialgia incluyen la espalda, como la región lumbar (espalda 
                        baja), el cuello, tórax y muslos. En algunos casos se observa espasmo muscular localizado.
                    </p>
                    <p>
                        No hay que perder de vista que en la fibromialgia duele todo el cuerpo. Hay dolor arriba y debajo de la cintura, 
                    </p>
                </div>
                <div class="col-md-6">
                    <p>
                        del lado izquierdo y derecho del cuerpo.Los trastornos del sueño son muy frecuentes en pacientes con dicha patología. Estos trastornos consisten 
                        básicamente en sueño no reparador entre otras manifestaciones.</br></br>

                       Otros síntomas adicionales pueden incluir malestar urinario, dolor de cabeza como la migraña, movimientos periódicos anormales de 
                       las extremidades, en especial de las piernas, dificultad de concentración y dificultad para recordar cosas; también es frecuente 
                       un aumento de la sensibilidad táctil, a la luz, sonidos y olores, prurito generalizado, sequedad de ojos y boca, alteraciones en
                        la audición o la visión y algunos síntomas neurológicos de incoordinación motora.
                    </p>
                </div>
             </div>
            
             
             <div class="row info">
                <div class="col-md-12"><h2>Acción terapéutica</h2></div>
                <div class="col-md-6">
                    <p>¿Existen medicamentos que ayuden a aliviar los síntomas de la fibromialgia?
                    </p>
                    <p>
                      SI. Varios medicamentos pueden ayudar a aliviar los síntomas de esta enfermedad.
                    </p>
                    <p>
                       Existen estudios que demostraron que algunos de estos medicamentos, podrían reducir los signos y síntomas de la fibromialgia 
                       sobre todo el dolor, la fatiga, los trastornos de sueño, y la calidad de vida. Se debe consultar al médico para saber cuál de 
                       los medicamentos es el adecuado.
                    </p>
                    <p>
                        No se debe olvidar que, el manejo de la fibromialgia se basa en tres aspectos principales (ejercicio, psicoterapia y fármacos). 
                        En la actualidad se considera que el tratamiento que debe llevarse a cabo es MULTIDISCIPLINARIO, colaborando varias especialidades 
                        médicas.
                    </p>
                    <p>
                        Además del tratamiento médico, ¿qué más se puede hacer para aliviar los síntomas?
                    </p>
                </div>
                <div class="col-md-6">
                    <p>
                        Una de las mejores opciones es hacer ejercicios aeróbicos de bajo impacto. Otras buenas alternativas incluyen los masajes, 
                        hacer ejercicios de estiramiento, practicar yoga o tai-chi.
                    </p>
                    <p>
                        Debido a que los síntomas de fibromialgia empeoran con el estrés y poco descanso, es importante disminuir el estrés y dormir 
                        el tiempo necesario.
                    </p>
                    <p>
                        El alcohol y la cafeína empeoran la calidad del sueño, por lo tanto, se deben evitar estas substancias al acercarse la hora 
                        de irse a dormir.
                    </p>
                    <p>
                        Otros cambios simples en el estilo de vida pueden ser útiles. Por ejemplo, tratar de mantener el nivel de actividad igual todos 
                        los días. Muchas personas con fibromialgia tratan de hacer todo lo que pueden en los "días buenos", lo cual hace que tengan varios 
                        "días malos". Si se mantiene el nivel de actividad parejo es posible que no se tengan tantos "días malos". Muchas personas 
                        descubren que seguir una rutina para comer, dormir y hacer ejercicio les ayuda a aliviar sus síntomas.

                    </p>
                </div>
            </div>
        </div>
    </div>
      <?php include 'footer.php' ?>
   
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>
    <script>
     $( ".boton_dolor1" ).on( "click", function() {
         $('.info_dolor1').css('display', 'block');
         $('.info_dolor2').css('display', 'none');
         $('.info_dolor3').css('display', 'none');
         
         
      });
     $( ".boton_dolor2" ).on( "click", function() {
         $('.info_dolor1').css('display', 'none');
         $('.info_dolor2').css('display', 'block');
         $('.info_dolor3').css('display', 'none');
         
      });
     $( ".boton_dolor3" ).on( "click", function() {
         $('.info_dolor1').css('display', 'none');
         $('.info_dolor2').css('display', 'none');
         $('.info_dolor3').css('display', 'block');
       
      });
    $( ".boton_dolor4" ).on( "click", function() {
         $('.info_dolor1').css('display', 'none');
         $('.info_dolor2').css('display', 'none');
         $('.info_dolor3').css('display', 'none');
       
      });
     

    </script>
    </script>
<script>
  $(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            interval: false
        });
    });
});​
</script>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>

<script>
$(document).ready(function(){

    $( "body" ).on('click', '.pf-off', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on").addClass( "apagado" );

        $(this).removeClass( "pf-off" );
        $(".pf-on").addClass( "pf-off" );
        $(".pf-on").removeClass( "pf-on" );
        $(this).addClass( "pf-on" );    });

    $( "body" ).on('click', '.pf-off1', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on1").addClass( "apagado" );

        $(this).removeClass( "pf-off1" );
        $(".pf-on1").addClass( "pf-off1" );
        $(".pf-on1").removeClass( "pf-on1" );
        $(this).addClass( "pf-on1" );    });
});
</script>
</body>
</html>