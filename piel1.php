<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    
    <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/piel-banner.jpg" >
     <?php include 'mainnav.php' ?>
        <div class="container title">
         <h1 class="heading-interno">PIEL Y ARTICULACIONES</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
<div  class="container-fluid white pad-30">
        <div id="vida"></div>
        <div class="container">
            <ol class="breadcrumb">
            <li><a href="piel.php">Piel y Articulaciones</a></li>
            <li class="active">Psoriasis</li>
          </ol>
                <!-- <h1>DIFTERIA</h1> -->
            <div class="row info">
                <div class="col-md-6 que-es-vacunas justificar menos_espacio">
                    <h2>¿Qué es la psoriasis?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        La psoriasis es una enfermedad inflamatoria crónica de la piel, ocasionada por un desorden del sistema 
                        inmunológico, que afecta el proceso de renovación de la piel.
                    </p>
                    <p>
                        La puede padecer por igual los niños y adultos, tanto de sexo femenino como masculino, las células de la piel se 
                        desarrollan más rápido de lo normal, acumulándose en la superficie de la piel y dando como resultado la formación 
                        de escamas blanquecinas sobre manchas rojizas que constituyen la denominada “placa psoriásica”. Es de naturaleza genética, 
                        por lo que tiene un fundamento hereditario y no es contagiosa, por lo que no se transmite si se toca a alguien con psoriasis.
                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6">
                    <img class="img-2" src="img/piel-psoriasis-interior.jpg" />
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h2>Tipos de psoriasis</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->

                    <div class="fila-slider">
              <div id="myCarouselPiel1" class="carousel slide pfizer-carousel" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators hidden-xs">
                <li data-target="#myCarouselPiel1" data-slide-to="0"></li>
                <li data-target="#myCarouselPiel1" data-slide-to="1"></li>
                <li data-target="#myCarouselPiel1" data-slide-to="2"></li>
                <li data-target="#myCarouselPiel1" data-slide-to="3"></li>
                <li data-target="#myCarouselPiel1" data-slide-to="4"></li>
               
            </ol>
            <div class="col-sm-7 pfizer-slider">
                <div class="carousel-inner" role="listbox">
                    <div class="item active" style="background:#33D7F2;">
                        <!-- <img class="first-slide" src="images/WB26_003.jpg" alt="First slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_2">
                                  <p>
                                    Es la forma más frecuente, consiste en parches o placas gruesas bien delimitadas, de color plateado- blanco, enrojecidas 
                                        y descamativas. Las zonas de afección más frecuentes son: codos, rodillas, pliegues de extensión de miembros, sacro 
                                        (espalda baja), cuero cabelludo, palmas de las manos y plantas de los pies.
                                  </p>
                                
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#25BBCA;">
                        <!-- <img class="second-slide" src="images/WB33_009.jpeg" alt="Second slide"> -->
                        <div class="container">
                            <div class="carousel-caption"> 
                              <div class="carousel-caption-text cardio_text_2">
                                <p>
                                  Generalmente es precedida por un proceso infeccioso. Las lesiones pueden ser poco características y consisten en pápulas 
                                            eritematosas (rojas) y descamativas.
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#2394A4;">
                        <!-- <img class="third-slide" src="images/CW40_085.jpg" alt="Third slide"> -->
                        
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_2">
                                <p>En este caso el enrojecimiento e irritación de la piel ocurre en las axilas, la ingle, la región submamaria y el ombligo.
                                </p>
                                
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#13717D;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_2">
                                 <p> 
                                  El paciente puede presentar lesiones pustulosas, que suelen localizarse en palmas de las manos y plantas de los pies, o 
                                    generalizadas y acompañadas de fiebre, malestar y diarrea.
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#0F363D;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_2">
                                 <p>
                                  Tiene una presentación generalizada y es una forma grave de psoriasis. Se presenta como lesiones extensas que compromete 
                                    más del 80% de la superficie corporal. Generalmente se presenta en pacientes que tienen una enfermedad crónica previa.
                                    
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
                <div class="hidden-xs col-sm-5 pfizer-slider-indicators" >
                    <ol class="carousel-indicators">
                        <li class="botones5 slide1 pf-on" data-target="#myCarouselPiel1" data-slide-to="0" ><p>Psoriasis en placas</p></li>
                        <li class="botones5 slide2 apagado pf-off" data-target="#myCarouselPiel1" data-slide-to="1" ><p>Psoriasis en gotas o eruptiva</p></li>
                        <li class="botones5 slide2 apagado pf-off" data-target="#myCarouselPiel1" data-slide-to="2" ><p>Psoriasis inversa</p></li>
                        <li class="botones5 slide2 apagado pf-off" data-target="#myCarouselPiel1" data-slide-to="3" ><p>Psoriasis pustulosa</p></li>
                        <li class="botones5u slide2 apagado pf-off" data-target="#myCarouselPiel1" data-slide-to="4" ><p>Psoriasis eritrodérmica</p></li>
                        </ol>

                </div>
                <a class="left carousel-control visible-xs" href="#myCarouselPiel1" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left visi" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control visible-xs" href="#myCarouselPiel1" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        </div>
            </div>
            <div class="row info hidden-xs">
                <div class="col-md-12">
                    <div class="col-md-5">
                        <div class="row">
                            <div class= "col-md-7 botonera boton_piel1 pf-on1">
                                <figure>
                                    <img src="img/heart288.png" width="100px" height="100px" />
                                </figure>
                            </div>
                            <div class= "col-md-7 botonera boton_piel2 apagado pf-off1">
                                <figure>
                                    <img src="img/message30.png" width="100px" height="100px" />
                                </figure>
                            </div>
                        </div>
                        <div class="row">
                            <div class= "col-md-7 botonera boton_piel3 apagado pf-off1">
                                <figure>
                                    <img src="img/medical14.png" width="100px" height="100px" />
                                </figure>
                            </div>
                            <div class= "col-md-7 botonera boton_piel4 apagado pf-off1">
                                <figure>
                                    <img src="img/medical51.png" width="100px" height="100px" />
                                </figure>
                            </div>
                        </div>
                        <div class="row">
                            <div class= "col-md-7 botonera boton_piel5 apagado pf-off1">
                                <figure>
                                    <img src="img/exclamationmark.png" width="100px" height="100px" />
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 piel_info info_piel1">
                    <h2>Signos</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        Lo primero que aparece en la piel es una serie de granitos rojos que van formando parches llamados placas. Estas superficies son 
                        gruesas y escamosas, adoptando un color grisáceo a medida que crecen.
                    </p>
                    <p>
                        Las áreas específicas que tienen lesiones o están afectadas por el sol, especialmente pueden ser codos y rodillas. También pueden 
                        aparecer en cuero cabelludo, en la zona que rodea el ombligo, las nalgas y los genitales, incluso en las uñas.
                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6 piel_info info_piel2">
                    <h2>Causas de la Psoriasis</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        La causa exacta de la psoriasis es desconocida, se sabe que es un trastorno autoinmune, es decir, del sistema inmunológico, por lo 
                        que se dice que la misma comienza dentro del cuerpo. La sobre activación de su sistema inmune puede ser el causante de que las 
                        células de la piel crezcan demasiado rápido por un proceso inflamatorio y aparezcan como placas rojas y escamosas sobre la 
                        superficie de la piel.
                    </p>
                    <p>
                        Sin embargo, hay factores que pueden desencadenar un brote o hacer que la afección sea más difícil de tratar:
                        <ul>
                            <li>
                                Bacterias (como Estreptococo) infecciones virales.
                            </li>
                            <li>Situaciones estresantes: que es difícil demostrar y controlar en los niños.</li>
                            <li>Traumatismos físicos que generen reacciones en la piel, como golpes o cortaduras.</li>
                            <li>Poca o demasiada luz solar (quemaduras solares).</li>
                            <li>Clima: el clima frío y seco tiene un efecto adverso sobre la psoriasis; mientras que el clima cálido y húmedo tiene 
                                un efecto benéfico.</li>
                            <li>Piel seca.</li>
                        </ul>
                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6 piel_info info_piel3">
                    <h2>Síntomas</h2>
                    <ul>
                        <li>Descamación</li>
                        <li>Prurito (picazón)</li>
                        <li>Enrojecimiento de la piel</li>
                        <li>Rigidez de la piel</li>
                        <li>Sangrado de las lesiones</li>
                        <li>Sensación de ardor</li>
                        <li>Fatiga</li>
                        <li>Depresión, baja autoestima</li>
                    </ul>
                </div>
                <div class="col-md-6 piel_info info_piel4">
                    <h2>Diagnóstico</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Es importante que un médico, de preferencia un dermatólogo haga el diagnóstico de psoriasis. Para ello el especialista deberá evaluar:
                       <ul>
                            <li>Historia Clínica: se evalúan antecedentes personales y familiares.</li>
                            <li>Biopsia de piel (si se considera necesario).</li>
                            <li>Exámenes de laboratorio.</li>
                            <li>Examen físico.</li>
                            <li>Radiologías.</li>
                       </ul>
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6 piel_info info_piel5">
                    <h2>Factores de Riesgo</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        Los siguientes factores aumentan el riesgo de desarrollar psoriasis:
                        <ul>
                            <li>
                                Tener un familiar con psoriasis: 
                                <ul><li class="lista-sin-estilo">
                                    Es el principal factor de riesgo para desarrollar la enfermedad. Se estima que una de cada 3 personas con psoriasis tiene 
                                    un familiar cercano con esta enfermedad.
                                </li></ul>
                            </li>
                            <li>
                                Padecer otras enfermedades:
                                <ul><li class="lista-sin-estilo">
                                    Infecciones recurrentes, HIV (SIDA), entre otras.
                                </li></ul>
                            </li>
                            <li>
                                Cambios emocionales frecuentes.
                            </li>
                            <li>
                                Obesidad
                            </li>
                            <li>
                                Fumar cigarrillo
                            </li>
                        </ul>
                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
        </div>
            <div class="visible-xs">
            <div class="boton-wide boton_piel1">
              <figure>
                <img src="img/heart288.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                <h2>Signos</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        Lo primero que aparece en la piel es una serie de granitos rojos que van formando parches llamados placas. Estas superficies son 
                        gruesas y escamosas, adoptando un color grisáceo a medida que crecen.
                    </p>
                    <p>
                        Las áreas específicas que tienen lesiones o están afectadas por el sol, especialmente pueden ser codos y rodillas. También pueden 
                        aparecer en cuero cabelludo, en la zona que rodea el ombligo, las nalgas y los genitales, incluso en las uñas.
                    </p>
              </div>
            </div>
            <div class="boton-wide boton_piel2">
              <figure>
                <img src="img/message30.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
               <h2>Causas de la Psoriasis</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        La causa exacta de la psoriasis es desconocida, se sabe que es un trastorno autoinmune, es decir, del sistema inmunológico, por lo 
                        que se dice que la misma comienza dentro del cuerpo. La sobre activación de su sistema inmune puede ser el causante de que las 
                        células de la piel crezcan demasiado rápido por un proceso inflamatorio y aparezcan como placas rojas y escamosas sobre la 
                        superficie de la piel.
                    </p>
                    <p>
                        Sin embargo, hay factores que pueden desencadenar un brote o hacer que la afección sea más difícil de tratar:
                        <ul>
                            <li>
                                Bacterias (como Estreptococo) infecciones virales.
                            </li>
                            <li>Situaciones estresantes: que es difícil demostrar y controlar en los niños.</li>
                            <li>Traumatismos físicos que generen reacciones en la piel, como golpes o cortaduras.</li>
                            <li>Poca o demasiada luz solar (quemaduras solares).</li>
                            <li>Clima: el clima frío y seco tiene un efecto adverso sobre la psoriasis; mientras que el clima cálido y húmedo tiene 
                                un efecto benéfico.</li>
                            <li>Piel seca.</li>
                        </ul>
                    </p>
              </div>
            </div>
            <div class="boton-wide boton_piel3">
              <figure>
                <img src="img/medical14.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                 <h2>Síntomas</h2>
                    <ul>
                        <li>Descamación</li>
                        <li>Prurito (picazón)</li>
                        <li>Enrojecimiento de la piel</li>
                        <li>Rigidez de la piel</li>
                        <li>Sangrado de las lesiones</li>
                        <li>Sensación de ardor</li>
                        <li>Fatiga</li>
                        <li>Depresión, baja autoestima</li>
                    </ul>
              </div>
            </div>
            <div class="boton-wide boton_piel4">
              <figure>
                <img src="img/medical51.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                 <h2>Diagnóstico</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Es importante que un médico, de preferencia un dermatólogo haga el diagnóstico de psoriasis. Para ello el especialista deberá evaluar:
                       <ul>
                            <li>Historia Clínica: se evalúan antecedentes personales y familiares.</li>
                            <li>Biopsia de piel (si se considera necesario).</li>
                            <li>Exámenes de laboratorio.</li>
                            <li>Examen físico.</li>
                            <li>Radiologías.</li>
                       </ul>
                    </p>
              </div>
            </div>
            <div class="boton-wide boton_piel5">
              <figure>
                <img src="img/exclamationmark.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                 <h2>Factores de Riesgo</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        Los siguientes factores aumentan el riesgo de desarrollar psoriasis:
                        <ul>
                            <li>
                                Tener un familiar con psoriasis: 
                                <ul><li class="lista-sin-estilo">
                                    Es el principal factor de riesgo para desarrollar la enfermedad. Se estima que una de cada 3 personas con psoriasis tiene 
                                    un familiar cercano con esta enfermedad.
                                </li></ul>
                            </li>
                            <li>
                                Padecer otras enfermedades:
                                <ul><li class="lista-sin-estilo">
                                    Infecciones recurrentes, HIV (SIDA), entre otras.
                                </li></ul>
                            </li>
                            <li>
                                Cambios emocionales frecuentes.
                            </li>
                            <li>
                                Obesidad
                            </li>
                            <li>
                                Fumar cigarrillo
                            </li>
                        </ul>
                    </p>
              </div>
            </div>
        </div>
            
            <div class="row info">
                <div class="col-md-6 transmite-vacunas">
                    <h2>Recomendaciones</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Sea portavoz de la información
                       <ul>
                            <p class="parrafo">
                                <li>Intente hablarle regularmente a un amigo o miembro de la familia, mantenerse en contacto Ie facilitará comentar como lo 
                                    afecta esta condición.</li>
                                <li>Aprenda sobre la enfermedad, entre más conozca usted sobre el tema, mejor lo podrá explicar.</li>
                            </p>
                
                
                            <p class="parrafo2">
                                <li>Sea directo, dígales a sus amigos y familiares lo que ustedes necesitan.</li>
                                <li>Un buen sentido del humor puede ser benéfico.</li>
                            </p>
                        </ul>
                    </p>
                </div>
                <div class="col-md-6">
                    </br></br>
                        <p>
                            ¡Diviértanse diariamente!
                            <ul>
                                <li>
                                    Acuérdese de reír. De ejemplo a su hijo, ría todo lo que pueda y procure mantener el sentido del humor. Está demostrado 
                                    científicamente que la risa ayuda a fortalecer el sistema inmunológico.
                                </li>
                                <li>
                                    Encuentre apoyo emocional, la conversación con un terapeuta o la incorporación a un grupo de apoyo formado por personas 
                                    que comprendan los desafíos que presenta la psoriasis pueden ser beneficiosos para muchos niños que sufren problemas 
                                    emocionales debido a la psoriasis.
                    
                                </li>
                                <li>
                                    Recuérdele que mantenga su piel limpia y bien humectada. Los baños diarios con sales de baño o aceites y la aplicación 
                                    posterior de un humectante pueden ayudarlo a aliviar los síntomas de la psoriasis. Pase tiempo con el niño al aire libre.
                                    La exposición breve a la luz natural puede ayudar a mejorar la psoriasis

                                </li>
                            </ul>
                        </p>
                </div>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                
            </div>
        </div>
    </div>
<?php include 'footer.php' ?>
   
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>

    <script>
     $( ".boton_piel1" ).on( "click", function() {
         $('.info_piel1').css('display', 'block');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'none');
         $('.info_piel5').css('display', 'none');
         
         
      });
     $( ".boton_piel2" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'block');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'none');
         $('.info_piel5').css('display', 'none');
         
      });
     $( ".boton_piel3" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'block');
         $('.info_piel4').css('display', 'none');
         $('.info_piel5').css('display', 'none');
         
         
      });
    $( ".boton_piel4" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'block');
         $('.info_piel5').css('display', 'none');
         
         
      });
     $( ".boton_piel5" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'none');
         $('.info_piel5').css('display', 'block');
         
         
      });
     

    </script>
    <script>
  $(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            interval: false
        });
    });
});​
</script>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>

<script>
$(document).ready(function(){

    $( "body" ).on('click', '.pf-off', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on").addClass( "apagado" );

        $(this).removeClass( "pf-off" );
        $(".pf-on").addClass( "pf-off" );
        $(".pf-on").removeClass( "pf-on" );
        $(this).addClass( "pf-on" );    });

    $( "body" ).on('click', '.pf-off1', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on1").addClass( "apagado" );

        $(this).removeClass( "pf-off1" );
        $(".pf-on1").addClass( "pf-off1" );
        $(".pf-on1").removeClass( "pf-on1" );
        $(this).addClass( "pf-on1" );    });
});
</script>
</body>
</html>