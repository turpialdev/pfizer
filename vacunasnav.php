 <div class="row row-interno vacunas-fondo " role="tabpanel tab-interno">
          
          <ul class="nav nav-tabs tabs-custom-interno" role="tablist">
                <li role="presentation" class="active"><a href="#bacteriana" aria-controls="bacteriana" role="tab" data-toggle="tab">Enfermedades Bacterianas Prevenibles por Vacunas</a></li>
                <li role="presentation"><a href="#virales" aria-controls="virales" role="tab" data-toggle="tab">Enfermedades Virales Prevenibles por Vacunas</a></li>      
          </ul>
          <div class="tab-content tab-content-interno">
                <div role="tabpanel" class="tab-pane active" id="bacteriana"><?php include 'bacteriana.php' ?></div>
                <div role="tabpanel" class="tab-pane" id="virales"><?php include 'virales.php' ?></div>
              </div> 

</div>
