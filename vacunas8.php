<div  class="container-fluid padding-vacunas-interno">
      
            
                <!-- <h1>PAROTIDITIS</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>¿Qué es la parotiditis?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>  La parotiditis es una infección viral que puede hacer que las glándulas salivales se hinchen.

                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h2>¿De qué manera la parotiditis  puede afectar a mi hijo?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                             La parotiditis puede provocar:
                            <ul>
                                <li>Dolor de oídos y sensibilidad debajo de la mandíbula.</li>
                                <li>Pancreatitis</li>
                                <li>Meningitis con o sin síntomas. La meningitis causada por la parotiditis, por lo general, no ocasiona una enfermedad 
                                    grave. </li>
                            </ul>
                            </li>
                            <li>
                                La parotiditis también puede provocar inflamación de ovarios en las niñas o de testículos en los niños; en raras ocasiones, 
                                esto puede causar infertilidad.

                            </li>
                            <li>
                                En casos infrecuentes, la parotiditis puede ocasionar complicaciones graves como: encefalitis o pérdida de la audición, 
                                por lo general, en los adultos.

                            </li>
                           
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h2>¿Mi hijo está en riesgo?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h2>¿Cómo se transmite?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La  parotiditis se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                            <li>También se puede transmitir por contacto directo con saliva o gotitas infectadas.</li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
