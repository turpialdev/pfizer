<div  class="container-fluid padding-vacunas-interno">
       
            
                <!-- <h1>RUBEOLA</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>rubeola</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>  La rubeola es una infección viral frecuentemente conocida como “sarampión alemán”.

                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h4>¿De qué manera el rotavirus puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                             La rubeola puede provocar una erupción en la cara y el cuerpo, y fiebre baja.
                            </li>
                            <li>En ocasiones infrecuentes las personas con rubeola desarrollan problemas graves relacionados con la enfermedad </li>
                            <li>Sin embargo, la rubeola es peligrosa para las mujeres embarazadas, especialmente en el primer trimestre, porque provoca 
                                defectos congénitos.
                                 <ul>
                                    <li>Los lactantes con rubeola pueden transmitir la enfermedad a otras personas, lo que incluye a las mujeres embarazadas.</li>
                                </ul>
                            </li>
                               
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                           <li>La rubeola se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
