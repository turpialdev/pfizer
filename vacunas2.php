<div  class="container-fluid padding-vacunas-interno">
       
            
                <!-- <h1>HAEMOPHILUS INFLUENZAE TIPO B (Hib)</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>Haemophilus influenzae tipo b (Hib)</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> El Hib (Haemophilus influenzae tipo b) es una bacteria que generalmente provoca enfermedades graves en lactantes y 
                                niños menores.
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h4>¿El Hib puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                              Puede ocasionar enfermedades graves como:
                              <ul>
                                <li>Meningitis </li>
                                <li>Neumonía</li>
                                <li>Inflamación intensa de la garganta (epiglotitis)</li>
                                <li>Otitis</li>
                                <li>Sinusitis</li>
                              </ul>
                            </li>
                            <li>
                                La infección por Hib puede tener consecuencias graves que incluyen:
                                <ul>
                                    <li>Infecciones cardíacas, articulares, óseas o cutáneas.</li>
                                    <li>Pérdida de la audición, lesión cerebral o muerte.</li>
                                </ul>

                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                              Se transmite a través del aire al:
                              <ul>
                                <li>Toser </li>
                                <li>Estornudar </li>
                                <li>Inhalar gotitas infectadas</li>
                              </ul>
                            </li>
                            <li>
                                Las personas pueden transmitir el Hib sin que manifiesten síntomas.
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
