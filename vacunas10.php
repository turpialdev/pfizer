<div class="container-fluid padding-vacunas-interno">
       
            
                <!-- <h1>ENFERMEDAD NEUMOCÓCICA</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>enfermedad neumocócica</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li class="lista-sin-estilo">  La enfermedad neumocócica es una infección bacteriana que puede causar enfermedades graves. 

                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h4>¿De qué manera la enfermedad neumocócica puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul class="col-md-6">
                            <li>
                            La enfermedad neumocócica puede ocasionar las siguientes enfermedades:
                            <ul>
                                <li>Meningitis (una infección del cerebro y la medula  espinal)</li>
                                <li>
                                Bacteriemia (una infección en la sangre) 
                                </li>
                                <li>
                                   Infecciones del oído u otitis media
                                </li>
                                <li>
                                    Neumonía: Infección de los pulmones
                                </li>
                            </ul>
                            </li>
                        </ul>
                        <ul class="col-md-6">
                            <li>
                                 La enfermedad neumocócica puede tener consecuencias graves: 
                                 <ul>
                                     <li>
                                         La meningitis puede ocasionar pérdida de la audición, lesión cerebral o la muerte.
                                     </li>
                                     <li>
                                         La bacteriemia también puede ocasionar complicaciones para la salud que representan un riesgo para la vida.
                                     </li>
                                     <li>
                                         La enfermedad neumocócica se puede complicar con derrame pleural y empiema.
                                     </li>
                                     <li>
                                         La otitis media se puede complicar con Otomastoiditis. 
                                     </li>
                                 </ul>
                            </li>
                           
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                            <li>Algunas veces, la bacteria que causa enfermedad neumocócica puede ser resistente a los antibióticos, lo que dificulta el
                             tratamiento.</li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La enfermedad neumocócica se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                            <li>
                                 Las personas pueden portar y transmitir la bacteria sin estar enfermas.
                            </li>
                           
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
