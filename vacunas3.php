<div  class="container-fluid padding-vacunas-interno">
       
            
                <!-- <h1>HEPATITIS A</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>hepatitis A</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> La hepatitis A es una infección del hígado causada por un virus.

                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h4>¿De qué manera la hepatitis A puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                              Los síntomas de la hepatitis A pueden incluir:
                              <ul>
                                <li>Fiebre</li>
                                <li>Náuseas y dolor de estómago.</li>
                                <li>Pérdida del apetito y cansancio.</li>
                                <li>Color amarillento de la piel y la parte blanca de los ojos.</li>
                              </ul>
                            </li>
                            <li>
                               En casos aislados, la hepatitis A puede causar la muerte.
                            </li>
                            <li>
                                Los síntomas pueden durar 2 meses.
                            </li>
                            <li>
                                Los niños menores de 6 años, por lo general, no presentan síntomas.
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La hepatitis A se encuentra en la materia fecal de las personas infectadas y se transmite fácilmente a las manos y los 
                                objetos.</li>
                            <li>Comer o beber algo que se haya contaminado con el virus puede provocar la infección.</li>
                            <li>Las personas pueden transmitir la hepatitis A sin que manifiesten síntomas.</li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
