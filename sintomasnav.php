<div class="container-fluid white ">
        <div class="row">
           <div role="tabpanel">
          <!-- Nav tabs -->
              <ul class="nav nav-tabs uppercase tabs-custom" role="tablist">
                <li  role="presentation"><a class="cardio" href="cardio.php" aria-controls="cardio" >Cardiología</a></li>
                <li role="presentation"><a class="dolor" href="dolor.php" aria-controls="dolor" >Dolor</a></li>
                <li role="presentation"><a class="piel" href="piel.php" aria-controls="piel" >Piel y Articulaciones</a></li>
                <li role="presentation"><a class="nervioso" href="nervioso.php" aria-controls="nervioso" >Sistema Nervioso</a></li>
                <li role="presentation"><a class="vacunas" href="vacunas.php" aria-controls="vacunas" >Vacunas</a></li>
              </ul>

             

            </div>
        </div> 
    </div>

    <script type="text/javascript">
      $('ul.nav-pills li a').click(function (e) {
      $('ul.nav-pills li.active').removeClass('active')
      $(this).parent('li').addClass('active')
    })
      $(document).ready(function(){
       if (window.location.href.indexOf("dolor") > -1) {
        $('.dolor').css('background-color', '#42BCE2');
        $('.dolor').css('color', '#fff');
        // $('.navbar-brand img').css('height', '55%');

       }
       if (window.location.href.indexOf("vacunas") > -1) {
        $('.vacunas').css('background-color', '#42BCE2');
        $('.vacunas').css('color', '#fff');
        // $('.navbar-brand img').css('height', '55%');
       }
       if (window.location.href.indexOf("piel") > -1) {
        $('.piel').css('background-color', '#42BCE2');
        $('.piel').css('color', '#fff');
        // $('.navbar-brand img').css('height', '55%');
       }
      if (window.location.href.indexOf("cardio") > -1) {
        $('.cardio').css('background-color', '#42BCE2');
        $('.cardio').css('color', '#fff');
        // $('.navbar-brand img').css('height', '55%');
       }
     if (window.location.href.indexOf("nervioso") > -1) {
        $('.nervioso').css('background-color', '#42BCE2');
        $('.nervioso').css('color', '#fff');
        // $('.navbar-brand img').css('height', '55%');
       }
      });
    </script>

