<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/cardio-banner.jpg" >
    <?php include 'mainnav.php' ?>
        <div class="container title">
         <h1 class="heading-interno">CARDIOLOGÍA</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
    
        <div class="container">
            <div class="row cardio-inter ">
                <div class="col-md-6 col-xs-12 hover_img">

                        <!-- <div id="grid" class="grid clearfix"> 
                            <a href="#" data-path-hover="M 0,0 0,38 90,58 180.5,38 180,0 z">
                                <figure>
                                    <img src="img/1.png" />
                                    <svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="M 0 0 L 0 182 L 90 126.5 L 180 182 L 180 0 L 0 0 z "/></svg>
                                    <figcaption>
                                        <h2>Coresterol</h2>
                                        <button>Ver</button>
                                    </figcaption>
                                </figure>
                            </a>
                            
                            <a href="#" data-path-hover="M 0,0 0,38 90,58 180.5,38 180,0 z">
                                <figure>
                                    <img src="img/3.png" />
                                    <svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="M 0 0 L 0 182 L 90 126.5 L 180 182 L 180 0 L 0 0 z "/></svg>
                                    <figcaption>
                                        <h2>Triglicerios</h2>
                                        <button>View</button>
                                    </figcaption>
                                </figure>
                            </a>


                            <!-<a href="cardio1.php" data-path-hover="M 0,0 0,38 90,58 180.5,38 180,0 z">
                                <figure>
                                    <img src="img/photo_ejemplo2.jpeg" />
                                    <svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="M 0 0 L 0 182 L 90 126.5 L 180 182 L 180 0 L 0 0 z "/></svg>
                                    <figcaption>
                                        <h2>Coresterol</h2>
                                        <button>Ver</button>
                                    </figcaption>
                                </figure>
                            </a>
                            <a href="cardio2.php" data-path-hover="M 0,0 0,38 90,58 180.5,38 180,0 z">
                                <figure>
                                    <img src="img/photo_ejemplo3.jpeg" />
                                    <svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="M 0 0 L 0 182 L 90 126.5 L 180 182 L 180 0 L 0 0 z "/></svg>
                                    <figcaption>
                                        <h2>Triglicerios</h2>
                                        <button>Ver</button>
                                    </figcaption>
                                </figure>
                            </a> 
                        </div> -->
                        <a class="sin_decoracion img-10" href="cardio1.php">
                            <img class="" src="img/cardio-intermedia-colesterol.jpg">
                            <div class="vista-inter-cardio">
                                Colesterol
                            </div>
                        </a>
                        </div>
                            <div class="col-md-6 col-xs-12 hover_img">
                                <a class="sin_decoracion img-10" href="cardio2.php">
                                    <img src="img/cardio-intermedia-trigliceridos.jpg">
                                    <div class="vista-inter-cardio">
                                        Triglicéridos
                                    </div> 
                                </a>
                            </div> 
                
            </div>
        </div>
            
                        

                

   
    <?php include 'footer.php' ?>
    <script src="js/snap.svg-min.js"></script>
   
    <script type="text/javascript">
        function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 100,
                header = document.querySelector("nav");
            if (distanceY > shrinkOn) {
                classie.add(header,"smaller");
            } else {
                if (classie.has(header,"smaller")) {
                    classie.remove(header,"smaller");
                }
            }
        });
    }
    window.onload = init();
    $('.dropdown-toggle').dropdown();
    jQuery('ul.nav li.dropdown').hover(function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
    }, 
    function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
    });

    </script>

<script type="text/javascript">
    $(document).ready(function(){
        $('a[href^="#"]').on('click', function(event) {

            var target = $( $(this).attr('href') );

            if( target.length ) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
            }

        });
    });


    (function() {

    function init2() {
        var speed = 300,
        easing = mina.backout;

        [].slice.call ( document.querySelectorAll( '#grid > a' ) ).forEach( function( el ) {
            var s = Snap( el.querySelector( 'svg' ) ), path = s.select( 'path' ),
                pathConfig = {
                    from : path.attr( 'd' ),
                    to : el.getAttribute( 'data-path-hover' )
                };

            el.addEventListener( 'mouseenter', function() {
                path.animate( { 'path' : pathConfig.to }, speed, easing );
            } );

            el.addEventListener( 'mouseleave', function() {
                path.animate( { 'path' : pathConfig.from }, speed, easing );
            } );
        } );
        
    }

    init2();

})();
</script>
</body>
</html>