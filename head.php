<head lang="en">
        <meta charset="UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <title>Pfizer Conmigo</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- <link href="css/bootstrap-theme.css" rel="stylesheet"> -->
        <link href="css/fonts.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/component.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <!-- hover -->
        <link rel="stylesheet" type="text/css" href="css/css-hover/normalize.css" />
        <link rel="stylesheet" type="text/css" href="css/css-hover/component.css" />
        
       
        <!-- <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300' rel='stylesheet' type='text/css'> -->
        <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
        <!-- // <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script> -->
        <!-- // <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
        <script type="text/javascript" src="js/classie.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script src="js/parallax.js"></script>
       
        <!-- hover -->

       
    </head>
