 <div class=" row-interno " role="tabpanel tab-interno">
          <!-- Nav tabs -->
              <ul class="nav nav-tabs tabs-custom-interno" role="tablist">
                <li role="presentation" class="active"><a href="#dolor1" aria-controls="dolor1" role="tab" data-toggle="tab">Dolor</a></li>
                <li role="presentation"><a href="#dolor2" aria-controls="dolor2" role="tab" data-toggle="tab">Dolor Nociceptivo</a></li>
                <li role="presentation"><a href="#dolor3" aria-controls="dolor3" role="tab" data-toggle="tab">Dolor Neuropático</a></li>
                <li role="presentation"><a href="#dolor4" aria-controls="dolor4" role="tab" data-toggle="tab">Fibriomialgia</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content tab-content-interno">
                <div role="tabpanel" class="tab-pane active" id="dolor1"><?php include 'dolor1.php' ?></div>
                <div role="tabpanel" class="tab-pane" id="dolor2"><?php include 'dolor2.php' ?></div>
                <div role="tabpanel" class="tab-pane" id="dolor3"><?php include 'dolor3.php' ?></div>
                <div role="tabpanel" class="tab-pane" id="dolor4"><?php include 'dolor4.php' ?></div>
              
              </div>

            </div>