<!DOCTYPE html>
<html>

     <?php include 'head.php' ?>
    <body>
        <div class="parallax-window" data-parallax="scroll" data-image-src="img/home-banner.jpg" data-position-y="-5px">
    <?php include 'mainnav.php' ?>
     
        
    </div>

    <div class="container-fluid white upper_arrow pad-30">
        <div class="container">
            <h1>¿TE HAS PREGUNTADO SOBRE TU SALUD?</h1>
            <h4 class="texto-principio">Pfizer te acompaña a tener una mejor calidad de vida.</h4>
            <div class="row">
                <div class="col-md-4">
                    <div class="col-md-12 ">
                        <div class="img-6"></div><!-- <img src="img/home-circulo-2.jpg" class="img-5"> --></div>
                    <div class="col-md-12 text-center padding-extra"> Encontrarás contenidos certificados con el respaldo médico de Pfizer.</div> 
                </div>
                <div class="col-md-4">
                    <div class="col-md-12 "><!-- <img src="img/Picture1.jpg" class="img-6"> --></div>
                    <div class="img-5"></div>
                    <div class="col-md-12 text-center padding-extra">Conoce sobre síntomas y causas de las patologías.</div> 
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        <div class="img-4"></div>
                        <!-- <img src="img/cuadrada1.jpg" class="img-4"> --></div>
                   <div class="col-md-12 text-center padding-extra"> Te otorgamos tips de alimentación y rutinas de ejercicios diarios.</div> 
                </div>
            </div>
        </div>
    </div> 
    <div class="container-fluid blue upper_arrow pad-30">
        <div  id="sintomas" ></div>
        <div class="row">
            <h1>¿CÓMO ME SIENTO?</h1>
            <h3 class="sub-title-header">Mis síntomas </h3>
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
                <a href="cardio.php" class="white-link">
                <img src="img/cardio.png" class="img-3">
                <div class="col-md-12 text-center white-link">
                Cardiovascular
                </div>
                </a>
            </div>
            <div class="col-md-2">
                <a href="vacunas.php" class="white-link">
                <img src="img/vacunas.png" class="img-3">
                 <div class="col-md-12 text-center white-link">
                    Vacunas
                </div>
                </a>
            </div>
            <div class="col-md-2">
                <a href="nervioso.php" class="white-link">
                <img src="img/brain.png" class="img-3">
                 <div class="col-md-12 text-center white-link">
                    Sistema Nervioso Central
                </div>
                </a>
            </div>
            <div class="col-md-2">
                <a href="piel.php" class="white-link">
                <img src="img/knee.png" class="img-3">
                <div class="col-md-12 text-center white-link">
                    Piel y Articulaciones
                </div>
                </a>
            </div>
            <div class="col-md-2">
                <a href="dolor.php" class="white-link">
                <img src="img/first-aid.png" class="img-3">
                <div class="col-md-12 text-center white-link">
                    Dolor
                </div>
            </a>
            </div>

            <div class="col-md-1">
            </div>
        </div>
    
    </div>
    <div class="parallax-window blue2 image_arrow module" data-parallax="scroll" data-image-src="img/home2.jpg">
        <div class="container title">
         <h1>“Más allá de un tratamiento”</h1>
        </div>
      </div>
    <div  class="container-fluid white upper_arrow pad-30">
        <div id="vida"></div>
        <div class="container">
            
                <h1>VIDA SALUDALE</h1>
            <div class="row padding-extra">
                <div class="col-xs-12 visible-xs">
                    <img src="img/healthy.jpg" class="img-1">
                </div>
                <div class="col-md-5 col-xs-12">
                    <h3>Nutrición</h3>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p class="margenes" style="text-align: justify;" >Saber lo que come es importante. Las personas diagnosticadas con colesterol alto deben limitar su consumo de ciertos tipos de alimentos que contienen colesterol. La American Heart Association (AHA) recomienda que las personas no coman más de 300 mg de colesterol por día -200 mg si tienen alguna enfermedad del corazón. (Para poner estas cifras en perspectiva, una sola yema de huevo contiene 186 mg.).  
                    </p>
                   <a href="nutricion.php" target="_blank" class="button ">Ver más</a>
                </div>
                <div class="col-md-7 col-xs-12 image_section hidden-xs">
                    <img src="img/healthy.jpg" class="img-1">
                </div>
            </div>
            <div class="row padding-extra">
                <div class="col-md-7 col-xs-12">
                    <img src="img/doctor1.jpg" class="img-1">
                </div>
                <div class="col-md-5 col-xs-12">
                    <h3 class="text-right"> Tu médico te habla </h3>
                    <!-- <h5 class="text-right sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p class="text-right"  style="text-align: justify;">¿Cómo saber si mis síntomas pertenecen a alguna patología? Si busco en Internet la información, consigo que poseo todas las enfermedades. Pfizer te ofrece la opinión de los expertos y aprender a diferenciar mis síntomas. En esta sección, conozca toda la información que los médicos recomiendan para conocer los primeros síntomas.</p>
                    <a href="#" target="_blank" class="button button-right">Ver más</a>
                </div>
            </div>
            <div class="row padding-extra">
                 <div class="col-md-12 visible-xs">
                    <img src="img/gentecorriendo.jpg" class="img-1">
                </div>
                <div class="col-md-5 col-xs-12">

                    <h3>Ejercicios</h3>
                     <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p class="margenes" style="text-align: justify;">La adopción de hábitos saludables en su vida diaria incluye hacer ejercicio regularmente. Sin embargo, eso no significa que se inscriba en el boot camp o empezar a correr maratones. Incluso pequeñas cantidades de ejercicio pueden jugar un papel fundamental en la disminución de los niveles de colesterol. De hecho, puede ser más fácil de lo que piensa hacer del ejercicio una parte de su vida diaria.</p>
                    <a href="ejercicios.php" target="_blank" class="button">Ver más</a>
                </div>
                <div class="col-md-7 image_section hidden-xs">
                    <img src="img/gentecorriendo.jpg" class="img-1">
                </div>


            </div>

        </div>
    </div>
    <div class="parallax-window white2 image_arrow module" data-parallax="scroll" data-image-src="img/home3.jpg">
        <div class="container title">
         <h1>“Trabajando juntos por un mundo más saludable”</h1>
        </div>
    </div>
     <?php include 'footer.php' ?>

    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});

</script>

<script>
$(document).ready(function () {
    $(window).scroll(function () {
        if ($(document).scrollTop() > 100) {
            $(".navbar").addClass("scrolled");
        } else {
            $(".navbar").removeClass("scrolled");
        }
    });
});

</script>

</body>
</html>