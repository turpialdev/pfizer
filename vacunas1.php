<div  class="container-fluid padding-vacunas-interno">
    
                <!-- <h1>DIFTERIA</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>difteria</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> La difteria es una enfermedad bacteriana que por lo general provoca fiebre leve y dolor de garganta. </li>
                            <li>La enfermedad es causada por una toxina que libera la bacteria y que forma una membrana gruesa. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h4>¿De qué manera la difteria puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                              A veces la difteria puede tener complicaciones graves que incluyen:
                              <ul>
                                <li>Daño cardíaco </li>
                                <li>Daño en los nervios </li>
                                <li>Debilidad muscular</li>
                                <li>Parálisis</li>
                              </ul>
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                              La difteria se transmite a través del aire al:
                              <ul>
                                <li>Toser </li>
                                <li>Estornudar </li>
                                <li>Inhalar gotitas infectadas</li>
                              </ul>
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
