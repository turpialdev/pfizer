<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    <?php include 'mainnav.php' ?>
    <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/banner_pa.jpg" >
     
        <div class="container title">
         <h1 class="heading-interno">NUTRICIÓN</h1>
        </div>
    </div>
   
<div  class="container-fluid white pad-30">
        <div id="vida"></div>
          <div class="container">
            <ol class="breadcrumb">
              <li><a href="#">Vida Saludable</a></li>
              <li class="active">Nutrición</li>
            </ol>
            <div class="row info hidden-xs pad-30">

                <div class="col-md-12" style="margin-left:12%;">
                 <div class="row">
                      <div class= "col-md-4 botonera_nutricion boton_nutri1">
                        <figure>
                          <img src="img/steak.png" width="75px" height="75px" />
                        </figure>
                      </div>
                        <div class= "col-md-4 botonera_nutricion boton_nutri2">
                        <figure>
                          <img src="img/sprig1.png" width="75px" height="75px" />
                        </figure>
                      </div>
                      <div class= "col-md-4 botonera_nutricion boton_nutri3">
                        <figure>
                          <img src="img/bread14.png" width="75px" height="75px" />
                        </figure>
                      </div>
                      <div class= "col-md-6 botonera_nutricion boton_nutri4">
                        <figure>
                          <img src="img/fruit68.png" width="75px" height="75px" />
                        </figure>
                      </div>
                      <div class= "col-md-6 botonera_nutricion boton_nutri5">
                        <figure>
                          <img src="img/fastfood6.png" width="75px" height="75px" />
                        </figure>
                      </div>
                    </div>  
                  </div>
                  </br></br>
              <div class="col-md-7 nutri_info info_nutri1">
                <h2>Proteínas</h2>
                <p>Las proteínas tienen alta concentración en las carnes. Hay proteínas en los vegetales en baja concentración, tales como las 
                  presentes en frutos secos y cereales integrales. Fuentes animales: vacunos, porcinos, aves de corral y pescados, los productos 
                  lácteos y los huevos también son fuentes de proteínas. Muchos alimentos animales con contenido proteico son ricos en grasas 
                  saturadas y colesterol, los cuales pueden tener un efecto negativo sobre la salud cardiovascular.</p>             
              </div>
              <div class="col-md-6 nutri_info info_nutri2">
                <h2>Fibra</h2>
                    <p>
                       La fibra tiene un efecto beneficioso. Se encuentra en cereales integrales, frutas, verduras, legumbres, frutos secos y 
                       semillas. Una dieta con alto contenido de fibra se asocia a una reducción en el riesgo de contraer cáncer, enfermedades 
                       cardiacas, diabetes de tipo 2 y obesidad.
                    </p>
              </div>
              <div class="col-md-7 nutri_info info_nutri3">
                <h2>Carbohidratos</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                  <p>Carbohidratos simples: son  azúcares que proveen energía (calorías) pero poseen poco valor nutritivo. El azúcar blanco y 
                    moreno, la miel, el azúcar que se halla naturalmente en las frutas (fructosa), el jarabe de alto contenido de fructosa 
                  (ampliamente usado como edulcorante en jugos de frutas) son algunos ejemplos. Los azúcares simples deberían limitarse porque 
                  aumentan el riesgo de desarrollo de diabetes tipo 2, obesidad y caries dental.
                  </p>  
                  <p>Carbohidratos complejos: son fuente primaria de energía del cuerpo. Están combinados en los vegetales con una variedad de 
                    vitaminas, minerales, fibras y fitoquímicos (nutrientes de plantas). Se hallan en frutas, verduras, panes de cereales 
                    integrales, cereales, frutos secos y semillas.
                  </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
              </div>
                <div class="col-md-7 nutri_info info_nutri4">
                    <h2>Grasas buenas o saludables</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>Las grasa monoinsaturadas: disminuyen las LDL, aumentan las HDL, y reducen los riesgos de enfermedad cardiaca. Fuentes: maní, nueces, aceitunas, aguacates, aceite de oliva y canola.
 
                    <p>Grasas poliinsaturadas: disminuyen las LDL, pueden disminuir las HDL, y reducen el riesgo de enfermedad cardiaca. Fuentes: 
                      aceite de maíz, girasol y soya. 
                    </p>
                    <p>Grasas poliinsaturadas tipo omega 3: bajan la LDL y los triglicéridos, disminuyen el riesgo de coagulación de la sangre y el 
                      riesgo de arritmias, bajan la presión sanguínea. Fuentes: pescados (salmón, sardina etc), aceite de linaza y nueces.
                    </p>    
                  
                </div>
                <div class="col-md-7 nutri_info info_nutri5">
                    <h2>Grasas malas</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>Grasas saturadas: suben la LDL y aumentan el riesgo de enfermedad coronaria. Fuentes: carnes rojas, embutidos, carnes 
                      procesadas, quesos, leche entera, crema de leche, helados.
                    </p>
                    <p>
                      Grasas trans: suben las LDL, bajan las HDL, aumentan el riesgo de enfermedad coronaria. Fuentes: pollo frito, buñuelos, 
                      donuts y otras comidas fritas, margarina parcialmente hidrogenada, grasas para repostería
                    </p>
                  
                </div>
                
            </div>

             <div class="visible-xs">
            <div class="boton-wide boton_nutri1">
              <figure>
                <img src="img/steak.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12 info_nutri1">
                <h2>Proteínas</h2>
                  <p>Las proteínas tienen alta concentración en las carnes. Hay proteínas en los vegetales en baja concentración, tales como las 
                    presentes en frutos secos y cereales integrales. Fuentes animales: vacunos, porcinos, aves de corral y pescados, los productos 
                    lácteos y los huevos también son fuentes de proteínas. Muchos alimentos animales con contenido proteico son ricos en grasas 
                    saturadas y colesterol, los cuales pueden tener un efecto negativo sobre la salud cardiovascular.</p>     
              </div>
            </div>
            <div class="boton-wide boton_nutri2">
              <figure>
                <img src="img/sprig1.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12 info_nutri2">
                <h2>Fibra</h2>
                    <p>
                       La fibra tiene un efecto beneficioso. Se encuentra en cereales integrales, frutas, verduras, legumbres, frutos secos y 
                       semillas. Una dieta con alto contenido de fibra se asocia a una reducción en el riesgo de contraer cáncer, enfermedades 
                       cardiacas, diabetes de tipo 2 y obesidad.
                    </p>
                    
              </div>
            </div>
            <div class="boton-wide boton_nutri3">
              <figure>
                <img src="img/bread14.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12 info_nutri3">
                 <h2>Carbohidratos</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                  <p>Carbohidratos simples: son  azúcares que proveen energía (calorías) pero poseen poco valor nutritivo. El azúcar blanco y 
                    moreno, la miel, el azúcar que se halla naturalmente en las frutas (fructosa), el jarabe de alto contenido de fructosa 
                  (ampliamente usado como edulcorante en jugos de frutas) son algunos ejemplos. Los azúcares simples deberían limitarse porque 
                  aumentan el riesgo de desarrollo de diabetes tipo 2, obesidad y caries dental.
                  </p>  
                  <p>Carbohidratos complejos: son fuente primaria de energía del cuerpo. Están combinados en los vegetales con una variedad de 
                    vitaminas, minerales, fibras y fitoquímicos (nutrientes de plantas). Se hallan en frutas, verduras, panes de cereales 
                    integrales, cereales, frutos secos y semillas.
                  </p>
              </div>
            </div>
            <div class="boton-wide boton_nutri4">
              <figure>
                <img src="img/fruit68.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12 info_nutri4">
                <h2>Grasas buenas o saludables</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>Las grasa monoinsaturadas: disminuyen las LDL, aumentan las HDL, y reducen los riesgos de enfermedad cardiaca. Fuentes: maní, nueces, aceitunas, aguacates, aceite de oliva y canola.
 
                    <p>Grasas poliinsaturadas: disminuyen las LDL, pueden disminuir las HDL, y reducen el riesgo de enfermedad cardiaca. Fuentes: 
                      aceite de maíz, girasol y soya. 
                    </p>
                    <p>Grasas poliinsaturadas tipo omega 3: bajan la LDL y los triglicéridos, disminuyen el riesgo de coagulación de la sangre y el 
                      riesgo de arritmias, bajan la presión sanguínea. Fuentes: pescados (salmón, sardina etc), aceite de linaza y nueces.
                    </p>    
              </div>
            </div>
            <div class="boton-wide boton_nutri5">
              <figure>
                <img src="img/fastfood6.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12 info_nutri5">
                <h2>Grasas malas</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>Grasas saturadas: suben la LDL y aumentan el riesgo de enfermedad coronaria. Fuentes: carnes rojas, embutidos, carnes 
                      procesadas, quesos, leche entera, crema de leche, helados.
                    </p>
                    <p>
                      Grasas trans: suben las LDL, bajan las HDL, aumentan el riesgo de enfermedad coronaria. Fuentes: pollo frito, buñuelos, 
                      donuts y otras comidas fritas, margarina parcialmente hidrogenada, grasas para repostería
                    </p>
                       
              </div>
            </div>
        </div>
       
            <div class="row ">
                <div class="col-md-12  referencias-nutri">
                    <h2>Referencias</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                      <ul>
                      <li class="lista-sin-estilo">1- www.healt.Havard.Edu. </li>
                      <li class="lista-sin-estilo">2- Organización Mundial de la Salud. Oficina regional para Europa y oficina regional para las 
                        Américas. Programa CINDI – OMS. EUR/00/5018028. 2000. pp. 6-19. </li>
                      </ul>
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
        
        </div>
    </div>
        <?php include 'footer.php' ?>
   
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>
 <script>
     $( ".boton_nutri1" ).on( "click", function() {
         $('.info_nutri1').css('display', 'block');
         $('.info_nutri2').css('display', 'none');
         $('.info_nutri3').css('display', 'none');
         $('.info_nutri4').css('display', 'none');
         $('.info_nutri5').css('display', 'none');
         
         
      });
     $( ".boton_nutri2" ).on( "click", function() {
         $('.info_nutri1').css('display', 'none');
         $('.info_nutri2').css('display', 'block');
         $('.info_nutri3').css('display', 'none');
         $('.info_nutri4').css('display', 'none');
         $('.info_nutri5').css('display', 'none');
         
      });
     $( ".boton_nutri3" ).on( "click", function() {
         $('.info_nutri1').css('display', 'none');
         $('.info_nutri2').css('display', 'none');
         $('.info_nutri3').css('display', 'block');
         $('.info_nutri4').css('display', 'none');
         $('.info_nutri5').css('display', 'none');
         
      });
    $( ".boton_nutri4" ).on( "click", function() {
         $('.info_nutri1').css('display', 'none');
         $('.info_nutri2').css('display', 'none');
         $('.info_nutri3').css('display', 'none');
         $('.info_nutri4').css('display', 'block');
         $('.info_nutri5').css('display', 'none');
         
      });
   $( ".boton_nutri5" ).on( "click", function() {
         $('.info_nutri1').css('display', 'none');
         $('.info_nutri2').css('display', 'none');
         $('.info_nutri3').css('display', 'none');
         $('.info_nutri4').css('display', 'none');
         $('.info_nutri5').css('display', 'block');
         
      });

    </script>

</body>
</html>