 <!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    
     <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/dolor-banner.jpg">
     <?php include 'mainnav.php' ?>
        <div class="container title">
         <h1 class="heading-interno">DOLOR</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
<div  class="container-fluid white pad-30">
        <div id="vida"></div>
        <div class="container">
            <ol class="breadcrumb">
            <li><a href="dolor.php">Dolor</a></li>
            <li class="active">Dolor Neuropático</li>
          </ol>
                <!-- <h1>DOLOR NEUROPÁTICO</h1> -->
            <div class="row info" >
                <div class="col-md-6 justificar menos_espacio ">
                   <h2>¿Qué es el dolor neuropático?</h2> 
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                   <p> Los médicos utilizan el  término de dolor neuropático para referirse al dolor que es causado por una lesión primaria (golpe, daño o
                    herida) o una alteración en el sistema nervioso. Este tipo de dolor se produce cuando ocurre una lesión en los nervios periféricos, 
                    en el cerebro o en la médula espinal que ocasiona que percibamos dolor en alguna parte del organismo; por ejemplo, cuando una persona 
                    tiene un accidente vascular cerebral y a causa de esto siente dolor intenso en un brazo o pierna, que puede llegar a ser persistente 
                    por varios años.
                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6">
                    <img class="img-2" src="img/12.jpg" />
                </div>
                
            </div>

            <div class="row info">
                    <div class="col-md-12"><h2>¿Cuáles son los distintos tipos de dolor neuropático?</h2></div>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <div class="col-md-6">
                    <p>
                        Existen  varias  clases  de  dolor  neuropático,  de  acuerdo  a  la  Red Internacional del Dolor Neuropático, e incluyen:
                        <ul>
                            <li>
                                Neuropatías periféricas dolorosas, como algunos casos de neuropatías diabéticas - una complicación de la diabetes 
                                que afecta el tejido nervioso.   

                            </li>
                            <li>
                                Neuralgia postherpética - dolor persistente (continuo) o que va y viene, en la zona de la erupción del herpes 
                                zoster (culebrilla).
                            </li>
                            <li>
                                Neuralgia del trigémino – también llamada tic doloroso, es una afección dolorosa crónica que afecta al nervio 
                                trigémino, uno de los nervios más largos de la cabeza. El trastorno causa ardor extremo, esporádico y súbito o 
                                dolor facial tipo shock que dura desde unos segundos hasta dos minutos por episodio. La intensidad del dolor 
                                puede ser física y mentalmente incapacitante.
                              
                            </li>
                            <li>
                                Neuropatías por compresión como el síndrome del túnel carpiano - dolor en la mano y la muñeca.
                            </li>
                </div>
                <div class="col-md-6">
                            <li>
                                Dolor del miembro fantasma - una sensación de dolor que se origina en una zona de un miembro amputado.
                            </li>
                            <li>
                                Esclerosis múltiple - dolor en varios lugares de los miembros inferiores  y superiores.
                            </li>
                            <li>
                                Dolor posterior a un accidente cerebrovascular.
                            </li>
                            <li>
                                Neuropatías periféricas asociadas al virus de inmunodeficiencia humano (VIH) - una amplia gama de trastornos 
                                dolorosos observados en personas infectadas por VIH. Hasta el 50 por ciento de las personas infectadas por VIH 
                                padecen neuropatía periférica durante el transcurso de sus vidas.
                            </li>
                            <li>
                               Dolor neuropático relacionado con el cáncer (inducido por el tumor o la quimioterapia utilizada como tratamiento).
                            </li>
                            <li>
                                Dolor neuropático inducido por fármacos.
                            </li>
                            <li>
                               Algunos casos de dolor de espalda y cuello.
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row pad-30 piel-icono info hidden-xs">
            <div class="col-md-12">
            <div class="col-md-5">
                 <div class="row">
                    <div class= "col-md-8 botonera boton_dolor31 pf-on">
                      <figure>
                        <img src="img/pin56.png" width="100px" height="100px" />
                      </figure>
                    </div>
                      <div class= "col-md-8 botonera boton_dolor32 apagado pf-off">
                      <figure>
                        <img src="img/medical50.png" width="100px" height="100px" />
                      </figure>
                    </div>
                  </div>
                  <div class="row">
                    <div class= "col-md-8 botonera boton_dolor33 apagado pf-off">
                      <figure>
                        <img src="img/medical51.png" width="100px" height="100px" />
                      </figure>
                    </div>
                    <div class= "col-md-8 botonera boton_dolor34 apagado pf-off">
                      <figure>
                        <img src="img/pill11.png" width="100px" height="100px" />
                      </figure>
                    </div>
                  </div>
            </div>
              <div class="col-md-7 info_piel1">
                 <h2>Puntos dolorosos</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    
                        <ul>
                            <li> 
                                Infecciones
                            </li>
                            <li>Inflamación
                            </li>
                            <li>
                                Irritación de nervios espinales (ej. como consecuencia de una hernia de disco).
                            </li>
                            <li>
                                Compresión nerviosa por tumores (enfermedades neoplásicas).
                            </li>
                            <li>
                                Enfermedades  metabólicas  como  la  diabetes,  trastornos  de  la  glándula tiroides 
                                o algunas otras como la artritis o el lupus eritematoso.
                            </li>
                            <li>
                                Lesión cerebral (ej. infartos o hemorragias cerebrales).
                            </li>
                            <li>
                                Traumatismos (golpes) en extremidades (principalmente en los hombros, caderas o tobillos).
                            </li>
                            <li>
                                Uso excesivo de alguna articulación como las muñecas al trabajar en la computadora.
                            </li>
                            <li>
                                Drogas, toxinas
                            </li>
                            <li>
                               Sin causa identificable (como la neuralgia intercostal que se presenta debido a la lesión 
                               o inflamación de los nervios intercostales que son los que corren entre las costillas en 
                               el tórax, es una causa habitual de dolor torácico).
                            </li>
                        </ul>

                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-7 info_piel2">
                    <h2>Características del dolor neuropático</h2>
                    <ul>
                        <li>Dolor quemante (arde en llamas), punzante (como agujas)</li>
                        <li>Dolor paroxístico (como descargas súbitas e intensas)</li>
                        <li>Dolor pobremente localizado, difuso</li>
                        <li>Alodinia (percepción de dolor ante estímulo no doloroso)</li>
                        <li>Hiperalgesia (respuesta exagerada a estímulo doloroso)</li>
                        <li>Hiperestesia (mayor sensibilidad a estímulos no dolorosos)</li>
                        <li>Parestesia (sensación anormal, como calambres)</li>
                        <li>Disestesias (percepción desagradable ante estímulos)</li>
                        <li>La intensidad del dolor se altera con estados emocionales</li>
                    </ul>
                    <p>
                        La persistencia del dolor y sensaciones desagradables pueden alterar la calidad del sueño de los 
                        pacientes, no cumpliendo las funciones reparadoras normales del organismo. A su vez, los pacientes 
                        con mala calidad de sueño presentan mayor sensibilidad al dolor, disminución de la concentración y 
                        síntomas de ansiedad y depresión. Estas perturbaciones se traducen en una disminución de la calidad de vida.
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                 <div class="col-md-7 info_piel3">
                     <h2>¿Cómo se llega al diagnóstico?</h2>
                    <p>Existen muchas causas de esta lesión nerviosa o cerebral que origina dolor, y muchos de los estudios que solicitan 
                        los médicos van encaminados a descubrir lo que está causando la alteración de los nervios o del cerebro o de la 
                        médula espinal.</p>
                    <p>
                        Con el objeto de encontrar las causas del dolor, el médico hará un interrogatorio, además de que le realizará:
                    </p>
                    <ul>
                        <li>Análisis de sangre</li>
                        <li>Radiografías</li>
                        <li>Tomografías</li>

                    </ul>
                    <br>
                    <p>
                        Y frecuentemente, solicitará estudios de conducción nerviosa en los que se aplican pequeños electrodos que revisan 
                        el funcionamiento de los nervios en el organismo. Como ejemplo podemos citar a la diabetes, en la cual la glucosa 
                        se encuentra tan elevada en la sangre, que se produce una lesión en los nervios y la persona entonces tiene dolor 
                        neuropático. De manera que algunas personas con dolor neuropático deben someterse a estudios de glucosa en la sangre 
                        para identificar si son diabéticos y no lo sabían.
                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-7 info_piel4">
                   <h2>¿Cómo se trata el dolor neuropático?</h2>
                    <p>Debido a que los síntomas del dolor neuropático pueden ser invalidantes, progresivos y de larga duración, el abordaje 
                        terapéutico es multimodal; es decir, debe incluir en forma personalizada tanto los aspectos físicos como los psicosociales. 
                        El tratamiento de la enfermedad de base y del dolor deben ser considerados en forma simultánea.
                    </p>
                    <p>
                      Los objetivos terapéuticos son: mitigar el dolor, mejorar el movimiento y la fuerza muscular, controlar el estrés y los 
                      estados de ánimo. La psicoeducación y grupos de autoayuda son esenciales a la hora de esclarecer la naturaleza y manejo de la 
                      enfermedad.
                    </p>
                    <p>
                       El dolor neuropático no tiene un tratamiento farmacológico único y absoluto, y su eficacia dependerá de su detección temprana, 
                       el conocimiento de los mecanismos responsables y del uso de estrategias terapéuticas alternativas.
                    </p>
                    <p>
                        Los tratamientos farmacológicos actuales para el dolor neuropático incluyen la administración de diferentes tipos de medicamentos 
                        de efecto local o general, tratamientos físicos como estimulación eléctrica o insensibilización, e intervenciones quirúrgicas 
                        (descompresivas).
                    </p>
                    <p>Lejos de ser un padecimiento incurable, en la actualidad la neuropatía dolorosa debe considerarse como un desafío terapéutico 
                        alcanzable por un equipo experto y dedicado a esta compleja condición</p>
                    
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>

            </div>
            <div class="visible-xs">
            <div class="boton-wide boton_dolor31">
              <figure>
                <img src="img/pin56.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                <h2>Puntos dolorosos</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    
                        <ul>
                            <li> 
                                Infecciones
                            </li>
                            <li>Inflamación
                            </li>
                            <li>
                                Irritación de nervios espinales (ej. como consecuencia de una hernia de disco).
                            </li>
                            <li>
                                Compresión nerviosa por tumores (enfermedades neoplásicas).
                            </li>
                            <li>
                                Enfermedades  metabólicas  como  la  diabetes,  trastornos  de  la  glándula tiroides 
                                o algunas otras como la artritis o el lupus eritematoso.
                            </li>
                            <li>
                                Lesión cerebral (ej. infartos o hemorragias cerebrales).
                            </li>
                            <li>
                                Traumatismos (golpes) en extremidades (principalmente en los hombros, caderas o tobillos).
                            </li>
                            <li>
                                Uso excesivo de alguna articulación como las muñecas al trabajar en la computadora.
                            </li>
                            <li>
                                Drogas, toxinas
                            </li>
                            <li>
                               Sin causa identificable (como la neuralgia intercostal que se presenta debido a la lesión 
                               o inflamación de los nervios intercostales que son los que corren entre las costillas en 
                               el tórax, es una causa habitual de dolor torácico).
                            </li>
                        </ul>

              </div>
            </div>
            <div class="boton-wide boton_dolor32">
              <figure>
                <img src="img/medical50.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
               <h2>Características del dolor neuropático</h2>
                    <ul>
                        <li>Dolor quemante (arde en llamas), punzante (como agujas)</li>
                        <li>Dolor paroxístico (como descargas súbitas e intensas)</li>
                        <li>Dolor pobremente localizado, difuso</li>
                        <li>Alodinia (percepción de dolor ante estímulo no doloroso)</li>
                        <li>Hiperalgesia (respuesta exagerada a estímulo doloroso)</li>
                        <li>Hiperestesia (mayor sensibilidad a estímulos no dolorosos)</li>
                        <li>Parestesia (sensación anormal, como calambres)</li>
                        <li>Disestesias (percepción desagradable ante estímulos)</li>
                        <li>La intensidad del dolor se altera con estados emocionales</li>
                    </ul>
                    <p>
                        La persistencia del dolor y sensaciones desagradables pueden alterar la calidad del sueño de los 
                        pacientes, no cumpliendo las funciones reparadoras normales del organismo. A su vez, los pacientes 
                        con mala calidad de sueño presentan mayor sensibilidad al dolor, disminución de la concentración y 
                        síntomas de ansiedad y depresión. Estas perturbaciones se traducen en una disminución de la calidad de vida.
                    </p>
              </div>
            </div>
            <div class="boton-wide boton_dolor33">
              <figure>
                <img src="img/medical51.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                  <h2>¿Cómo se llega al diagnóstico?</h2>
                    <p>Existen muchas causas de esta lesión nerviosa o cerebral que origina dolor, y muchos de los estudios que solicitan 
                        los médicos van encaminados a descubrir lo que está causando la alteración de los nervios o del cerebro o de la 
                        médula espinal.</p>
                    <p>
                        Con el objeto de encontrar las causas del dolor, el médico hará un interrogatorio, además de que le realizará:
                    </p>
                    <ul>
                        <li>Análisis de sangre</li>
                        <li>Radiografías</li>
                        <li>Tomografías</li>

                    </ul>
                    <p>
                        Y frecuentemente, solicitará estudios de conducción nerviosa en los que se aplican pequeños electrodos que revisan 
                        el funcionamiento de los nervios en el organismo. Como ejemplo podemos citar a la diabetes, en la cual la glucosa 
                        se encuentra tan elevada en la sangre, que se produce una lesión en los nervios y la persona entonces tiene dolor 
                        neuropático. De manera que algunas personas con dolor neuropático deben someterse a estudios de glucosa en la sangre 
                        para identificar si son diabéticos y no lo sabían.
                    </p>
              </div>
            </div>
            <div class="boton-wide boton_dolor34">
              <figure>
                <img src="img/pill11.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                  <h2>¿Cómo se trata el dolor neuropático?</h2>
                    <p>Debido a que los síntomas del dolor neuropático pueden ser invalidantes, progresivos y de larga duración, el abordaje 
                        terapéutico es multimodal; es decir, debe incluir en forma personalizada tanto los aspectos físicos como los psicosociales. 
                        El tratamiento de la enfermedad de base y del dolor deben ser considerados en forma simultánea.
                    </p>
                    <p>
                      Los objetivos terapéuticos son: mitigar el dolor, mejorar el movimiento y la fuerza muscular, controlar el estrés y los 
                      estados de ánimo. La psicoeducación y grupos de autoayuda son esenciales a la hora de esclarecer la naturaleza y manejo de la 
                      enfermedad.
                    </p>
                    <p>
                       El dolor neuropático no tiene un tratamiento farmacológico único y absoluto, y su eficacia dependerá de su detección temprana, 
                       el conocimiento de los mecanismos responsables y del uso de estrategias terapéuticas alternativas.
                    </p>
                    <p>
                        Los tratamientos farmacológicos actuales para el dolor neuropático incluyen la administración de diferentes tipos de medicamentos 
                        de efecto local o general, tratamientos físicos como estimulación eléctrica o insensibilización, e intervenciones quirúrgicas 
                        (descompresivas).
                    </p>
                    <p>Lejos de ser un padecimiento incurable, en la actualidad la neuropatía dolorosa debe considerarse como un desafío terapéutico 
                        alcanzable por un equipo experto y dedicado a esta compleja condición</p>
                    
              </div>
            </div>
            </div>
        </div>
            <div class="row info">
                <div class="col-md-12">
                      <h2>¿Cuáles son los síntomas?</h2>
                  </div>
            <div class="col-md-6">
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    
                    <p text-align: justify;>
                        De acuerdo a la Red Internacional del Dolor Neuropático, los síntomas se describen habitualmente 
                        como dolor punzante, ardoroso o quemante o similar a una descarga eléctrica. Normalmente los 
                        pacientes se quejan de dolores espontáneos (sin que haya existido un estímulo que los provoque), 
                        y de dolores provocados (sensaciones dolorosas ante estímulos que no deberían producir dolor, por 
                        ejemplo al tocar al paciente para revisarlo). Estos dolores espontáneos pueden ser continuos o 
                        intermitentes).
                    </p>
                </div>
                <div class="col-md-6">
                
                    <p>
                        Otros síntomas frecuentes del dolor neuropático incluyen:
                    </p>
                    
                        <ul>
                            <li>Dolor generado por algo que normalmente no provoca dolor, como el tocar con un algodón o el 
                                simple roce de los dedos como una caricia.</li>
                            <li>Una respuesta dolorosa excesiva hacia algo que normalmente es doloroso.</li>
                            <li>Una respuesta excesiva al tacto, como el simple roce de las sábanas.</li>
                            <li>Un dolor persistente incluso después de haber eliminado la causa del dolor.</li>
                            <li>Parestesias y disestesias - sensaciones anormales y desagradables descritas como 
                                hormigueos y pinchazos con aguja.</li>
                        </ul>
                    </p>
                </div>
            </div>
            
             
        </div>
    </div>
    <?php include 'footer.php' ?>
   
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>
<script>
     $( ".boton_dolor31" ).on( "click", function() {
         $('.info_piel1').css('display', 'block');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'none');
       
         
         
      });
     $( ".boton_dolor32" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'block');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'none');
        
         
      });
     $( ".boton_dolor33" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'block');
         $('.info_piel4').css('display', 'none');
         
         
      });
    $( ".boton_dolor34" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'block');
         
         
      });
   

    </script>
    </script>
<script>
  $(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            interval: false
        });
    });
});​
</script>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>

<script>
$(document).ready(function(){

    $('.carousel-indicators li').on('click', function(){

        $('.carousel-indicators li').each(function(){

            $(this).removeClass("apagado");

        });

        $(this).addClass("apagado");
    });

    $( "body" ).on('click', '.pf-off', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on").addClass( "apagado" );

        $(this).removeClass( "pf-off" );
        $(".pf-on").addClass( "pf-off" );
        $(".pf-on").removeClass( "pf-on" );
        $(this).addClass( "pf-on" );

    });
});



</script>
</body>
</html>