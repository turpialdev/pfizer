<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    
    <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/piel-banner.jpg" >
     <?php include 'mainnav.php' ?>
        <div class="container title">
         <h1 class="heading-interno">PIEL Y ARTICULACIONES</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
       <div class="container">
         <div class="row dolor-inter-sup ">
        <div class="col-xs-6 col-xs-12 hover_img">
        <a class="sin_decoracion img-10" href="piel1.php">
       
            <img src="img/piel-intermedia-psoriasis.jpg">
            <div class="vista-inter-cardio">
                Psoriasis
            </div>
        

        </a>
    </div>
    <div class="col-xs-6 col-xs-12 hover_img"> 
        <a class="sin_decoracion img-10" href="piel2.php">
       
            <img src="img/piel-intermedia-reumatoide.jpg">
            <div class="vista-inter-cardio">
               Artritis Reumatoide
            </div>
        

        </a>
    </div>
        
    </div>
    <div class="row dolor-inter-inf ">
        <div class="col-xs-6 col-xs-12 hover_img">
        <a class="sin_decoracion img-10" href="piel3.php">
       
            <img src="img/piel-intermedia-espondilitis.jpg">
            <div class="vista-inter-cardio">
                Espondilitis Anquilosante
            </div>
        

        </a>
    </div>
    <div class="col-xs-6 col-xs-12 hover_img">
        <a class="sin_decoracion img-10" href="piel4.php">
       
            <img src="img/piel-intermedia-psoriasica.jpg">
            <div class="vista-inter-cardio">
               Artritis Psoriásica
            </div>
        

        </a>
    </div>
        
    </div>
    </div>
   
    
    <?php include 'footer.php' ?>
   
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>
</body>
</html>