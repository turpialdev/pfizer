<html>
<head>
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="owl-carousel/owl.theme.css">
    <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="js/classie.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/parallax.js"></script>
        <script src="js/jquery-asAccordion.js"></script>
        <script src="owl-carousel/owl.carousel.js"></script>
</head>
<body>
<div id="owl-demo" class="owl-carousel owl-theme">
    
    <div><img src="http://www.owlgraphic.com/owlcarousel/demos/assets/fullimage1.jpg" alt="The Last of us" title="1st img Title"/></div>
    
    <div><img src="http://www.owlgraphic.com/owlcarousel/demos/assets/fullimage2.jpg" alt="The Last of us" title="2nd img Title"/></div>
    
    <div><img src="http://www.owlgraphic.com/owlcarousel/demos/assets/fullimage3.jpg" alt="The Last of us" title="3rd img Title"/></div>
    
    <div><img src="http://www.owlgraphic.com/owlcarousel/demos/assets/fullimage4.jpg" alt="The Last of us" title="4th img Title"/></div>    
    
</div><!-- / #owl-demo -->
<script>
 var owl;

$(document).ready(function () {

function customPager() {

    $.each(this.owl.userItems, function (i) {

        var titleData = jQuery(this).find('img').attr('title');
        var paginationLinks = jQuery('.owl-controls .owl-pagination .owl-page span');

    $(paginationLinks[i]).append(titleData);

    });

}

    $('.owl-carousel').owlCarousel({
        navigation: false, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        afterInit: customPager,
        afterUpdate: customPager
    });

});
</script>

</body>

</html>