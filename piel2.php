<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    
    <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/piel-banner.jpg" >
     <?php include 'mainnav.php' ?>
        <div class="container title">
         <h1 class="heading-interno">PIEL Y ARTICULACIONES</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
<div  class="container-fluid white pad-30">
        <div id="vida"></div>
        <div class="container">
            <ol class="breadcrumb">
            <li><a href="piel.php">Piel y Articulaciones</a></li>
            <li class="active">Artritis Reumatoide</li>
          </ol>
                <!-- <h1>DIFTERIA</h1> -->
            <div class="row info">
                <div class="col-md-6 que-es-vacunas justificar menos_espacio">
                    <h2>¿Qué es la Artritis Reumatoide?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        Esta es una enfermedad crónica que se caracteriza por dolor y rigidez de las articulaciones en ambos lados del cuerpo, que 
                        pueden llegar a deformarse. Es una enfermedad autoinmune, es decir, que el sistema inmunológico del cuerpo ataca a sus 
                        propias células y tejidos sanos.
                    </p>
                    <p>
                       Se puede controlar con tratamientos para detener la progresión de la enfermedad. Es importante el diagnóstico temprano para 
                       un tratamiento precoz y oportuno que le permita continuar con sus actividades cotidianas.
                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6">
                    <img class="img-2" src="img/piel-reumatoide-interior.jpg" />
                </div>
            </div>
           <div class="row piel-icono info hidden-xs">
              <div class="col-md-12">
                <div class="col-md-5">
                   <div class="row">
                      <div class= "col-md-7 botonera boton_piel11 pf-on1">
                        <figure>
                          <img src="img/medical50.png" width="100px" height="100px" />
                        </figure>
                      </div>
                      <div class= "col-md-7 botonera boton_piel12 apagado pf-off1">
                        <figure>
                          <img src="img/medical14.png" width="100px" height="100px" />
                        </figure>
                      </div>
                    </div>
                    <div class="row">
                      <div class= "col-md-7 botonera boton_piel13 apagado pf-off1">
                        <figure>
                          <img src="img/medical51.png" width="100px" height="100px" />
                        </figure>
                      </div>
                      <div class= "col-md-7 botonera boton_piel14 apagado pf-off1">
                        <figure>
                          <img src="img/question1.png" width="100px" height="100px" />
                        </figure>
                      </div>
                    </div>
                </div>
                  <div class="col-md-6 piel_info info_piel1">
                    <h2>Causas de la Artritis Reumatoide</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        Los factores genéticos son una causa importante de la AR. Puede ser hereditaria. La AR a menudo se presenta en varios 
                        miembros de la familia, pero se desconoce la causa genética exacta. A diferencia de la Osteoartritis, que es una enfermedad 
                        del cartílago, la artritis asociada con alteración del sistema inmunitario, como la AR se relacionan con una inflamación 
                        del recubrimiento articular. Otros factores asociados incluyen el clima, los agentes infecciosos (virus, bacterias u hongos) 
                        o un desequilibrio de ciertas enzimas. El estrés puede empeorar los síntomas.
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6 piel_info info_piel2">
                    <h2>Signos y Síntomas</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>Dolor e inflamación de las articulaciones.</li>
                            <li>Rigidez de las articulaciones.</li>
                            <li>Articulaciones hinchadas o deformes.</li>
                            <li>Articulaciones congeladas.</li>
                            <li>Fiebre baja.</li>
                            <li>Ganglios linfáticos inflamados.</li>
                            <li>Síndrome de Sjögren (resequedad de los ojos y la boca).</li>
                            <li>Inflamación del ojo.</li>
                        </ul>
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                 <div class="col-md-6 piel_info info_piel3">
                    <h2>Diagnóstico</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Historia clínica: antecedentes personales y familiares. El examen físico puede mostrar una acumulación 
                       de líquido sinovial alrededor de la articulación (derrame).
                    </p>
                    <h2>Pruebas diagnósticas:</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       <ul>
                           <li>
                               Una prueba de factor reumatoide y una de anticuerpos antinucleares, las cuales pueden ayudar a 
                               los médicos a verificar que es efectivamente la enfermedad.
                           </li>
                           <li>
                               Pruebas de sangre como velocidad de sedimentación globular (VSG) y proteína C reactiva (PCR), 
                               las cuales miden el grado de inflamación en su cuerpo.
                           </li>
                           <li>Radiografías de las articulaciones afectadas para ayudar a su médico a determinar el grado del 
                            daño, si existe, y medir el progreso de su enfermedad.</li>
                            <li>Exploración de las articulaciones dolorosas, dedos en forma de salchicha, etc.</li>
                            <li>EI índice de Actividad de la Enfermedad (DAS 28) mide la actividad de la enfermedad en 28 
                                articulaciones. EI DAS 28 facilita a los médicos el diagnóstico y evalúa que tan activa está.</li>
                            <li>El HAQ se enfoca en la incapacidad y la limitación funcional.</li>
                       </ul>
                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6 piel_info info_piel4">
                    <h2>¿Quién puede padecerla?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       <ul>
                           <li>La artritis reumatoide afecta más a las mujeres que a los hombres (el 75% de las personas que la tienen son mujeres).
                           </li>
                           <li>La enfermedad suele aparecer entre los 20 y los 45 años de edad.</li>
                           <li>Afecta aproximadamente al 1% de la población.</li>
                       </ul>
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
              </div>
            </div>
            <div class="visible-xs">
            <div class="boton-wide boton_piel11">
              <figure>
                <img src="img/medical50.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                <h2>Causas de la Artritis Reumatoide</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        Los factores genéticos son una causa importante de la AR. Puede ser hereditaria. La AR a menudo se presenta en varios 
                        miembros de la familia, pero se desconoce la causa genética exacta. A diferencia de la Osteoartritis, que es una enfermedad 
                        del cartílago, la artritis asociada con alteración del sistema inmunitario, como la AR se relacionan con una inflamación 
                        del recubrimiento articular. Otros factores asociados incluyen el clima, los agentes infecciosos (virus, bacterias u hongos) 
                        o un desequilibrio de ciertas enzimas. El estrés puede empeorar los síntomas.
                    </p>
              </div>
            </div>
            <div class="boton-wide boton_piel12">
              <figure>
                <img src="img/medical14.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
               <h2>Signos y Síntomas</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>Dolor e inflamación de las articulaciones.</li>
                            <li>Rigidez de las articulaciones.</li>
                            <li>Articulaciones hinchadas o deformes.</li>
                            <li>Articulaciones congeladas.</li>
                            <li>Fiebre baja.</li>
                            <li>Ganglios linfáticos inflamados.</li>
                            <li>Síndrome de Sjögren (resequedad de los ojos y la boca).</li>
                            <li>Inflamación del ojo.</li>
                        </ul>
                    </p>
              </div>
            </div>
            <div class="boton-wide boton_piel13">
              <figure>
                <img src="img/medical51.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                 <<h2>Diagnóstico</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Historia clínica: antecedentes personales y familiares. El examen físico puede mostrar una acumulación 
                       de líquido sinovial alrededor de la articulación (derrame).
                    </p>
                    <h2>Pruebas diagnósticas:</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       <ul>
                           <li>
                               Una prueba de factor reumatoide y una de anticuerpos antinucleares, las cuales pueden ayudar a 
                               los médicos a verificar que es efectivamente la enfermedad.
                           </li>
                           <li>
                               Pruebas de sangre como velocidad de sedimentación globular (VSG) y proteína C reactiva (PCR), 
                               las cuales miden el grado de inflamación en su cuerpo.
                           </li>
                           <li>Radiografías de las articulaciones afectadas para ayudar a su médico a determinar el grado del 
                            daño, si existe, y medir el progreso de su enfermedad.</li>
                            <li>Exploración de las articulaciones dolorosas, dedos en forma de salchicha, etc.</li>
                            <li>EI índice de Actividad de la Enfermedad (DAS 28) mide la actividad de la enfermedad en 28 
                                articulaciones. EI DAS 28 facilita a los médicos el diagnóstico y evalúa que tan activa está.</li>
                            <li>El HAQ se enfoca en la incapacidad y la limitación funcional.</li>
                       </ul>
                    </p>
              </div>
            </div>
            <div class="boton-wide boton_piel14">
              <figure>
                <img src="img/question1.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                 <h2>¿Quién puede padecerla?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       <ul>
                           <li>La artritis reumatoide afecta más a las mujeres que a los hombres (el 75% de las personas que la tienen son mujeres).
                           </li>
                           <li>La enfermedad suele aparecer entre los 20 y los 45 años de edad.</li>
                           <li>Afecta aproximadamente al 1% de la población.</li>
                       </ul>
                    </p>
                    
              </div>
            </div>
            
        </div>
            
          
            <div class="row info">
                <div class="col-md-12 que-es-vacunas">
                    <h2>CONSEJOS UTILES</h2>
                    <div class="fila-slider">
              <div id="myCarouselPiel2" class="carousel slide pfizer-carousel" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators hidden-xs">
                <li data-target="#myCarouselPiel2" data-slide-to="0"></li>
                <li data-target="#myCarouselPiel2" data-slide-to="1"></li>
                <li data-target="#myCarouselPiel2" data-slide-to="2"></li>
                <li data-target="#myCarouselPiel2" data-slide-to="3"></li>
                <li data-target="#myCarouselPiel2" data-slide-to="4"></li>
                <li data-target="#myCarouselPiel2" data-slide-to="5"></li>
                <li data-target="#myCarouselPiel2" data-slide-to="6"></li>
                <li data-target="#myCarouselPiel2" data-slide-to="7"></li>
               
            </ol>
            <div class="col-sm-7 pfizer-slider">
                <div class="carousel-inner" role="listbox">
                    <div class="item active" style="background:#67c9e7;">
                        <!-- <img class="first-slide" src="images/WB26_003.jpg" alt="First slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_1" >
                                  <p>
                                  El apoyo familiar es fundamental, más aún si el paciente es un niño (menor de edad) o 
                                  un anciano; en estos casos es necesario que un familiar que preferiblemente viva con el paciente, se haga responsable de la aplicación 
                                  del medicamento, así como de acudir regularmente al médico y estar atento ante cualquiera de las situaciones 
                                  durante el tratamiento mencionadas con anterioridad.
                                  </p>
                                
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#54c2e4;">
                        <!-- <img class="second-slide" src="images/WB33_009.jpeg" alt="Second slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_4">
                                <p>
                                 Cuando le diagnosticaron su enfermedad, su familia y amigos pueden no saber la mejor forma de ayudarlo. Aquí están 
                                algunos 
                                consejos para ayudarle a explicar lo que usted está viviendo:
                                <br>
                                <ul>
                                  <li>Comente a sus familiares y amigos que puede variar el dolor o la rigidez que usted presenta.</li>
                                  <li>Si usted requiere cambiar o posponer actividades en conjunto pregunte si comprendieron.</li>
                                  <li>Cuando usted este con sus amigos o familiares, programe actividades en las que pueda participar.</li>
                                  <li>Sea directo, dígales a sus amigos y familiares lo que usted necesita.</li>
                                  <li>Hable abiertamente con su pareja. Expresar sus verdaderos sentimientos, lo ayudará a mantener una relación saludable y 
                                    también Ie permitirá a su pareja ayudarlo.</li>
                                </ul>
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#42bce2;">
                        <!-- <img class="third-slide" src="images/CW40_085.jpg" alt="Third slide"> -->
                        
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text">
                               Es importante ser un agente multiplicador de la información y que otras personas que puedan tener su misma condición se enteren de lo que 
                                 pueden hacer.
                                 <br>
                                 <ul>
                                   <li>Intente hablarle regularmente a un amigo o miembro de la familia, mantenerse en contacto Ie facilitará comentar como lo afecta su 
                                    condición.</li>
                                    <li>Aprenda sobre la enfermedad, entre más conozca usted sobre el tema, mejor lo podrá explicar.</li>
                                    <li>Enseñe a sus amigos y familiares sobre la enfermedad, proporcióneles este folleto para que lo lean y recomiende los sitios web que usted 
                                      ha encontrado que ofrecen información fácil de entender.</li>
                                 </ul>
                                
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#3ba9cb;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_1">
                                 <p>
                                 Diviértase diariamente: lea un libro, pasee a su perro, escuche música, escriba algo que le guste, monte bicicleta, cante mientras sebaña, vaya a 
                                al cine o al teatro. Recuerde, este es el mejor momento para disfrutar de su vida.
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#3496b4;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text ">
                                 <p>
                                   En general, no se ha asociado ningún tipo de dieta a la mejoría o empeoramiento de la enfermedad, Lo más importante es que lleve una
                                dieta saludable, consulte con su médico cuáles son los alimentos más sanos que le ayudarán a mantener un buen estado de salud. Es recomendable 
                                evitar los excesos en azucares y grasas con la finalidad de que no haya sobrepeso u obesidad que pudiera incrementar el esfuerzo de sus 
                                articulaciones.
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#2e839e;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_3">
                                 <p>
                                    Existen muchas cosas que usted puede hacer para ayudar a mantenerse en un estilo de vida saludable y sin estrés:
                                <ul>
                                  <li>
                                    Vigile su peso corporal: el sobrepeso puede poner un esfuerzo adicional en sus rodillas, así que pregunte a su 
                                    médico si es apropiado un programa de pérdida de peso.

                                  </li>
                                  <li>Organícese: organice su trabajo y hogar para que los artículos que usted emplea frecuentemente sean fácilmente 
                                    accesibles.</li>
                                  <li>
                                    Vístase cómodamente: existen cosas simples que usted puede hacer cuando se vista, para ayudar a minimizar el esfuerzo en sus 
                                    articulaciones:
                                    <ul>
                                      <li>Prepare su ropa la noche anterior; cuando sus articulaciones no están tan rígidas.</li>
                                      <li>No apriete demasiado el nudo de sus corbatas para que sus manos no tengan que hacer mucho esfuerzo.</li>
                                      <li>Vista ropa holgada con aberturas grandes para que pueda ponérsela y quitársela fácilmente.</li>
                                      <li>Elija zapatos cómodos y fáciles de poner, para reducir o eliminar la presión en sus pies.</li>
                                    </ul>
                                  </li>
                                  <li>
                                    Tome sus precauciones cuando viaje:
                                    <ul>
                                      <li>
                                        Haga reservaciones oportunas para asegurarse de que sus necesidades serán cubiertas.
                                    </li>
                                    </ul>
                                  </li>
                                </ul>
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                     <div class="item" style="background:#277087;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_3">
                                 <p>
                                 Es necesario realizar una jornada de ejercicios con esta condición. Dedique un tiempo para poder ejercer el 
                                objetivo de mantener su movimiento y función. Hay 24 horas en el día, por lo que el ejercicio durante 20 minutos 
                                todavía le deja con un montón de tiempo para otras cosas. Elija el momento de hacer ejercicio cuando usted esté por 
                                lo general, más rígido. Si se le pasa un día o dos sin ejercitarse, No sea duro consigo mismo, simplemente recuerde 
                                volver al programa.
                                <ul>
                                  <li>
                                    Haga que sea divertido: si no le gusta hacer ejercicio, sea creativo, en el ejercicio escuche su música favorita, 
                                    o haga ejercicio con un amigo.
                                  </li>
                                  <li>
                                    Comience poco a poco: si tiene miedo de los movimientos… le hará daño, empiece muy, muy lentamente, con un 
                                    estiramiento suave y la gama de ejercicios de movimiento sugeridos.
                                  </li>
                                  <li>Hacer tiempo: si usted no puede disponer de un espacio amplio de tiempo para hacer ejercicios, trate de hacerlo en 15 minutos 
                                    dos veces al día.</li>
                                    <li>Siéntase cómodo: use ropa cómoda y trate de relajarse contando en voz alta. Esto ayuda a su respiración. 
                                      ¿Por qué es importante esto? Ya que, los tejidos relajados trabajan con mayor facilidad.</li>
                                </ul>
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#215e71;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_3">
                                 <p>
                                  Es importante reconocer que es mucho lo que se puede hacer para incorporar actividades en el día a día que mejoren la 
                                  calidad de vida: 
                                  <ul>
                                    <li>Asistir a Grupos Terapéuticos de Apoyo conformados por personas con la misma condición para compartir experiencias es una de ellas, estas 
                                      sesiones pueden producir un verdadero empoderamiento de la Calidad de Vida.</li>
                                    <li>
                                      La investigación demuestra que la meditación consciente, el Tai Chi, el Yoga o similares, pueden tener un efecto favorable sobre los síntomas 
                                      de su enfermedad.
                                    </li>

                                  </ul>
          
                                </p>
                                <br>
                                <p>
                                  Es bien sabido que cuando una persona toma el control de cualquier enfermedad crónica, y tiene un punto de vista saludable, es más probable que su 
                                  autoestima sea adecuada y su calidad de vida mejore, para incluir esperanza y optimismo en miras al futuro. <br>
                                </p>
                                <p>
                                  Antes de iniciar su rutina de ejercicios recuerde:
                                  <ul>
                                    <li>
                                      Consulte a su reumatólogo para que evalúe su estado físico.
                                    </li>
                                    <li>Aumente su nivel de ejercicios gradualmente.</li>
                                    <li>
                                      Aprenda a entender y respetar su dolor (Conozca sus límites).
                                    </li>
                                  </ul>
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hidden-xs col-sm-5 pfizer-slider-indicators" >
                <ol class="carousel-indicators">
                  
                      <li class="botones8a slide18 pf-on" data-target="#myCarouselPiel2" data-slide-to="0" ><p>Familia</p></li>
                      <li class="botones8a slide28 apagado pf-off" data-target="#myCarouselPiel2" data-slide-to="1" ><p>Comunicación</p></li>

                      <li class="botones8a slide38 apagado pf-off" data-target="#myCarouselPiel2" data-slide-to="2" ><p>Vocero</p></li>
                      <li class="botones8a slide48 apagado pf-off" data-target="#myCarouselPiel2" data-slide-to="3" ><p>Hobby</p></li>
                  
                      <li class="botones8a slide58 apagado pf-off" data-target="#myCarouselPiel2" data-slide-to="4" ><p>Alimentación</p></li>
                      <li class="botones8a slide68 apagado pf-off" data-target="#myCarouselPiel2" data-slide-to="5" ><p>Estrés</p></li>
                  
                      <li class="botones8au slide78 apagado pf-off" data-target="#myCarouselPiel2" data-slide-to="6" ><p>Ejercicio</p></li>
                      <li class="botones8au slide88 apagado pf-off" data-target="#myCarouselPiel2" data-slide-to="7" ><p>Terapia</p></li>

                </ol>
            </div>
            <a class="left carousel-control visible-xs" href="#myCarouselPiel2" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left visi" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control visible-xs" href="#myCarouselPiel2" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
        </div>
            </div>
            
                </div>
            </div>
            <!-- <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>Ejercicios</h2>
                  
                  <ul>
                    <li>
                      Ejercicios de FLEXIÓN:
                      <ol>
                        <li>Colóquese de pie, levante y extienda lo más alto posible su brazo izquierdo sin levantar los talones.</li>
                        <li>Mantenga esta posición por 5 segundos.</li>
                        <li>Luego bájelo y repita con el brazo derecho.</li>
                      </ol>
                    </li>
                    <li>
                      Ejercicios de FORTALECIMIENTO:
                      <ol>
                        <li>Tumbado sobre la espalda, las rodillas dobladas y los pies en el suelo.</li>
                        <li>Levante las caderas del suelo lo más alto.</li>
                      </ol>
                    </li>
                    <li>
                      Ejercicios para Cuellov
                      <ul>
                        <li>
                          En posición sentado manteniendo la espalda recta gire lentamente la cabeza hacia el lado izquierdo mirando por encima 
                          del hombro retorne su cabeza al centro y gírela suavemente hacia el lado derecho mirando por encima del hombro.
                        </li>
                      </ul>
                    </li>
                    <li>
                        Ejercicios para Codo y Hombro:
                        <ul>
                          <li>En posición sentado con los hombros y los brazos relajados a los lados comience a girar suavemente los hombros hacia 
                            adelante en movimientos circulares haciéndolo con los dos hombros al mismo tiempo o de manera alternada, repita el 
                            procedimiento girando los hombros hacia atrás.</li>
                          <li>En posición de pie o sentado mantenga los brazos extendidos a los lados del cuerpo luego levante uno de los brazos 
                            hasta lograr una posición totalmente horizontal con el codo en extensión, luego flexiónelo hasta tocar el hombro con 
                            las punta de los dedos y finalmente devuélvalo a su posición inicial y realice lo mismo con el otro brazo.</li>
                          <li>En posición de pie entrelace sus manos sobre la parte de atrás de la cabeza tratando de mantener los codos lo más 
                            separado posible uno del otro y luego llévelos hacia adelante, sin retirar las manos de la cabeza, intentando 
                            juntarlos.</li>
                            <li>Tome un palo largo (palo de escoba) y sujételo por detrás de la espalda por sus extremos con las dos manos, 
                              realice ejercicios de deslizamiento lateral alcanzando el máximo de amplitud posible en cada movimiento a 
                              los dos lados.</li>
                        </ul>
                    </li>
                    <li>
                      Ejercicios para Manos y Muñecas:
                      <ul>
                        <li>Apoye la mano, completamente extendida, sobre una superficie plana. Posteriormente recoja los dedos hasta formar 
                          un puño y luego regrese a la posición inicial de la mano.</li>
                        <li>Con la mano en posición horizontal toque con el dedo pulgar la punta de los demás dedos de la mano manteniéndolos 
                            siempre extendidos.</li>
                        <li>
                          Con la palma de la mano apoyada sobre una superficie plana y los dedos juntos comience a separar el dedo pulgar y 
                          luego desplace cada uno de los dedos restantes hacia este.
                        </li>
                        <li>
                          Apoye la muñeca y la palma de la mano sobre el borde de una superficie plana (mesa) con la otra mano tome las puntas 
                          de los dedos y levántela sin despegar la muñeca de la superficie y luego llévela hacia abajo.
                        </li>
                        <li>Con la mano abierta y los dedos completamente extendidos mueva la mano de lado a lado lo más que pueda. 
                          Realícelo alternadamente.</li>
                        <li>Tome una pelota antiestrés suave y cierre la mano ejerciendo presión sobre la pelota. Realícelo con ambas manos 
                          alternadamente.</li>
                      </ul>
                    </li>
                    <li>
                      Ejercicios para Caderas y Rodillas:
                      <ul>
                        <li>Acostado boca arriba sobre una superficie cómoda con las piernas totalmente extendidas separe una pierna lo máximo 
                          que pueda de la otra y vuelva a la posición inicial. Realícelo con ambas piernas de manera alternada.</li>
                        <li>Manténgase acostado boca arriba lleve una rodilla hacia el pecho, ayudándose con las manos y manteniendo la otra pierna 
                          totalmente extendida. Realice esta actividad de manera alternada con ambas piernas.</li>
                        <li>Acostado boca arriba sobre una superficie cómoda, con las piernas extendidas y los pies separados unos 40 cm gire las 
                          piernas y muslos hacia adentro hasta poner en contacto la punta de los dedos gordos de ambos pies y posteriormente rotar 
                          piernas y muslos hacia afuera hasta alejar lo máximo posible los dedos gordos de ambos pies. Sentado en una silla con 
                          los pies retirados del suelo extienda una rodilla lo máximo que pueda y posteriormente flexiónela lo más que pueda. 
                          Realícelo alternadamente con cada rodilla.</li>
                      </ul>
                    </li>
                    <li>
                      Ejercicios para Tobillos y Pies:
                      <ul>
                        <li>Acostado boca arriba sobre una superficie cómoda con las piernas estiradas completamente, mueva los pies hacia atrás 
                          y delante lo máximo que pueda, realice el ejercicio con los dos pies.</li>
                        <li>Sentado en una silla levante una pierna manteniendo el pie extendido y comience a realizar movimientos circulares. 
                            Realícelo alternadamente.</li>
                      </ul>
                    </li>
                  </ul>
                 
                </div>
            </div> -->
        </div>
    </div>
    <?php include 'footer.php' ?>
    <script>
     $( ".boton_piel11" ).on( "click", function() {
         $('.info_piel1').css('display', 'block');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'none');
       
         
         
      });
     $( ".boton_piel12" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'block');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'none');
        
         
      });
     $( ".boton_piel13" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'block');
         $('.info_piel4').css('display', 'none');
         
         
      });
    $( ".boton_piel14" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'block');
         
         
      });
   

    </script>
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>
<script>
  $(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            interval: false
        });
    });
});​
</script>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script>
$(document).ready(function(){
    $( "body" ).on('click', '.pf-off', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on").addClass( "apagado" );

        $(this).removeClass( "pf-off" );
        $(".pf-on").addClass( "pf-off" );
        $(".pf-on").removeClass( "pf-on" );
        $(this).addClass( "pf-on" );    });

    $( "body" ).on('click', '.pf-off1', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on1").addClass( "apagado" );

        $(this).removeClass( "pf-off1" );
        $(".pf-on1").addClass( "pf-off1" );
        $(".pf-on1").removeClass( "pf-on1" );
        $(this).addClass( "pf-on1" );    
      });
});
</script>
</body>
</html>