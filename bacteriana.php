<!DOCTYPE html>
<html>
    <?php include 'head.php' ?>
    <body class="vacunas-pag">
        <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/vacunas-banner.jpg" >
            <?php include 'mainnav.php' ?>
            <div class="container title">
                <h1 class="heading-interno">INMUNIZACIÓN</h1>
            </div>
        </div>
        <?php include 'sintomasnav.php' ?>
        <!-- Juanchi 
        <div >
        -->
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="vacunas.php">Vacunas</a></li>
                <li class="active">Enfermedades Bacterianas Inmunoprevenibles</li>
            </ol>
            <div id="menu_vacunas" name="menu_vacunas" class="row hidden-xs">
                <!-- <a class="link_scroll" href="#vacuna10"> -->
                <div class="col-xs-2 botonera_vacunas boton_vacuna1">
                    <img src="img/molecule12.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                       ENFERMEDAD NEUMOCÓCICA
                    </div>
                </div>
                <!-- </a> -->
                <!--  <a class="link_scroll" href="#vacuna14"> -->
                <div class="col-xs-2 botonera_vacunas boton_vacuna2">
                    <img src="img/user168.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        TÉTANOS
                    </div>
                </div>
                <!-- </a>
                <a class="link_scroll" href="#vacuna1"> -->
                <div class="col-xs-2 botonera_vacunas boton_vacuna3">
                    <img src="img/virus5.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        DIFTERIA
                    </div>
                </div>
                <!--  </a>
                <a class="link_scroll" href="#vacuna9"> -->
                <div class="col-xs-2 botonera_vacunas boton_vacuna4">
                    <img src="img/woman.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        TOS FERINA
                    </div>
                </div>
                <!--   </a>
                <a class="link_scroll" href="#vacuna7"> -->
                <div class="col-xs-2 botonera_vacunas boton_vacuna5">
                    <img src="img/vacunas.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        ENFERMEDAD MENINGOCÓCICA
                    </div>
                </div>
                <!--  </a>
                <a class="link_scroll" href="#vacuna2"> -->
                <div class="col-xs-2 botonera_vacunas boton_vacuna6">
                    <img src="img/boy3.png" width="75px" height="75px" />
                    <div class="texto-iconos">
                        HAEMOPHILUS INFLUENZAE TIPO B
                    </div>
                </div>
                <!--  </a> -->
            </div>                
            <div class="boton-wide boton_vacuna1 visible-xs">
                <figure>
                    <img src="img/molecule12.png" width="100%" height="100%" />
                <figure>
            </div>                
            <div id="vacuna10" name="vacuna10" class="row vacuna vacuna10">
                <div class="col-md-6">
                    <h2>enfermedad neumocócica</h2>   
                    <p>La enfermedad neumocócica es una infección bacteriana que puede causar enfermedades graves.</p>
                    <h4>¿De qué manera la enfermedad neumocócica puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul >
                            <li>La enfermedad neumocócica puede ocasionar las siguientes enfermedades:
                                <ul>
                                    <li>
                                        Meningitis (una infección del cerebro y la medula  espinal)
                                    </li>
                                    <li>
                                        Bacteriemia (una infección en la sangre)
                                    </li>
                                    <li>
                                        Infecciones del oído u otitis media
                                    </li>
                                    <li>
                                        Neumonía: Infección de los pulmones
                                    </li>
                                    <li>
                                        La meningitis puede ocasionar pérdida de la audición, lesión cerebral o la muerte.
                                    </li>
                                    <li>
                                        La bacteriemia también puede ocasionar complicaciones para la salud que representan un riesgo para la vida.
                                    </li>
                                    <li>
                                        La enfermedad neumocócica se puede complicar con derrame pleural y empiema.
                                    </li>
                                    <li>
                                        La otitis media se puede complicar con Otomastoiditis. 
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </p>
                </div> 
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                            <li>Algunas veces, la bacteria que causa enfermedad neumocócica puede ser resistente a los antibióticos, lo que dificulta el tratamiento.</li>
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La enfermedad neumocócica se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                            <li>
                                 Las personas pueden portar y transmitir la bacteria sin estar enfermas.
                            </li>                       
                        </ul>
                    </p>
                </div> 
                <!-- Juanchi
                <a class="link_scroll" href="#menu_vacunas">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                --> 
            </div>
            <div class="boton-wide boton_vacuna2 visible-xs">
                <figure>
                    <img src="img/user168.png" width="100%" height="100%" />
                </figure>
            </div>                  
            <div id="vacuna14" name="vacuna14" class="row vacuna vacuna14">
                <div class="col-md-6">
                    <h2>tétanos</h2>   
                    <p>
                        El tétanos es una infección bacteriana que también se denomina “trismo”. La bacteria que causa el tétanos libera una 
                        fuerte toxina que provoca síntomas neurológicos (del sistema nervioso).
                    </p>
                    <h4>¿De qué manera el tétanos puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                                El tétanos ataca los músculos, la médula espinal y el cerebro, y puede ser mortal.
                            </li>
                            <li>
                                Los síntomas incluyen rigidez muscular, espasmos, fiebre, dificultad para respirar, pulso acelerado y cambios en la presión arterial.
                            </li>
                            <li>
                                La recuperación del tétanos puede llevar meses y requerir estadías prolongadas en el hospital.
                            </li>       
                        </ul>
                    </p>
                </div> 
                
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>                        
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                                Una persona puede contraer tétanos a través de una herida en la piel, por lo general, una 
                                herida punzante causada por clavos de construcción, alambre de púas o mordidas de animales.
                            </li>
                            <li>
                                El tétanos no se puede transmitir directamente de persona a persona.
                            </li>
                        </ul>
                    </p>
                </div> 
                <!-- Juanchi     
                <a class="link_scroll" href="#menu_vacunas">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                --> 
            </div>   
            <div class="boton-wide boton_vacuna3 visible-xs">
                <figure>
                    <img src="img/virus5.png" width="100%" height="100%" />
                </figure>
            </div>               
            <div id="vacuna1" name="vacuna1" class="row vacuna vacuna1">
                <div class="col-md-6">
                    <h2>difteria</h2>   
                    <p>
                        La difteria es una enfermedad bacteriana que por lo general provoca fiebre leve y dolor de garganta.
                        La enfermedad es causada por una toxina que libera la bacteria y que forma una membrana gruesa.
                    </p>
                    <h4>¿De qué manera la difteria puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>A veces la difteria puede tener complicaciones graves que incluyen:
                                <ul>
                                    <li>Daño cardíaco </li>
                                    <li>Daño en los nervios </li>
                                    <li>Debilidad muscular</li>
                                    <li>Parálisis</li>
                                </ul>
                            </li>
                        </ul>
                    </p>
                </div> 
                    
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>      
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La difteria se transmite a través del aire al:
                                <ul>
                                    <li>Toser </li>
                                    <li>Estornudar </li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                        </ul>
                    </p>
                </div>
                <!-- Juanchi
                <a class="link_scroll" href="#menu_vacunas">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                --> 
            </div>                
            <div class="boton-wide boton_vacuna4 visible-xs">
                <figure>
                    <img src="img/bacteria3.png" width="100%" height="100%" />
                </figure>
            </div>                
            <div id="vacuna9" name="vacuna9" class="row vacuna vacuna9">
                <div class="col-md-6">
                    <h2>tos ferina</h2>   
                    <p>
                        La tos ferina es una enfermedad bacteriana sumamente contagiosa.
                    </p>
                    <h4>¿De qué manera la tos ferina puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                                La tos ferina provoca accesos de tos intensos y rápidos.
                            </li>
                            <li>
                                Puede durar 6 semanas o más.
                            </li>
                            <li>
                               Las complicaciones pueden incluir neumonía y convulsiones, especialmente en lactantes.
                            </li>
                            <li>
                                En algunos casos, la tos ferina puede ser mortal.
                            </li>
                        </ul>
                    </p>
                </div> 
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                                
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La tos ferina se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>   
                        </ul>
                    </p>
                </div>
                <!-- Juanchi 
                <a class="link_scroll" href="#menu_vacunas">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                --> 
            </div>            
            <div class="boton-wide boton_vacuna5 visible-xs">
                <figure>
                    <img src="img/molecule12.png" width="100%" height="100%" />
                </figure>
            </div>            
            <div id="vacuna7" name="vacuna7" class="row vacuna vacuna7">
                <div class="col-md-6">
                    <h2>enfermedad meningocócica</h2>   
                    <p>
                        La enfermedad meningocócica es una enfermedad bacteriana grave.
                    </p>
                    <h4>¿De qué manera la enfermedad meningocócica puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La enfermedad meningocócica puede ocasionar:
                                <ul>
                                    <li>Meningitis</li>
                                    <li>Meningococemia (una infección en la sangre)</li>
                                    <li>Neumonía</li>
                                </ul>
                            </li>
                            <li>
                                En algunos casos, la enfermedad meningocócica puede ocasionar una pérdida permanente de la audición, lesión cerebral 
                                o pérdida de un miembro.
                            </li>
                            <li>
                                La enfermedad meningocócica, en ocasiones, puede provocar la muerte.
                            </li>  
                        </ul>
                    </p>
                </div> 
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La enfermedad meningocócica se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                            <li>También se puede transmitir por contacto directo con la persona infectada. </li>
                        </ul>
                    </p>
                </div> 
                <!-- Juanchi
                <a class="link_scroll" href="#menu_vacunas">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                --> 
            </div>            
            <div class="boton-wide boton_vacuna6 visible-xs">
                <figure>
                    <img src="img/boy3.png" width="100%" height="100%" />
                </figure>
            </div>            
            <div id="vacuna2" name="vacuna2" class="row vacuna vacuna2">
                <div class="col-md-6">
                    <h2>Haemophilus influenzae tipo b (Hib)</h2>   
                    <p>
                        El Hib (Haemophilus influenzae tipo b) es una bacteria que generalmente provoca enfermedades graves en lactantes y 
                        niños menores.     
                    </p>
                    <h4>¿El Hib puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>Puede ocasionar enfermedades graves como:
                                <ul>
                                    <li>Meningitis </li>
                                    <li>Neumonía</li>
                                    <li>Inflamación intensa de la garganta (epiglotitis)</li>
                                    <li>Otitis</li>
                                    <li>Sinusitis</li>
                                </ul>
                            </li>
                            <li>La infección por Hib puede tener consecuencias graves que incluyen:
                                <ul>
                                    <li>Infecciones cardíacas, articulares, óseas o cutáneas.</li>
                                    <li>Pérdida de la audición, lesión cerebral o muerte.</li>
                                </ul>
                            </li>
                        </ul>
                    </p>
                </div> 
                <div class="col-md-6 media-vacuna">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>    
                        </ul>
                    </p>
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>Se transmite a través del aire al:
                                <ul>
                                    <li>Toser </li>
                                    <li>Estornudar </li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                            <li>
                                Las personas pueden transmitir el Hib sin que manifiesten síntomas.
                            </li>
                        </ul>
                    </p>
                </div> 
                <!-- 
                <a class="link_scroll" href="#menu_vacunas">
                    <div class="flecha-up hidden-xs">
                        <img src="img/up arrow5.png" width="50px" height="50px" />
                    </a>
                </div>
                --> 
            </div>
        </div>
        <?php include 'footer.php' ?>
        
        <script type="text/javascript">
            function init() {
                window.addEventListener('scroll', function(e){
                    var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                        shrinkOn = 100,
                        header = document.querySelector("nav");
                    if (distanceY > shrinkOn) {
                        classie.add(header,"smaller");
                    } else {
                        if (classie.has(header,"smaller")) {
                            classie.remove(header,"smaller");
                        }
                    }
                });
            }
            window.onload = init();
            $('.dropdown-toggle').dropdown();
            jQuery('ul.nav li.dropdown').hover(function() {
              jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
            }, function() {
              jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('a[href^="#"]').on('click', function(event) {

                    var target = $( $(this).attr('href') );

                    if( target.length ) {
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                    }

                });
            });
        </script>
        
        <script type="text/javascript">
            $(document).ready(function(){  
            $('.vacunas').css('background-color', 'rgb(66,188,226)'); 
            $('.vacunas').css('color', '#fff');
            // $('.navbar-brand img').css('height', '55%');
            });
        </script>
        
        <script>
            $( ".boton_vacuna1" ).on( "click", function() {
             $('.vacuna10').css('display', 'block');
             $('.vacuna14').css('display', 'none');
             $('.vacuna1').css('display', 'none');
             $('.vacuna9').css('display', 'none');
             $('.vacuna7').css('display', 'none');
             $('.vacuna2').css('display', 'none');
          });
            $( ".boton_vacuna2" ).on( "click", function() {
             $('.vacuna10').css('display', 'none');
             $('.vacuna14').css('display', 'block');
             $('.vacuna1').css('display', 'none');
             $('.vacuna9').css('display', 'none');
             $('.vacuna7').css('display', 'none');
             $('.vacuna2').css('display', 'none');
          });
            $( ".boton_vacuna3" ).on( "click", function() {
             $('.vacuna10').css('display', 'none');
             $('.vacuna14').css('display', 'none');
             $('.vacuna1').css('display', 'block');
             $('.vacuna9').css('display', 'none');
             $('.vacuna7').css('display', 'none');
             $('.vacuna2').css('display', 'none');
          });
            $( ".boton_vacuna4" ).on( "click", function() {
             $('.vacuna10').css('display', 'none');
             $('.vacuna14').css('display', 'none');
             $('.vacuna1').css('display', 'none');
             $('.vacuna9').css('display', 'block');
             $('.vacuna7').css('display', 'none');
             $('.vacuna2').css('display', 'none');
          });
            $( ".boton_vacuna5" ).on( "click", function() {
             $('.vacuna10').css('display', 'none');
             $('.vacuna14').css('display', 'none');
             $('.vacuna1').css('display', 'none');
             $('.vacuna9').css('display', 'none');
             $('.vacuna7').css('display', 'block');
             $('.vacuna2').css('display', 'none');
          });
            $( ".boton_vacuna6" ).on( "click", function() {
             $('.vacuna10').css('display', 'none');
             $('.vacuna14').css('display', 'none');
             $('.vacuna1').css('display', 'none');
             $('.vacuna9').css('display', 'none');
             $('.vacuna7').css('display', 'none');
             $('.vacuna2').css('display', 'block');
          });
        </script>
    </body>            
</html>