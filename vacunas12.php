<div  class="container-fluid padding-vacunas-interno">
        
            
                <!-- <h1>ROTAVIRUS</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>rotavirus</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>  El rotavirus es un virus que puede provocar diarrea intensa en lactantes y niños pequeños.

                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h4>¿De qué manera el rotavirus puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                             Los síntomas de una infección por rotavirus incluyen: 
                            <ul>
                                <li>Diarrea, vómitos, fiebre, dolor de estómago y deshidratación.</li>
                                <li>En algunos casos, el rotavirus puede provocar la muerte debido a una deshidratación intensa. </li>
                            </ul>
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>El rotavirus se transmite por contacto de persona a persona o por contacto con objetos infectados.
                            </li>
                            <li>El rotavirus puede vivir en superficies durante semanas, si las mismas no se desinfectan bien.</li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
 