<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    <?php include 'mainnav.php' ?>
    <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/banner_pa.jpg" >
     
        <div class="container title">
         <h1 class="heading-interno">EJERCICIOS</h1>
        </div>
    </div>
   
<div  class="container-fluid white pad-30">
        <div id="vida"></div>
        <div class="container">
            <ol class="breadcrumb">
            <li><a href="#">Vida Saludable</a></li>
            <li class="active">Ejercicios</li>
          </ol>
          
           <div class="col-md-8 ejer_info media-vacuna justificar menos_espacio">
            <h2 class="titulos_ejer ">Tipos de Ejercicios</h2>
            <p>En la actualidad para evitar el sedentarismo es recomendable llevar a cabo una rutina de ejercicios y la prioridad de variables que deben 
              ser trabajadas son:</p>
            </div>
            <div class="row info hidden-xs pad-30">
               <div class="col-md-6">
                 <div class="row">
                    <div class= "col-md-4 botonera_ejer boton_ejer1">
                      <figure>
                        <img src="img/jump12.png" width="100px" height="100px" />
                      </figure>
                    </div>
                      <div class= "col-md-4 botonera_ejer boton_ejer2">
                      <figure>
                        <img src="img/strong3.png" width="100px" height="100px" />
                      </figure>
                    </div>
                  

                  </div>
                  <div class="row">
                    <div class= "col-md-6 botonera_ejer boton_ejer3">
                      <figure>
                        <img src="img/yoga13.png" width="100px" height="100px" />
                      </figure>
                    </div>
                    <div class= "col-md-6 botonera_ejer boton_ejer4">
                      <figure>
                        <img src="img/exercise35.png" width="100px" height="100px" />
                      </figure>
                    </div>
                  </div>
            </div>

            <div class="col-md-6 nutri_info info_ejer1">
              <h2>Entrenamientos cardiovasculares</h2>
              <p>Como por ejemplo caminar, nadar, bailar, etc. Y todo esto unido a un cambio en su estilo de vida.</p>             
            </div>
            <div class="col-md-6 nutri_info info_ejer2">
                <h2>Entrenamientos de fuerza</h2>
                    <p>
                      Debemos realizarlos como complemento al cardiovascular, siempre que se hagan de forma dinámica, con cargas ligeras, 3 series 
                      cada ejercicio y de 8 a 15 repeticiones cada serie. Y todo esto unido a un cambio en su estilo de vida.

                    </p>
                </div>
            <div class="col-md-6 nutri_info info_ejer3">
                <h2>Estabilidad</h2>
                    
                  <p>Capacidad para mantener el equilibrio. Y todo esto unido a un cambio en su estilo de vida.
                  </p>  
                 
                </div>
                <div class="col-md-6 nutri_info info_ejer4">
                    <h2>Flexibilidad</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>Capacidad para mover una articulación en todo su rango de movimiento sin sentir dolor o incomodidad. Y todo esto unido a 
                      un cambio en su estilo de vida.
                  </p> 
                  
                </div>
               
            </div>

             <div class="visible-xs">
            <div class="boton-wide boton_nutri1">
              <figure>
                <img src="img/steak.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                <h2>Causas de la Espondilitis Anquilosante</h2>
              <p>Se desconocen las causas. Nadie sabe por qué el sistema inmune ataca las células de las articulaciones. Un gen particular puede 
                hacer que algunas personas tengan más posibilidades de padecer EA.</p>
              <p>
                Algunas cosas que sabemos bien son:
                <ul>
                  <li>La genética tiene un papel clave en la EA. Para muchas personas, un gen particular puede hacerlas más propensas a desarrollar 
                    esta enfermedad.</li>
                  <li>Las investigaciones han sugerido que, en las personas propensas a EA las bacterias u otros agentes infecciosos pueden ser 
                    participantes importantes en la actividad de la enfermedad.</li>
                </ul>
              </p>
              </div>
            </div>
            <div class="boton-wide boton_piel22">
              <figure>
                <img src="img/medical14.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
               <h2>Síntomas característicos</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Normalmente, las personas con EA el dolor y el primer aviso de rigidez son en la parte inferior de la espalda y las nalgas, 
                       causado por la artritis en las articulaciones sacroilíacas. Los síntomas generalmente son peor por las mañanas y durante la 
                       noche, y la rigidez puede extenderse hasta la columna vertebral y a veces hasta el cuello.
                    </p>
                    
              </div>
            </div>
            <div class="boton-wide boton_piel23">
              <figure>
                <img src="img/medical51.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                 <h2>¿Cómo se realiza el diagnóstico?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Para determinar si usted tiene EA, su médico Ie preguntará acerca de sus antecedentes médicos y familiares y sobre sus síntomas 
                       actuales. También si presenta dolor y rigidez que:
                       <ul>
                         <li>Haya durado varios meses.</li>
                         <li>Tome tiempo en aliviarse después de que se despierte en la mañana.</li>
                         <li>Disminuye cuando usted hace ejercicio o con otra actividad física.</li>
                         <li>Disminuye cuando usted toma medicamentos antiinflamatorios no esteroideos (AINEs).</li>
                       </ul>
                      </p>
              </div>
            </div>
            <div class="boton-wide boton_piel24">
              <figure>
                <img src="img/question1.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                 <h2>¿Quién puede padecerla?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Parece ser más frecuente en los hombres. Por lo general comienza en las personas entre los 20 y 30 años. Durante mucho tiempo, 
                       los investigadores sospecharon que tiene un componente hereditario ya que alrededor del 20% de las personas con EA también tienen 
                       un familiar con la enfermedad.
                    </p>
              </div>
            </div>
            
        </div>
        <h2 class="titulos_ejer">Ejercicios Básicos</h2>

           
            <div class="row info botonera_info_ejer2 hidden-xs pad-30">
               
            <div class="col-md-6 ejer_info2 info_ejer21 justificar menos_espacio">
              <h2>1</h2>
              <p>Siéntese en una silla con los pies separados, mantenga la columna y la cabeza derecha, los brazos relajados, luego levántese y 
                siéntese de forma continua y despacio. Realice esto varias veces (3 Series de 10 repeticiones por serie).</p>             
            </div>
             <div class="col-md-6 ejer_info2 info_ejer22 justificar menos_espacio">
              <h2>2</h2>
              <p>Colóquese al lado de una pared para apoyarse de manera lateral, mantenga una pierna levemente flexionada y la otra estirada a un ángulo de 45º, o hasta donde pueda, baje y suba la pierna de manera lateral y repetidamente, 
                luego realice el movimiento con la otra pierna. (3 series de 10 repeticiones por serie).</p>             
            </div>
            <div class="col-md-6 ejer_info2 info_ejer23 justificar menos_espacio">
              <h2>3</h2>
              <p>De la misma manera colóquese al lado de una pared o agárrese de una silla, suba una de las rodillas hasta donde pueda, preferiblemente hasta los 90º, baje y suba la rodilla de manera frontal y luego realice el movimiento 
                con la otra pierna. (3 Series de 12 repeticiones por pierna).</p>             
            </div>
            <div class="col-md-6 ejer_info2 info_ejer24 justificar menos_espacio">
              <h2>4</h2>
              <p>Acuéstese boca arriba con ambas rodillas flexionadas, contraiga los músculos del abdomen, procure contraer los músculos de la región glútea al mismo tiempo, las manos al lado del cuerpo. Si logra hacerlo correctamente la 
                región lumbar deberá tocar el suelo, una vez que consiga dominar el movimiento, mantenga contraída la musculatura por 5 segundos y relaje, esto deberá realizarse 3 veces.</p>             
            </div>
            <div class="col-md-6 ejer_info2 info_ejer25 justificar menos_espacio">
              <h2>5</h2>
              <p>Acuéstese boca arriba con las piernas flexionadas, levante la cabeza y la rodilla izquierda simultáneamente hasta tocar a lo máximo que pueda. Hago lo mismo con la otra pierna. (3 Series de 8 a 10 repeticiones).</p>             
            </div>
            <div class="col-md-6 ejer_info2 info_ejer26 justificar menos_espacio">
              <h2>6</h2>
              <p>Acuéstese boca abajo con una almohada debajo del abdomen. Levante el brazo y el hombro derecho conjuntamente con la pierna izquierda, bájelos y relaje, haga lo mismo con el otro lado. (Realice 3 series de 8 a 10 
                repeticiones por lado).</p>             
            </div>
            <div class="col-md-6 ejer_info2 info_ejer27 justificar menos_espacio">
              <h2>7</h2>
              <p>ASiéntese en una silla con los pies separados, mantenga la columna y su cabeza derecha y separado del respaldar, luego con unas mancuernas de 2 a 3 libras, (latas de maíz o un paquete de harina de 1Kg) realice 
                movimientos laterales hasta la altura del hombro y manteniendo las palmas de la mano mirando en dirección al piso. Realice 3 series de 10 repeticiones.</p>             
            </div>
            <div class="col-md-6 ejer_info2 info_ejer28 justificar menos_espacio">
              <h2>8</h2>
              <p>Siéntese en una silla con los pies separados, mantenga la columna y su cabeza derecha y separado del respaldar, luego con unas mancuernas de 2 a 3 libras, latas de maíz o un paquete de harina de 1Kg, realice una 
                flexión y extensión del codo, es decir manteniendo cualquier peso de los antes mencionados y con los brazos al lado del cuerpo intente llevar la mano hacia el hombro. (3 Series de 10 repeticiones).</p>             
            </div>
            <div class="col-md-6 ejer_info2 info_ejer29 justificar menos_espacio">
              <h2>9</h2>
              <p>Siéntese en una silla con los pies separados, deje caer el cuello, luego los hombros y los brazos. Doble el tronco entre las rodillas hasta donde le sea posible. Regrese a la posición normal y relaje. 
                (Realice este movimiento 3 veces).</p>             
            </div>
            <div class="col-md-6 ejer_info2 info_ejer30 justificar menos_espacio">
              <h2>10</h2>
              <p>Acuéstese boca arriba con ambas rodillas en flexión. Suba una rodilla en dirección hacia la cara hasta lo máximo, luego estire la pierna hacia arriba, bájela suavemente hasta el suelo, haga lo mismo con la 
                otra pierna. (1 Serie de 15 repeticiones).</p>             
            </div>
            <div class="col-md-6 ejer_info2 info_ejer31 justificar menos_espacio">
              <h2>11</h2>
              <p>Siéntese en una silla, entrelace los dedos detrás de la cabeza, lleve los codos hacia atrás al máximo, regrese a la posición original, deje caer los brazos y relaje, repítalo 3 veces.</p>             
            </div>
                <div class="col-md-6">
                 <div class="row">
                    <div class= "col-md-3 botonera_ejer2 boton_ejer21">
                      <figure>
                        1
                      </figure>
                    </div>
                    <div class= "col-md-3 botonera_ejer2 boton_ejer22">
                      <figure>
                        2
                      </figure>
                    </div>
                    <div class= "col-md-3 botonera_ejer2 boton_ejer23">
                      <figure>
                        3
                      </figure>
                    </div>
                    <div class= "col-md-3 botonera_ejer2 boton_ejer24">
                      <figure>
                       4
                      </figure>
                    </div>
                  

                  </div>
                  <div class="row">
                     <div class= "col-md-3 botonera_ejer2 boton_ejer25">
                      <figure>
                        5
                      </figure>
                    </div>
                    <div class= "col-md-3 botonera_ejer2 boton_ejer26">
                      <figure>
                        6
                      </figure>
                    </div>
                    <div class= "col-md-3 botonera_ejer2 boton_ejer27">
                      <figure>
                        7
                      </figure>
                    </div>
                    <div class= "col-md-3 botonera_ejer2 boton_ejer28">
                      <figure>
                       8
                      </figure>
                    </div>
            </div>
            <div class="row">
                     <div class= "col-md-3 botonera_ejer2 boton_ejer29">
                      <figure>
                        9
                      </figure>
                    </div>
                    <div class= "col-md-3 botonera_ejer2 boton_ejer30">
                      <figure>
                        10
                      </figure>
                    </div>
                    <div class= "col-md-3 botonera_ejer2 boton_ejer31">
                      <figure>
                        11
                      </figure>
                    </div>
                    
            </div>

               
            </div>
 </div> 
            
        
            <div class="row ">
                <div class="col-md-8 justificar menos_espacio">
                    <h2 class="titulos_ejer ">Prevención de la inactividad física (sedentarismo):</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                      Un factor importante que contribuye a prolongar la vida es practicar suficiente actividad física. La vida sedentaria ha sido 
                      vinculada con fallecimientos por enfermedades crónicas. Se recomienda que los adultos y los adultos mayores realicen (cada 
                      semana) actividad aeróbica de intensidad moderada durante 150 minutos (como caminar en plano con rapidez), o una mezcla 
                      equivalente de los ejercicios de fuerza (anaeróbicos) con actividad aeróbica. 

                    </p>
                    <p>Las personas que hacen ejercicio moderado a enérgico de manera regular tienen menor riesgo de infarto de miocardio, ictus 
                      (accidente cerebrovascular), hipertensión, hiperlipidemia, diabetes mellitus tipo 2, enfermedad diverticular, depresión y 
                      osteoporosis. La evidencia médica reciente avala la práctica de 30 min de actividad física moderada casi todos los días de la 
                      semana, tanto en la prevención primaria como en la secundaria de cardiopatía coronaria.
                </p>    
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

            <div class="row">
              <div class="col-md-12 ">
                <h2 class="titulos_ejer">Beneficios del ejercicio físico</h2>
                <div class="col-md-12 imagen-running">
                <div class= "col-md-1 botonera_relative1">
                    <figure class="botonera_ejercicio3 boton_ejer21 icono_running1">
                        <img src="img/male172.png" width="50px" height="50px" />
                      </figure>
                      <figure class="botonera_ejercicio3 boton_ejer22 icono_running2">
                        <img src="img/lungs4.png" width="50px" height="50px" />
                      </figure>
                    <figure class="botonera_ejercicio3 boton_ejer23 icono_running3">
                        <img src="img/heart288.png" width="50px" height="50px" />
                      </figure>
                    <figure class="botonera_ejercicio3 boton_ejer24 icono_running4">
                        <img src="img/gymnast60.png" width="50px" height="50px" />
                      </figure>
                    <figure class="botonera_ejercicio3 boton_ejer25 icono_running5">
                        <img src="img/washing13.png" width="50px" height="50px" />
                      </figure>
                    <figure class="botonera_ejercicio3 boton_ejer26 icono_running6">
                        <img src="img/man13.png" width="50px" height="50px" />
                      </figure>
                    <figure class="botonera_ejercicio3 boton_ejer27 icono_running7">
                        <img src="img/fat3.png" width="50px" height="50px" />
                      </figure>
                    <figure class="botonera_ejercicio3 boton_ejer28 icono_running8">
                        <img src="img/dog95.png" width="50px" height="50px" />
                      </figure>
                    <figure class="botonera_ejercicio3 boton_ejer29 icono_running9">
                        <img src="img/yogui.png" width="50px" height="50px" />
                      </figure>
                    </div>
                  <div class="col-md-4 botonera_relative">
                    <figure class="botonera_ejercicio4 boton_ejer21 boton_running1 ">
                      <div class="col-md-2">
                        <img src="img/male172.png" width="50px" height="50px" />
                      </div>
                      <div class="col-md-10">
                        <ul>
                          <li class="lista-sin-estilo">Mejora la autoestima</li>
                          <li class="lista-sin-estilo">Disminución del estrés</li>
                          <li class="lista-sin-estilo">Disminución del insomnio</li>
                          <li class="lista-sin-estilo">Aumento de la memoria</li>
                          <li class="lista-sin-estilo">Mejora la concentración</li>
                        </ul>
                      </div>
                        <hr class="raya rayaejer1">
                        
                    </figure>
                    <figure class="botonera_ejercicio4 boton_ejer22 boton_running2 ">
                      <div class="col-md-2">
                        <img src="img/lungs4.png" width="50px" height="50px" />
                      </div>
                      <div class="col-md-10">
                        <ul>
                          <li class="lista-sin-estilo">Aumento de la ventilación pulmonar</li>                          
                        </ul>
                      </div>
                      <hr class="raya rayaejer2">
                    </figure>
                    <figure class="botonera_ejercicio4 boton_ejer23 boton_running3 ">
                      <div class="col-md-2">
                        <img src="img/heart288.png" width="50px" height="50px" />
                      </div>
                      <div class="col-md-10">
                        <ul>
                          <li class="lista-sin-estilo">Aumento de la capacidad aeróbica</li>
                          <li class="lista-sin-estilo">Disminución de la presión arterial</li>
                          <li class="lista-sin-estilo">Mejora del perfil lipídico</li>            
                        </ul>
                      </div>
                      <hr class="raya rayaejer3">
                    </figure>
                    <figure class="botonera_ejercicio4 boton_ejer24 boton_running4 ">
                      <div class="col-md-2">
                        <img src="img/gymnast60.png" width="50px" height="50px" />
                      </div>
                      <div class="col-md-10">
                        <ul>
                          <li class="lista-sin-estilo">Aumento de nuestra fuerza</li>
                          <li class="lista-sin-estilo">Disminución de la tensión muscular</li>
                          <li class="lista-sin-estilo">Aumento en los tiempos de reacción (prevención de caídas)</li>            
                        </ul>
                      </div>
                       <hr class="raya rayaejer4">
                    </figure>
                    <figure class="botonera_ejercicio4 boton_ejer25 boton_running5 ">
                      <div class="col-md-2">
                        <img src="img/washing13.png" width="50px" height="50px" />
                      </div>
                      <div class="col-md-10">
                        <ul>
                          <li class="lista-sin-estilo">Mejora de la sensibilidad a la insulina</li>        
                        </ul>
                      </div>
                      <hr class="raya rayaejer5">
                    </figure>
                    <figure class="botonera_ejercicio4 boton_ejer26 boton_running6 ">
                      <div class="col-md-2">
                        <img src="img/man13.png" width="50px" height="50px" />
                      </div>
                      <div class="col-md-10">
                        <ul>
                          <li class="lista-sin-estilo">Mejora la imágen corporal</li>
                                    
                        </ul>
                      </div>
                      <hr class="raya rayaejer6">
                    </figure>
                    <figure class="botonera_ejercicio4 boton_ejer27 boton_running7 ">
                      <div class="col-md-2">
                        <img src="img/fat3.png" width="50px" height="50px" />
                      </div>
                      <div class="col-md-10">
                        <ul>
                          
                          <li class="lista-sin-estilo">Control del peso corporal</li>   
                          <li class="lista-sin-estilo">Disminución del porcentaje de tejido graso y aumento del tejido magro (muscular).</li>  
                                   
                        </ul>
                      </div>
                      <hr class="raya rayaejer7">
                    </figure>
                    <figure class="botonera_ejercicio4 boton_ejer28 boton_running8 ">
                      <div class="col-md-2">
                        <img src="img/dog95.png" width="50px" height="50px" />
                      </div>
                      <div class="col-md-10">
                        <ul>
                           <li class="lista-sin-estilo">Huesos más fuertes</li> 
                                   
                        </ul>
                      </div>
                      <hr class="raya rayaejer8">
                    </figure>
                    <figure class="botonera_ejercicio4 boton_ejer29 boton_running9 ">
                      <div class="col-md-2">
                        <img src="img/yogui.png" width="50px" height="50px" />
                      </div>
                      <div class="col-md-10">
                        <ul>
                          <li class="lista-sin-estilo">Mejora la imágen corporal</li>   
                                   
                        </ul>
                      </div>
                      <hr class="raya rayaejer9">
                    </figure>
                  </div>
              </div>
              </div>
            </div>
        <div class="row ">
                <div class="col-md-8 justificar menos_espacio">
                    <h2 class="titulos_ejer ">Prevención de obesidad y peso</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                     La evaluación del riesgo de sobrepeso y obesidad comienzan con la medición del IMC (Índice de Masa Corporal) y la circunferencia 
                     abdominal. 

                    </p>
                    <p>El índice de masa corporal (IMC) se expresa en kilogramos divididos por la estatura en metros elevada al cuadrado (kg/m2). 
                      Ejemplo: Si usted mide 1,70 metros de estatura y pesa 70 Kg, su IMC es =  70 dividido por el producto de 1,70 X 1,70; lo cual 
                      resulta en 24,22. Para saber si su IMC de 24,22 kg/m2 es normal, buscar en la siguiente tabla.  

                </p>    
                <table class="col-md-offset-4    table-condensed table table-bordered texto-principio tabla-pequena2">
                        <th colspan="2" class="texto-principio col-md-6">Clasificación del estado nutricional según la OMS (Organización Mundial
                          de la Salud)</th>
                        <tr>
                          <td class="col-md-3">Clasificación</td>
                          <td class="col-md-3">IMC (Kg/m2)</td>
                        </tr>
                        <tr>
                          <td class="col-md-3">Debajo de lo normal</td>
                          <td class="col-md-3">Menor de 18,5</td>
                        </tr>
                        <tr>
                          <td class="col-md-3">Normal</td>
                          <td class="col-md-3">18.5 - 24.9</td>
                        </tr>
                        <tr>
                          <td class="col-md-3">Sobrepeso</td>
                          <td class="col-md-3">25.0 - 29.9</td>
                        </tr>
                         <tr>
                          <td class="col-md-3">Obesidad Tipo I</td>
                          <td class="col-md-3">30.0 - 34.9</td>
                        </tr>
                        <tr>
                          <td class="col-md-3">Obesidad Tipo II</td>
                          <td class="col-md-3">35.0 - 39.9</td>
                        </tr>
                        <tr>
                          <td class="col-md-3">Obesidad Tipo III</td>
                          <td class="col-md-3">Mayor de 40.0</td>
                        </tr>
                    </table>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
              <h5 class="sub-title">La tabla indica que el valor 24,22 cae en el rango 18,5-24,9 , el cual es NORMAL.</h5>




        
            <div class="row ">
                <div class="col-md-8 referencias-ejer ">
                    <h2 class="titulos_ejer">Referencias</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                      <ul>
                      <li class="lista-sin-estilo">1- Francesc Deltell Torres. Manual de nuevas tendencias y herramientas para el actual entrenador personal.
                       2006: 208: 19-20; 202-205. </li>
                      <li class="lista-sin-estilo">2- Isabel Maria Lopez Ruiz. Guía de Actividad física en Alumnos con Handycap. Cap 2 Hipertensión y 
                      Actividad Física. </li>
                      <li class="lista-sin-estilo">3- Susana Moral González. Manual de Tonificación Proyecto Fitness. 2003; 42: 8-12; 30-38. </li>
                      <li class="lista-sin-estilo">4- Edward T Howley y B. Don Franks. Manual del técnico en su salud y fitness editorial Paidotribo. 
                      Colección Fitness. 451; 87-92; 220-231; 419-432. </li>
                      <li class="lista-sin-estilo">5- Folleto de Rehabilitación del Instituto Roberts 1967. </li>
                       <li class="lista-sin-estilo">6- Pignone Michael, Salazar René. Prevención de enfermedades y promoción de la salud. 
                        Diagnostico Clínico y Tratamiento. McGraw-Hill Interamericana Editores, S.A de C.V. 50a Edición. 2012. Cap. 1. 
                        pp. 8-12-18. </li>
                      <li class="lista-sin-estilo">7- Pisunyer Fx. et al. Clinical guideline on the identification, evaluation and treatment of 
                        overweight and obesity in adults. 1998. Nat Hent. pp. XIV, XVII. Lung and Blood Inst.  </li> 

                      </ul>
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
        
    </div>
  </div>
        <?php include 'footer.php' ?>
   
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>
 <script>
     $( ".boton_ejer1" ).on( "click", function() {
         $('.info_ejer1').css('display', 'block');
         $('.info_ejer2').css('display', 'none');
         $('.info_ejer3').css('display', 'none');
         $('.info_ejer4').css('display', 'none');
         
         
      });
     $( ".boton_ejer2" ).on( "click", function() {
         $('.info_ejer1').css('display', 'none');
         $('.info_ejer2').css('display', 'block');
         $('.info_ejer3').css('display', 'none');
         $('.info_ejer4').css('display', 'none');
        
      });
     $( ".boton_ejer3" ).on( "click", function() {
         $('.info_ejer1').css('display', 'none');
         $('.info_ejer2').css('display', 'none');
         $('.info_ejer3').css('display', 'block');
         $('.info_ejer4').css('display', 'none');
         
      });
    $( ".boton_ejer4" ).on( "click", function() {
         $('.info_ejer1').css('display', 'none');
         $('.info_ejer2').css('display', 'none');
         $('.info_ejer3').css('display', 'none');
         $('.info_ejer4').css('display', 'block');
         
      });
   $( ".boton_nutri5" ).on( "click", function() {
         $('.info_ejer1').css('display', 'none');
         $('.info_ejer2').css('display', 'none');
         $('.info_ejer3').css('display', 'none');
         $('.info_ejer4').css('display', 'none');
        
      });

   $( ".boton_ejer21" ).on( "click", function() {
         $('.info_ejer21').css('display', 'block');
         $('.info_ejer22').css('display', 'none');
         $('.info_ejer23').css('display', 'none');
         $('.info_ejer24').css('display', 'none');
         $('.info_ejer25').css('display', 'none');
         $('.info_ejer26').css('display', 'none');
         $('.info_ejer27').css('display', 'none');
         $('.info_ejer28').css('display', 'none');
         $('.info_ejer29').css('display', 'none');
         $('.info_ejer30').css('display', 'none');
         $('.info_ejer31').css('display', 'none');
         $('.botonera_info_ejer2').css('background-color','#FF8601');
         
      });
    $( ".boton_ejer22" ).on( "click", function() {
         $('.info_ejer21').css('display', 'none');
         $('.info_ejer22').css('display', 'block');
         $('.info_ejer23').css('display', 'none');
         $('.info_ejer24').css('display', 'none');
         $('.info_ejer25').css('display', 'none');
         $('.info_ejer26').css('display', 'none');
         $('.info_ejer27').css('display', 'none');
         $('.info_ejer28').css('display', 'none');
         $('.info_ejer29').css('display', 'none');
         $('.info_ejer30').css('display', 'none');
         $('.info_ejer31').css('display', 'none');
         $('.botonera_info_ejer2').css('background-color','#FF8C0D');
         
      });
    $( ".boton_ejer23" ).on( "click", function() {
         $('.info_ejer21').css('display', 'none');
         $('.info_ejer22').css('display', 'none');
         $('.info_ejer23').css('display', 'block');
         $('.info_ejer24').css('display', 'none');
         $('.info_ejer25').css('display', 'none');
         $('.info_ejer26').css('display', 'none');
         $('.info_ejer27').css('display', 'none');
         $('.info_ejer28').css('display', 'none');
         $('.info_ejer29').css('display', 'none');
         $('.info_ejer30').css('display', 'none');
         $('.info_ejer31').css('display', 'none');
         $('.botonera_info_ejer2').css('background-color','#FF931C');
         
      });
    $( ".boton_ejer24" ).on( "click", function() {
         $('.info_ejer21').css('display', 'none');
         $('.info_ejer22').css('display', 'none');
         $('.info_ejer23').css('display', 'none');
         $('.info_ejer24').css('display', 'block');
         $('.info_ejer25').css('display', 'none');
         $('.info_ejer26').css('display', 'none');
         $('.info_ejer27').css('display', 'none');
         $('.info_ejer28').css('display', 'none');
         $('.info_ejer29').css('display', 'none');
         $('.info_ejer30').css('display', 'none');
         $('.info_ejer31').css('display', 'none');
         $('.botonera_info_ejer2').css('background-color','#FF9A28');
         
      });
    $( ".boton_ejer25" ).on( "click", function() {
         $('.info_ejer21').css('display', 'none');
         $('.info_ejer22').css('display', 'none');
         $('.info_ejer23').css('display', 'none');
         $('.info_ejer24').css('display', 'none');
         $('.info_ejer25').css('display', 'block');
         $('.info_ejer26').css('display', 'none');
         $('.info_ejer27').css('display', 'none');
         $('.info_ejer28').css('display', 'none');
         $('.info_ejer29').css('display', 'none');
         $('.info_ejer30').css('display', 'none');
         $('.info_ejer31').css('display', 'none');
         $('.botonera_info_ejer2').css('background-color','#FF9F36');
         
      });
    $( ".boton_ejer26" ).on( "click", function() {
         $('.info_ejer21').css('display', 'none');
         $('.info_ejer22').css('display', 'none');
         $('.info_ejer23').css('display', 'none');
         $('.info_ejer24').css('display', 'none');
         $('.info_ejer25').css('display', 'none');
         $('.info_ejer26').css('display', 'block');
         $('.info_ejer27').css('display', 'none');
         $('.info_ejer28').css('display', 'none');
         $('.info_ejer29').css('display', 'none');
         $('.info_ejer30').css('display', 'none');
         $('.info_ejer31').css('display', 'none');
         $('.botonera_info_ejer2').css('background-color','#FFA544');
         
      });
     $( ".boton_ejer27" ).on( "click", function() {
         $('.info_ejer21').css('display', 'none');
         $('.info_ejer22').css('display', 'none');
         $('.info_ejer23').css('display', 'none');
         $('.info_ejer24').css('display', 'none');
         $('.info_ejer25').css('display', 'none');
         $('.info_ejer26').css('display', 'none');
         $('.info_ejer27').css('display', 'block');
         $('.info_ejer28').css('display', 'none');
         $('.info_ejer29').css('display', 'none');
         $('.info_ejer30').css('display', 'none');
         $('.info_ejer31').css('display', 'none');
         $('.botonera_info_ejer2').css('background-color','#FFB35F');
         
      });
      $( ".boton_ejer28" ).on( "click", function() {
         $('.info_ejer21').css('display', 'none');
         $('.info_ejer22').css('display', 'none');
         $('.info_ejer23').css('display', 'none');
         $('.info_ejer24').css('display', 'none');
         $('.info_ejer25').css('display', 'none');
         $('.info_ejer26').css('display', 'none');
         $('.info_ejer27').css('display', 'none');
         $('.info_ejer28').css('display', 'block');
         $('.info_ejer29').css('display', 'none');
         $('.info_ejer30').css('display', 'none');
         $('.info_ejer31').css('display', 'none');
         $('.botonera_info_ejer2').css('background-color','#FFB35F');
         
      });
      $( ".boton_ejer29" ).on( "click", function() {
         $('.info_ejer21').css('display', 'none');
         $('.info_ejer22').css('display', 'none');
         $('.info_ejer23').css('display', 'none');
         $('.info_ejer24').css('display', 'none');
         $('.info_ejer25').css('display', 'none');
         $('.info_ejer26').css('display', 'none');
         $('.info_ejer27').css('display', 'none');
         $('.info_ejer28').css('display', 'none');
         $('.info_ejer29').css('display', 'block');
         $('.info_ejer30').css('display', 'none');
         $('.info_ejer31').css('display', 'none');
         $('.botonera_info_ejer2').css('background-color','#FFB86C');
         
      });
      $( ".boton_ejer30" ).on( "click", function() {
         $('.info_ejer21').css('display', 'none');
         $('.info_ejer22').css('display', 'none');
         $('.info_ejer23').css('display', 'none');
         $('.info_ejer24').css('display', 'none');
         $('.info_ejer25').css('display', 'none');
         $('.info_ejer26').css('display', 'none');
         $('.info_ejer27').css('display', 'none');
         $('.info_ejer28').css('display', 'none');
         $('.info_ejer29').css('display', 'none');
         $('.info_ejer30').css('display', 'block');
         $('.info_ejer31').css('display', 'none');
         $('.botonera_info_ejer2').css('background-color','#FFBF78');
         
      });
      $( ".boton_ejer31" ).on( "click", function() {
         $('.info_ejer21').css('display', 'none');
         $('.info_ejer22').css('display', 'none');
         $('.info_ejer23').css('display', 'none');
         $('.info_ejer24').css('display', 'none');
         $('.info_ejer25').css('display', 'none');
         $('.info_ejer26').css('display', 'none');
         $('.info_ejer27').css('display', 'none');
         $('.info_ejer28').css('display', 'none');
         $('.info_ejer29').css('display', 'none');
         $('.info_ejer30').css('display', 'none');
         $('.info_ejer31').css('display', 'block');
         $('.botonera_info_ejer2').css('background-color','#FFC586');
         

      });

      $( ".icono_running1" ).on( "click", function() {
         $('.boton_running1').css('display', 'block');
         $('.boton_running2').css('display', 'none');
         $('.boton_running3').css('display', 'none');
         $('.boton_running4').css('display', 'none');
         $('.boton_running5').css('display', 'none');
         $('.boton_running6').css('display', 'none');
         $('.boton_running7').css('display', 'none');
         $('.boton_running8').css('display', 'none');
         $('.boton_running9').css('display', 'none');
         $('.botonera_relative').css('left','200px');
         $('.botonera_relative').css('top','28px');
      });
      $( ".icono_running2" ).on( "click", function() {
         $('.boton_running1').css('display', 'none');
         $('.boton_running2').css('display', 'block');
         $('.boton_running3').css('display', 'none');
         $('.boton_running4').css('display', 'none');
         $('.boton_running5').css('display', 'none');
         $('.boton_running6').css('display', 'none');
         $('.boton_running7').css('display', 'none');
         $('.boton_running8').css('display', 'none');
         $('.boton_running9').css('display', 'none');
          $('.botonera_relative').css('top','266px');
      });
      $( ".icono_running3" ).on( "click", function() {
         $('.boton_running1').css('display', 'none');
         $('.boton_running2').css('display', 'none');
         $('.boton_running3').css('display', 'block');
         $('.boton_running4').css('display', 'none');
         $('.boton_running5').css('display', 'none');
         $('.boton_running6').css('display', 'none');
         $('.boton_running7').css('display', 'none');
         $('.boton_running8').css('display', 'none');
         $('.boton_running9').css('display', 'none');
         $('.botonera_relative').css('top','275px'); 
      });
      $( ".icono_running4" ).on( "click", function() {
         $('.boton_running1').css('display', 'none');
         $('.boton_running2').css('display', 'none');
         $('.boton_running3').css('display', 'none');
         $('.boton_running4').css('display', 'block');
         $('.boton_running5').css('display', 'none');
         $('.boton_running6').css('display', 'none');
         $('.boton_running7').css('display', 'none');
         $('.boton_running8').css('display', 'none');
         $('.boton_running9').css('display', 'none');
         $('.botonera_relative').css('top','184px');
      });
      $( ".icono_running5" ).on( "click", function() {
         $('.boton_running1').css('display', 'none');
         $('.boton_running2').css('display', 'none');
         $('.boton_running3').css('display', 'none');
         $('.boton_running4').css('display', 'none');
         $('.boton_running5').css('display', 'block');
         $('.boton_running6').css('display', 'none');
         $('.boton_running7').css('display', 'none');
         $('.boton_running8').css('display', 'none');
         $('.boton_running9').css('display', 'none');
         $('.botonera_relative').css('top','379px');
      });
       $( ".icono_running6" ).on( "click", function() {
         $('.boton_running1').css('display', 'none');
         $('.boton_running2').css('display', 'none');
         $('.boton_running3').css('display', 'none');
         $('.boton_running4').css('display', 'none');
         $('.boton_running5').css('display', 'none');
         $('.boton_running6').css('display', 'block');
         $('.boton_running7').css('display', 'none');
         $('.boton_running8').css('display', 'none');
         $('.boton_running9').css('display', 'none');
        $('.botonera_relative').css('top','437px');
      });
       $( ".icono_running7" ).on( "click", function() {
         $('.boton_running1').css('display', 'none');
         $('.boton_running2').css('display', 'none');
         $('.boton_running3').css('display', 'none');
         $('.boton_running4').css('display', 'none');
         $('.boton_running5').css('display', 'none');
         $('.boton_running6').css('display', 'none');
         $('.boton_running7').css('display', 'block');
         $('.boton_running8').css('display', 'none');
         $('.boton_running9').css('display', 'none');
         $('.botonera_relative').css('top','533px');
      });
        $( ".icono_running8" ).on( "click", function() {
         $('.boton_running1').css('display', 'none');
         $('.boton_running2').css('display', 'none');
         $('.boton_running3').css('display', 'none');
         $('.boton_running4').css('display', 'none');
         $('.boton_running5').css('display', 'none');
         $('.boton_running6').css('display', 'none');
         $('.boton_running7').css('display', 'none');
         $('.boton_running8').css('display', 'block');
         $('.boton_running9').css('display', 'none');
         $('.botonera_relative').css('top','722px');
      });
         $( ".icono_running9" ).on( "click", function() {
         $('.boton_running1').css('display', 'none');
         $('.boton_running2').css('display', 'none');
         $('.boton_running3').css('display', 'none');
         $('.boton_running4').css('display', 'none');
         $('.boton_running5').css('display', 'none');
         $('.boton_running6').css('display', 'none');
         $('.boton_running7').css('display', 'none');
         $('.boton_running8').css('display', 'none');
         $('.boton_running9').css('display', 'block');
         $('.botonera_relative').css('top','806px');
      });
    </script>

</body>
</html>