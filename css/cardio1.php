<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>

    <body>
    <?php include 'mainnav.php' ?>
    <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/banner_cardio.jpg" >
     
        <div class="container title">
         <h1 class="heading-interno">CARDIOLOGÍA</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
<div  class=" white pad-30">
        <div id="vida"></div>
        <div class="container">
            <ol class="breadcrumb">
            <li><a href="cardio.php">Cardiología</a></li>
            <li class="active">Colesterol</li>
          </ol>
                <!-- <h1>DIFTERIA</h1> -->
            <div class="row ">
                <div class="col-md-6 justificar menos_espacio">
                    <h2>Origen, utilidad y transformación del colesterol</h2>
                    <p>
                       El colesterol es  una sustancia lipídica o grasa corporal, indispensable para el  funcionamiento celular, participa en la síntesis 
                       de  hormonas femeninas y masculinas; se utiliza  para fabricar el cortisol y sales biliares indispensables para digerir las grasas 
                       y excretar parte del colesterol sintetizado en el hígado y para Ia síntesis de vitamina D esencial para el metabolismo de los huesos.
                      
                    </p>
                    <p>
                      Adicionalmente, el colesterol es un marcador de salud cardiovascular porque se vincula estrechamente con la formación de ateromas o 
                       placas que obstruyen las arterias.
                    </p>
                    
                </div>
                <div class="col-md-6">
                  <img class="img-2" src="img/photo_ejemplo3.jpg" />
                </div>
            </div>
            <div class="row ">
              <h2>Tipos de colesterol: Los llamados colesterol "bueno" y "malo"</h2>
                <div class="col-md-6 justificar menos_espacio2">
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       Coloquialmente se llama colesterol "buena" el que sale de las  arterias y  reduce el  tamaño de los  ateromas u obstrucciones; 
                       medicamente se  le llama HDL, que significa lipoproteína de alta densidad. 
                    </p>
                    <p>
                      El colesterol llamado "malo" es el  que se deposita en  las arterias y las obstruye en forma de placas o ateromas; 
                    </p>
                </div>
                <div class="col-md-6">
                  <p>
                    medicamente se le llama LDL, que significa  lipoproteína de baja densidad.
                  </p>
                    <p>
                        Cuantitativamente, tres  cuartas partes del  colesterol en sangre son LDL y una cuarta parte es HDL. Por ello cuando sube  el  
                        colesterol total, suele  deberse a elevaciones de LDL más  que de HDL.
                    </p>
                </div>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                
            </div>
            <div class="row fila-slider">
              <div id="myCarouselCardio1" class="carousel slide pfizer-carousel" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators hidden-xs">
                <li data-target="#myCarouselCardio1" data-slide-to="0"></li>
                <li data-target="#myCarouselCardio1" data-slide-to="1"></li>
                <li data-target="#myCarouselCardio1" data-slide-to="2"></li>
                <li data-target="#myCarouselCardio1" data-slide-to="3"></li>
               
            </ol>
            <div class="col-sm-7 pfizer-slider">
                <div class="carousel-inner" role="listbox">
                    <div class="item active" style="background:#33D7F2;">
                        <!-- <img class="first-slide" src="images/WB26_003.jpg" alt="First slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_1">
                                  <p>
                                   El colesterol aumentado  en  sangre no produce ningún síntoma, actúa en silencio clínico. Lo más frecuente es  que 
                                   las dislipoproteinemias, raramente provoquen síntomas o signos clínicos. Es necesario usar análisis de  laboratorio.
                                </p>
                                <br>
                                <p>
                                   Los síntomas  pueden variar: desde <b>la angina de pecho</b> y <b>el infarto de miocardio</b> que producen dolor 
                                   hasta la trombosis cerebral que se traduce en hemiplejia y paraliza una parte del cuerpo. 
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#25BBCA;">
                        <!-- <img class="second-slide" src="images/WB33_009.jpeg" alt="Second slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_2">
                                <p>
                                 Es  recomendable medir el  colesterol y sus fracciones de lipoproteínas  LDL y HDL después de  los 30 años de edad.
                                  Si un  niño o un  adolescente es hijo  de una persona que ha  sufrido infarto de miocardio es recomendable medir 
                                  su colesterol en sangre.
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#2394A4;">
                        <!-- <img class="third-slide" src="images/CW40_085.jpg" alt="Third slide"> -->
                        
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text">
                                <p>La dieta rica  en  colesterol aumentara este  lípido  en  la  sangre, pero también el colesterol y la obesidad 
                                  contribuyen a aumentar el colesterol. Por esa razón los médicos recomiendan el ejercicio moderado diario en un 
                                  parque o en coso; junto con el aumento de Ia actividad física cotidiana. No obstante, el hipotiroidismo, la 
                                  diabetes, el síndrome metabó1ico y la insuficiencia renal aumentan el colesterol en sangre; así como algunos 
                                  medicamentos.
                                </p>
                                <br>
                                <p>
                                    Contenido de colesterol de algunos elementos:
                                    <ul>
                                        <li>Sin Colesterol</li>
                                        <li>Colesterol Medio</li>
                                        <li>Colesterol Alto</li>
                                    </ul>
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background:#13717D;">
                        <!-- <img class="fourth-slide" src="images/WB30_008.jpg" alt="Fourth slide"> -->
                        <div class="container">
                            <div class="carousel-caption">
                              <div class="carousel-caption-text cardio_text_1">
                                 <p>
                                  <ul>
                                      <li>Hipercolesterolemia aislada (Aumento del colesterol solamente)</li>
                                     <li>Hiperlipidemia mixta (Aumento del colesterol y triglicéridos)</li>
                                     <li>Hipertrigliceridemia aislada (Aumento de los triglicéridos solamente)</li>
                                     <li>Hipoalfalipoproteinemia (Disminución del colesterol "buena" o HDL)</li>
                                  </ul>
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="hidden-xs col-sm-5 pfizer-slider-indicators" >
                <ol class="carousel-indicators">
                    <li data-target="#myCarouselCardio1" data-slide-to="0" style="background:#33D7F2;"><p>El colesterol y la enfermedad cardiovascular</p></li>
                    <li data-target="#myCarouselCardio1" data-slide-to="1" style="background:#25BBCA;"><p>La medición del colesterol</p></li>
                    <li data-target="#myCarouselCardio1" data-slide-to="2" style="background:#2394A4;"><p>Factores que aumentan el colesterol</p></li>
                    <li data-target="#myCarouselCardio1" data-slide-to="3" style="background:#13717D;"><p>Clasificación de las alteraciones de las grasas que circulan en la sangre</p></li>
                    </ol>

            </div>
            <a class="left carousel-control visible-xs" href="#myCarouselCardio1" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left visi" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control visible-xs" href="#myCarouselCardio1" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
        </div>
            </div>
            
           
            <div class="row ">
                <div class="col-md-12 ">
                    <h2>CONTENIDO DE COLESTEROL EN ALGUNOS ALIMENTOS</h2>
                    <p>En las frutas, las verduras, las hortalizas,  los frutos secos, el pan y los vegetales no hay  colesterol, a menos que se haya 
                                   agregado a ellos grasas  animales en una  receta para su cocción. La siguiente tabla contiene una lista de alimentos con su cantidad
                                    relativa de colesterol: </p> <br>
                   <table class="table texto-principio table-bordered">
                       <th colspan="6" class="texto-principio">Miligramos de colesterol por 100 gramos de alimento</th>
                       <tr>
                           <td class="col-md-3">Arroz</td>
                           <td class="col-md-1">0</td>
                           <td class="col-md-3">Helado cremoso</td>
                           <td class="col-md-1">25</td>
                           <td class="col-md-3">Pavo</td>
                           <td class="col-md-1">72</td>
                       </tr>
                       <tr>
                           <td class="col-md-3">Atún fresco</td>
                           <td class="col-md-1">38</td>
                           <td class="col-md-3">Jamón cocido</td>
                           <td class="col-md-1">52</td>
                           <td class="col-md-3">Pollo</td>
                           <td class="col-md-1">75</td>
                       </tr>
                       <tr>
                           <td class="col-md-3 sombreado">Calamar</td>
                           <td class="col-md-1 sombreado">150</td>
                           <td class="col-md-3">Jamón serrano</td>
                           <td class="col-md-1">70</td>
                           <td class="col-md-3 sombreado">Pulpo</td>
                           <td class="col-md-1 sombreado">129</td>
                       </tr>
                       <tr>
                           <td class="col-md-3">Clara de huevo</td>
                           <td class="col-md-1">0</td>
                           <td class="col-md-3">Leche descremada</td>
                           <td class="col-md-1">2</td>
                           <td class="col-md-3">Queso amarillo. parmesano</td>
                           <td class="col-md-1">40</td>
                       </tr>
                       <tr>
                           <td class="col-md-3">Conejo</td>
                           <td class="col-md-1">50</td>
                           <td class="col-md-3">Leche completa</td>
                           <td class="col-md-1">14</td>
                           <td class="col-md-3">Queso de cabra</td>
                           <td class="col-md-1">16</td>
                       </tr>
                       <tr>
                           <td class="col-md-3">Costillas de cerdo</td>
                           <td class="col-md-1">78</td>
                           <td class="col-md-3 sombreado">Mantequilla</td>
                           <td class="col-md-1 sombreado">250</td>
                           <td class="col-md-3">Trucha</td>
                           <td class="col-md-1">56</td>
                       </tr>
                       <tr>
                           <td class="col-md-3">Carne de res magra</td>
                           <td class="col-md-1">80</td>
                           <td class="col-md-3 sombreado">Mayonesa con yema de huevo</td>
                           <td class="col-md-1 sombreado">150</td>
                           <td class="col-md-3 sombreado">Vísceras de res o cochino</td>
                           <td class="col-md-1 sombreado">806</td>
                       </tr>
                       <tr>
                           <td class="col-md-3 sombreado">Pasta de hígado</td>
                           <td class="col-md-1 sombreado">380</td>
                           <td class="col-md-3">Mejillón</td>
                           <td class="col-md-1">50</td>
                           <td class="col-md-3 sombreado">Yema de huevo</td>
                           <td class="col-md-1 sombreado">100</td>
                       </tr>
                       <tr>
                           <td class="col-md-3 sombreado">Camarón, langostino, langosta</td>
                           <td class="col-md-1 sombreado">152</td>
                           <td class="col-md-3">Merluza</td>
                           <td class="col-md-1">23</td>
                           <td class="col-md-3"></td>
                           <td class="col-md-1"></td>
                       </tr>
                   </table>
                   <br>
                    <h5 class="sub-title">Están sombreados los que tienen más colesterol.</h5>
                 
                </div>
            </div> 
            
            <div class="row info hidden-xs">
                <div class="col-md-12 ">
                    <h2>Clasificación de las alteraciones de las grasas según su causa</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                  <div class="col-md-5">
                    <div class= "col-md-6 botonera boton_numero boton_cardio1">
                        <figure>
                        1
                        </figure>
                    </div>
                    <div class= "col-md-6 botonera boton_numero boton_cardio2">
                      <figure>
                        2
                      </figure>
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="col-md-12 cardio_info info_cardio1">
                    <p>
                        
                        <ul>
                            <li class="lista-sin-estilo"><b>Primarias</b>
                              <p>Ocurren por  alteraciones en los genes y se heredan.</p>
                            </li>
                          </ul>
                          
                        </p>
                    </div>
                   </div>
                  <div class="col-md-7">
                     <div class="col-md-12 cardio_info info_cardio2">
                        <p>
                        <ul>
                            <li class="lista-sin-estilo"><b>Secundarias</b>
                              <p>
                                Aumenta el colesterol y los triglicéridos a consecuencia de enfermedades como:
                                <ul>
                                  <li>Diábetes</li>
                                  <li>Síndrome metabó1ico: es el conjunto de hipertensión arterial, aumento de azúcar en sangre, colesterol,
                                  HDL (buena) bajo y circunferencia abdominal aumentada.</li>
                                  <li>Por enfermedad del hígado o riñón.</li>
                                  <li>Hipotiroidismo (Déficit de hormona tiroidea).</li>
                                  <li>Dieta excesiva en  grasas, inactividad física.</li>
                                  <li>Uso de medicamentos, como esteroides, algunos antihipertensivos (tiazidas, bloqueantes beta).</li>
                                </ul>
                              </p>

                            </li>
                            
                        </ul>
                      </p>
                    </div>
                  </div>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div> 
            <div class="visible-xs">
            <div class="boton-wide boton_cardio1">
              <figure>
                <img src="img/number41.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                <p>
                        
                        <ul>
                            <li class="lista-sin-estilo"><b>Primarias</b>
                              <p>Ocurren por  alteraciones en los genes y se heredan.</p>
                            </li>
                          </ul>
                          
                        </p>
              </div>
            </div>
            <div class="boton-wide boton_cardio2">
              <figure>
                <img src="img/number40.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                  <p>
                    <ul>
                        <li class="lista-sin-estilo"><b>Secundarias</b>
                          <p>
                            Aumenta el colesterol y los triglicéridos a consecuencia de enfermedades como:
                            <ul>
                              <li>Diábetes</li>
                              <li>Síndrome metabó1ico: es el conjunto de hipertensión arterial, aumento de azúcar en sangre, colesterol,
                              HDL (buena) bajo y circunferencia abdominal aumentada.</li>
                              <li>Por enfermedad del hígado o riñón.</li>
                              <li>Hipotiroidismo (Déficit de hormona tiroidea).</li>
                              <li>Dieta excesiva en  grasas, inactividad física.</li>
                              <li>Uso de medicamentos, como esteroides, algunos antihipertensivos (tiazidas, bloqueantes beta).</li>
                            </ul>
                          </p>
                          <br>
                          <br>
                        </li>
                      </ul>
                    </p>
                </div>
              </div>
            </div>
        </div>
      </div>
 <?php include 'footer.php' ?>
     <script>
     $( ".boton_cardio1" ).on( "mouseover", function() {
         $('.info_cardio1').css('display', 'block');
         $('.info_cardio2').css('display', 'none');
         
      });
     $( ".boton_cardio2" ).on( "mouseover", function() {
         $('.info_cardio1').css('display', 'none');
         $('.info_cardio2').css('display', 'block');

         
      });
     </script>
     <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});

</script>
<script>
  $(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            interval: false
        });
    });
});​
</script>
</body>
</html>
    