<!DOCTYPE html>
<html>
    <?php include 'head.php' ?>
    <body class="vacunas-pag">
        <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/vacunas-banner.jpg" >
            <?php include 'mainnav.php' ?>
            <div class="container title">
                <h1 class="heading-interno">INMUNIZACIÓN</h1>
            </div>
        </div>
        <?php include 'sintomasnav.php' ?>

        <div class="container">
            <div class="row dolor-inter-sup ">
                <!-- Juanchi <div class="col-xs-6"> -->
                <div class="col-xs-6 col-xs-12 hover_img">
                    <!-- Juanchi <a class="sin_decoracio img-10" href="bacteriana.php"> -->
                    <a class="sin_decoracion img-10" href="bacteriana.php">
                        <img src="img/vacunas-intermedias-bacterianas.jpg">
                        <!-- Juanchi <div class="vista-inter-vacu bact_caption"> -->
                        <div class="vista-inter-vacu">
                            Enfermedades Bacterianas Inmunoprevenibles
                        </div>
                    </a>
                </div>
                <!-- Juanchi <div class="col-xs-6"> -->
                <div class="col-xs-6 col-xs-12 hover_img">
                    <a class="sin_decoracion img-10" href="virales.php">
                        <img src="img/vacunas-intermedias-virales.jpg">
                        <!-- Juanchi <div class="vista-inter-vacu" style = "font-size: 24px"> -->
                        <div class="vista-inter-vacu" style = "font-size: 24px">
                           Enfermedades Virales Inmunoprevenibles
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <?php include 'footer.php' ?>
   
        <script type="text/javascript">
            function init() {
                         window.addEventListener('scroll', function(e){
                        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                            shrinkOn = 100,
                            header = document.querySelector("nav");
                        if (distanceY > shrinkOn) {
                            classie.add(header,"smaller");
                        } else {
                            if (classie.has(header,"smaller")) {
                                classie.remove(header,"smaller");
                            }
                        }
                    });
                }
                window.onload = init();
                $('.dropdown-toggle').dropdown();
                jQuery('ul.nav li.dropdown').hover(function() {
                  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
                }, function() {
                  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('a[href^="#"]').on('click', function(event) {

                    var target = $( $(this).attr('href') );

                    if( target.length ) {
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                    }

                });
            });
        </script>
    </body>
</html>