<div  class="container-fluid padding-vacunas-interno">
        
                <!-- <h1>Polio</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>¿Qué es la polio?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>  La polio es una infección viral sumamente contagiosa que puede provocar parálisis.

                            </li>
                            <li>
                                Antes, la polio era más frecuente, pero hoy en día es muy infrecuente gracias a la vacunación.
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h2>¿De qué manera la polio puede afectar a mi hijo?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                             La polio puede provocar: 
                            <ul>
                                <li>Parálisis permanente, por lo general de una pierna. </li>
                                <li>Debilidad muscular permanente. </li>
                                <li>Discapacidad de por vida. </li>
                                <li>Meningitis, aunque esto es infrecuente.</li>
                            </ul>
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h2>¿Mi hijo está en riesgo?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h2>¿Cómo se transmite?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La polio se encuentra en la materia fecal de las personas infectadas y se transmite fácilmente a las manos y los objetos.
                            </li>
                            <li>Las personas pueden transmitir el virus aunque no presenten síntomas.</li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
    