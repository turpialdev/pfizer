<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    
     <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/dolor-banner.jpg">
     <?php include 'mainnav.php' ?>
        <div class="container title">
         <h1 class="heading-interno">DOLOR</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
<div  class="container-fluid white pad-30">
        <div id="vida"></div>
        <div class="container">
            <ol class="breadcrumb">
            <li><a href="dolor.php">Dolor</a></li>
            <li class="active">Dolor</li>
          </ol>
               <!--  <h1>DOLOR</h1> -->
            <div class="row info">
                <div class="col-md-6 menos_espacio justificar">
                    <h2>¿Qué es el dolor?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        El dolor es una manifestación del cuerpo que responde a una lesión, enfermedad o molestia, y que manifiesta un síntoma 
                        (lo que el paciente le expresa al médico) y un motivo muy frecuente de consulta. La dificultad de describir esta sensación, 
                        llevó a la Asociación Internacional para el Estudio del Dolor a definir dolor como una experiencia desagradable sensitiva y 
                        emocional que se asocia a una lesión real o potencial de los tejidos. Dichaexperiencia es siempre "subjetiva", de tal modo que se 
                        debe admitir y creer que la intensidad del dolor es la que el paciente exprese.
                    </p>
                    <p>
                        De una manera simple, el dolor es una sensación de molestia que provoca en el paciente alteraciones físicas, psicológicas, sociales 
                        y laborales, capaces de afectar su bienestar y calidad de vida.
                    </p>
                    <p>
                        El dolor no siempre es una manifestación de una enfermedad grave, y con cierta frecuencia, puede ser también un efecto secundario de
                        algún tratamiento que el paciente esté recibiendo, o bien, no tener relación con una enfermedad específica ni con ninguna otra causa.
                    </p>
                    <p>
                        No todo dolor tiene el mismo origen, ni se resuelve con los mismos tratamientos. Por eso, es importante que al consultar al médico, 
                        se le explique cómo es ese dolor, dónde se presenta, a qué se atribuye, desde cuándo se siente, cómo se distribuye en el cuerpo o área
                         afectada, cómo varia, dónde se localiza, etc. 
                    </p>
 
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6  hidden-xs">
                    <img class="img-dolor1" src="img/backpain.jpg" />
                  
 
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-xs-12 visible-xs ">
                    <img src="img/backpain.jpg" />
                  
 
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row info">
                <div class="col-md-12">
                    <h2>Tipos de Dolor</h2>
                </div>
            </div>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
            <div class="row fila-slider">
                <div class="col-md-12">
                    <div id="myCarouselCardio2" class="carousel slide pfizer-carousel" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators hidden-xs">
                        <li data-target="#myCarouselCardio2" data-slide-to="0"></li>
                        <li data-target="#myCarouselCardio2" data-slide-to="1"></li>
                        <li data-target="#myCarouselCardio2" data-slide-to="2"></li>
                        
                    </ol>
                    <div class="col-sm-7 pfizer-slider">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active" style="background:#33D7F2;">
                                <!-- <img class="first-slide" src="images/WB26_003.jpg" alt="First slide"> -->
                                <div class="container">
                                    <div class="carousel-caption">
                                        <div class="carousel-caption-text cardio_text_3">
                                            <ul>
                                                <li>
                                                    <h4>Dolor Agudo:</h4>
                                                    <p>Sensación dolorosa de corta duración, que puede ser transitoria y fugaz después de sufrir un traumatismo moderado u 
                                                        otras posibles causas médico-quirúrgicas. Es limitado en el tiempo (de tres a seis meses). Por ejemplo, el dolor del
                                                         cólico renal, el provocado por un golpe o posterior a una fractura, entre otros.
                                                     </p> 
                                                </li>
                                                <br>
                                                <li>
                                                    <h4>Dolor Crónico:</h4>
                                                    <p>Es aquella sensación dolorosa prolongada, con más de seis meses de duración. Desde hace casi 50 años, el dolor crónico se definió como una
                                                     entidad médica propia: "Dolor enfermedad", que puede llegar a ser más importante que la propia enfermedad que lo inició y a veces de 
                                                     intensidad severa. Son ejemplos el dolor lumbar crónico, el dolor de las personas que padecen artrosis.
                                                 </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item" style="background:#25BBCA;">
                                <!-- <img class="second-slide" src="images/WB33_009.jpeg" alt="Second slide"> -->
                                <div class="container">
                                    <div class="carousel-caption">
                                        <div class="carousel-caption-text cardio_text_3">
                                            <ul>
                                                <li>
                                                    <h4>Dolor Somático (del latín cuerpo):</h4>
                                                        <p>
                                                            Se localiza con precisión y generalmente aparece en el lugar donde se produjo el daño o lesión, pudiendo ser, 
                                                            superficial o profundo. Suele ser agudo y bien definido, de origen cutáneo (piel) o más profundo en zonas 
                                                            músculo-esqueléticas, huesos, cartílagos, vasos, o en membranas que recubren los pulmones y el contenido abdominal 
                                                            como la pleura y el peritoneo.
                                                        </p>
                                                </li>
                                                <li>
                                                    <h4>Dolor Visceral:</h4>
                                                        <p>Es una variedad de dolor profundo, sin localización específica, sordo, difuso, y a veces distante de la zona afectada originalmente. 
                                                            El cuadro clínico va desde simples molestias a dolor muy intenso. Hay ocasiones en que grandes destrucciones de un órgano no se acompañan
                                                            de dolor importante como por ejemplo, el pulmón. Puede ser también intermitente,  como  por  ejemplo  el  dolor  abdominal  que  es  a 
                                                            manera  de  cólico,  y no constante. Se suele acompañar de otros síntomas como náuseas y vómitos, y tener un componente emocional intenso. 
                                                            Se produce por activación de receptores (inflamación, compresión, distensión).
                                                        </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item" style="background:#2394A4;">
                                <!-- <img class="third-slide" src="images/CW40_085.jpg" alt="Third slide"> -->
                                <div class="container">
                                    <div class="carousel-caption">
                                      <div class="carousel-caption-text cardio_text_3">
                                        <ul>
                                            <li>
                                                <h4>Dolor Nociceptivo:</h4>
                                                <p>Respuesta normal y adecuada o fisiológica a un estímulo que causa dolor.</p>
                                                <ul>
                                                    <li>
                                                        Tiene una causa (trauma, quemadura, lesión, enfermedad)
                                                    </li>
                                                    <li>
                                                        Dura mientras persiste el estímulo
                                                    </li>
                                                    <li>
                                                        Función protectora (restauradora)
                                                    </li>
                                                    <li>
                                                        Descrito como latido, punzada, molestia o entumecimiento
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                               <h4> Dolor Neuropático: </h4>
                                               <p>Producido por una respuesta anormal o inadecuada del sistema nervioso, debido a una lesión o mal funcionamiento de las
                                                células nerviosas.</p>
                                                <ul>
                                                    <li>
                                                        Originado en una lesión, estímulo directo o disfunción del sistema nervioso central o de nervios periféricos.
                                                    </li>
                                                    <li>
                                                        El  paciente  lo  percibe  como  sensaciones  extrañas:  quemazón,  descarga eléctrica, pinchazos. 
                                                        Se puede asociar a adormecimiento u hormigueos.
                                                    </li>
                                                    <li>
                                                        Dolor que perdió su función protectora, generalmente se vuelve crónico.
                                                    </li>
                                                    <li>
                                                        Ejemplos de enfermedades que cursan con dolor neuropático son la neuropatía diabética, la neuralgia postherpética,
                                                         la neuralgia del trigémino. En cualquier caso es recomendable que consulte a su médico.
                                                    </li>
                                                </ul>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="hidden-xs col-sm-5 pfizer-slider-indicators" >
                        <ol class="carousel-indicators">
                            <li class="botones3 slide1 pf-on" data-target="#myCarouselCardio2" data-slide-to="0" ><p> Según Duración</p></li>
                            <li class="botones3 slide2 apagado pf-off" data-target="#myCarouselCardio2" data-slide-to="1" ><p> Según su localización</p></li>
                            <li class="botones3u slide3 apagado pf-off" data-target="#myCarouselCardio2" data-slide-to="2" ><p>Según su causa desencadenante</p></li>
                        </ol>
                    </div>
                    <a class="left carousel-control visible-xs" href="#myCarouselCardio2" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left visi" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control visible-xs" href="#myCarouselCardio2" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
   
            <div class="row info">
                <div class="col-md-12 justificar menos_espacio">
                    <h2>¿Qué tratamientos existen para el dolor?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p class="otroparrafo">
                        El tratamiento del dolor puede ser farmacológico o no farmacológico. En el caso de los tratamientos farmacológicos, le recomendamos 
                        consulte a su médico para que le oriente.
                        Así mismo, puede recurrir a los tratamientos no farmacológicos como lo son: <br>
                        <ul>
                            <li> Programas de ejercicios de rehabilitación o fisioterapia </li>
                            <li>Tratamientos no convencionales como la acupuntura o la bioretroalimentación (forma </br>de control mental sobre el cuerpo que le
                             permite a una persona reducir la sensación </br>de dolor). <br><b>Consulte a su médico.</b>
                            </li>
                        </ul>

                    </p>
                    
                </div>
            </div>

        </div>
    </div>
      <?php include 'footer.php' ?>
   
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>
<script>
  $(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            interval: false
        });
    });
});​
</script>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>

<script>
$(document).ready(function(){

    $( "body" ).on('click', '.pf-off', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on").addClass( "apagado" );

        $(this).removeClass( "pf-off" );
        $(".pf-on").addClass( "pf-off" );
        $(".pf-on").removeClass( "pf-on" );
        $(this).addClass( "pf-on" );    });
});
</script>

</body>
</html>