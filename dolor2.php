<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    
     <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/dolor-banner.jpg">
     <?php include 'mainnav.php' ?>
        <div class="container title">
         <h1 class="heading-interno">DOLOR</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
<div  class="container-fluid white pad-30">
        <div id="vida"></div>
        <div class="container">
            <ol class="breadcrumb">
            <li><a href="dolor.php">Dolor</a></li>
            <li class="active">Dolor Nociceptivo</li>
          </ol>
                <!-- <h1>DOLOR NOCICEPTIVO</h1> -->
            <div class="row info">
                <div class="col-md-6 justificar menos_espacio">
                    <h2>¿Qué es el dolor nociceptivo?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                   <p> El dolor nociceptivo es la respuesta normal y adecuada o fisiológica a un estímulo que causa dolor. Ese estímulo se conduce a 
                    través de los nervios hasta el cerebro donde se elabora una respuesta que el paciente percibe como dolor. De manera más sencilla, el 
                    dolor nociceptivo es la molestia que se siente cuando un estímulo produce daño en los músculos, huesos, piel u órganos internos.
                    </p>
                    <p>
                     El dolor se localiza en el mismo lugar de la lesión y el tipo de dolor se describe como molestia o punzada, latido o entumecimiento. 
                    Si bien puede ser crónico, el dolor nociceptivo tiende a limitarse a medida que el tejido dañado se repara con base en los medicamentos 
                    que el médico indique. Las quemaduras, fracturas, golpes o caídas son ejemplos de dolores nociceptivos.
                </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6">
                  <img class="img-2" src="img/dolor-nociceptivo-interior.jpg">
                </div>
                
            </div>
            <div class="row info">
                <div class="col-md-12">
                    <h2>Síntomas</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p class="parrafo">
                        El dolor que experimenta cada individuo que lo padece es el resultado de una interacción de múltiples variables biológicas, psicológicas, sociales y culturales. El dolor es una percepción que posee varias dimensiones: duración, intensidad, localización, cualidad y afecto.
                    </p>
             <div class="row info hidden-xs">
              <div class="col-md-12">
                <div class="col-md-5">
                  <div class="row"> 
                      <div class= "col-md-6 botonera boton_dolor21 pf-on">
                          <figure>
                            <img src="img/stopwatch.png" width="100px" height="100px" />
                          </figure>
                      </div>
                      <div class= "col-md-6 botonera boton_dolor22 apagado pf-off">
                        <figure>
                          <img src="img/heart288.png" width="100px" height="100px" />
                        </figure>
                      </div>
                  </div>
                  <div class="row">
                    <div class= "col-md-6 botonera boton_dolor23 apagado pf-off">
                      <figure>
                        <img src="img/pin56.png" width="100px" height="100px" />
                      </figure>
                    </div>
                    <div class= "col-md-6 botonera boton_dolor24 apagado pf-off">
                      <figure>
                        <img src="img/medical50.png" width="100px" height="100px" />
                      </figure>
                    </div>
                  </div>
              </div>
              <div class="col-md-7 piel_info info_piel1 justificar menos_espacio">
                <h4>Duración</h4>
                              <p>Es el tiempo durante el cual se percibe el dolor. Este puede ser continuo o intermitente. El dolor agudo es aquel desencadenado por la activación de nociceptores (receptores) en el área de una lesión en los tejidos (ejemplo: traumatismo) y cuya duración es menor de tres meses.
                                    Dolor crónico es el dolor de duración mayor de tres meses, en forma continua o intermitente.
                                </p>
              </div>
              <div class="col-md-7 piel_info info_piel2 justificar menos_espacio">
                    <h4>Intensidad</h4>
                              <p>Es la magnitud del dolor percibido. Si bien es un fenómeno subjetivo, ya que el paciente puede indicar si le duele mucho o
                               poco, existen métodos que permiten objetivarla como las escalas numéricas o la análoga visual. En esta última, la intensidad 
                               del dolor es determinada por el mismo paciente sobre una línea recta cuyos extremos están marcados en cero y diez, siendo cero 
                               la ausencia de dolor y diez el dolor más intenso que el sujeto pueda imaginar.
                                </p> 
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
              </div>
              <div class="col-md-7 piel_info info_piel3 justificar menos_espacio">
                     <h4>Localización</h4>
                              <p>Se refiere al lugar del cuerpo donde el dolor es percibido.
                                </p> 
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-7 piel_info info_piel4 justificar menos_espacio2">
                   <h4>Cualidad</h4>
                                <p>Describe el tipo de dolor que se percibe (quemante, punzante, eléctrico, como calambre, etc.).</p>
                                <p>El dolor nociceptivo somático (piel, músculos, ligamentos, huesos), suele ser localizado, circunscripto, con sensaciones 
                                    claras que el paciente puede precisar. Explicado en general como punzante y agudo. Suele haber un antecedente de un trauma o 
                                    lesión, inflamación o infección. Puede serconstante o intermitente. 

                                </p>
                                <p>Ejemplos de dolor somático son el presente en la 
                                    osteoartritis, artritis reumatoide y dolor de espalda.</p>
                                <p>
                                Por el contrario, el visceral (órganos internos) suele ser difuso, mal localizado, se extiende más allá del órgano y
                                presenta síntomas asociados como náuseas o vómitos.
                            </p>
                   
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
              </div>
            </div>
             <div class="visible-xs">
            <div class="boton-wide boton_dolor21">
              <figure>
                <img src="img/stopwatch.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12 ">
               <h4>Duración</h4>
                              <p>Es el tiempo durante el cual se percibe el dolor. Este puede ser continuo o intermitente. El dolor agudo es aquel desencadenado por la activación de nociceptores (receptores) en el área de una lesión en los tejidos (ejemplo: traumatismo) y cuya duración es menor de tres meses.
                                    Dolor crónico es el dolor de duración mayor de tres meses, en forma continua o intermitente.
                                </p>
                </ul>
              </p>
              </div>
            </div>
            <div class="boton-wide boton_dolor22">
              <figure>
                <img src="img/heart288.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                <h4>Intensidad</h4>
                              <p>Es la magnitud del dolor percibido. Si bien es un fenómeno subjetivo, ya que el paciente puede indicar si le duele mucho o
                               poco, existen métodos que permiten objetivarla como las escalas numéricas o la análoga visual. En esta última, la intensidad 
                               del dolor es determinada por el mismo paciente sobre una línea recta cuyos extremos están marcados en cero y diez, siendo cero 
                               la ausencia de dolor y diez el dolor más intenso que el sujeto pueda imaginar.
                                </p> 
              </div>
            </div>
            <div class="boton-wide boton_dolor23">
              <figure>
                <img src="img/pin56.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                 <h4>Localización</h4>
                              <p>Se refiere al lugar del cuerpo donde el dolor es percibido.
                                </p> 
                    
                
              </div>
            </div>
            <div class="boton-wide boton_dolor24">
              <figure>
                <img src="img/medical50.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                
                                <h4>Cualidad</h4>
                                <p>Describe el tipo de dolor que se percibe (quemante, punzante, eléctrico, como calambre, etc.).</p>
                                <p>El dolor nociceptivo somático (piel, músculos, ligamentos, huesos), suele ser localizado, circunscripto, con sensaciones 
                                    claras que el paciente puede precisar. Explicado en general como punzante y agudo. Suele haber un antecedente de un trauma o 
                                    lesión, inflamación o infección. Puede serconstante o intermitente. Ejemplos de dolor somático son el presente en la 
                                    osteoartritis, artritis reumatoide y dolor de espalda.
                                </p>
                                <p>
                                Por el contrario, el visceral (órganos internos) suele ser difuso, mal localizado, se extiende más allá del órgano y
                                presenta síntomas asociados como náuseas o vómitos.
                            </p>
              </div>
            </div>
            
        </div>  
                       
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row info">
                <div class="col-md-6 justificar menos_espacio">
                    <h2>Diagnóstico y Tratamiento</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    
                        <ul>
                            <li > 
                                <h4>Diagnóstico</h4>
                                <p>El diagnóstico es básicamente clínico, es decir, descripción del dolor actual con sus características y existencia de 
                                    otros síntomas antes mencionados. Como orientación diagnóstica, el médico debe valorar la historia clínica, así como 
                                    los síntomas, estudios y análisis, evaluando si el paciente presenta dolor asociado a una lesión o a una enfermedad a 
                                    la cual se relaciona.</p>
                                <p>Es fundamental evaluar lo más objetivamente posible una variable subjetiva, para lo cual existen escalas validadas para
                                su cuantificación y cuestionarios que miden las distintas dimensiones del dolor.
                                </p>
                             </li>
                            </ul>
                  </div>
                  <div class="col-md-6">
                              </br></br>
                              <li>
                                <h4>Tratamiento</h4>
                                <p>Entre las opciones que existen para tratar el dolor nociceptivo encontramos:</p>
                                <ul>
                                    <li>
                                        <p>El tratamiento farmacológico que consiste en utilizar diversos medicamentos. La intensidad del dolor es fundamental para
                                        determinar el tipo de medicamento que se debe utilizar. Los avances en el tratamiento del dolor se han traducido en una 
                                        amplia gama de opciones seguras y eficaces para el control del mismo. Siempre se debe acudir al médico para obtener un 
                                        diagnóstico oportuno y específico para cada paciente.</p>
                                    </li>
                                    <li>
                                        <p>El tratamiento no farmacológico con programas de fisioterapia, ejercicio y programas educativos y de rehabilitación que 
                                            ayuden al paciente a controlar la sintomatología y reincorporarse a sus tareas habituales. Consulte a su médico.</p>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      </div>

                    </p>
                
                

            </div>

        </div>
    </div>
      <?php include 'footer.php' ?>
   
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>
<script>
     $( ".boton_dolor21" ).on( "click", function() {
         $('.info_piel1').css('display', 'block');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'none');
       
         
         
      });
     $( ".boton_dolor22" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'block');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'none');
        
         
      });
     $( ".boton_dolor23" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'block');
         $('.info_piel4').css('display', 'none');
         
         
      });
    $( ".boton_dolor24" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'none');
         $('.info_piel3').css('display', 'none');
         $('.info_piel4').css('display', 'block');
         
         
      });
   

    </script>
    <script>
  $(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            interval: false
        });
    });
});​
</script>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>

<script>
$(document).ready(function(){

    $('.carousel-indicators li').on('click', function(){

        $('.carousel-indicators li').each(function(){

            $(this).removeClass("apagado");

        });

        $(this).addClass("apagado");
    });

    $( "body" ).on('click', '.pf-off', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on").addClass( "apagado" );

        $(this).removeClass( "pf-off" );
        $(".pf-on").addClass( "pf-off" );
        $(".pf-on").removeClass( "pf-on" );
        $(this).addClass( "pf-on" );

    });
});



</script>

</body>
</html>