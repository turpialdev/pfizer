<div  class="container-fluid padding-vacunas-interno">
      
            
                <!-- <h1>ENFERMEDAD MENINGOCÓCICA</h1> -->
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>enfermedad meningocócica</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> La enfermedad meningocócica es una enfermedad bacteriana grave.
                            </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 afectar-vacunas">
                    <h4>¿De qué manera la enfermedad meningocócica puede afectar a mi hijo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>
                             La enfermedad meningocócica puede ocasionar:
                            <ul>
                                <li>Meningitis</li>
                                <li>Meningococemia (una infección en la sangre)</li>
                                <li>Neumonía</li>
                            </ul>
                            </li>
                            <li>
                                En algunos casos, la enfermedad meningocócica puede ocasionar una pérdida permanente de la audición, lesión cerebral 
                                o pérdida de un miembro.

                            </li>
                            <li>
                                La enfermedad meningocócica, en ocasiones, puede provocar la muerte.
                            </li>
                           
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 riesgo-vacunas">
                    <h4>¿Mi hijo está en riesgo?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li> Todos los niños se encuentran en riesgo. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 transmite-vacunas">
                    <h4>¿Cómo se transmite?</h4>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                        <ul>
                            <li>La enfermedad meningocócica se transmite a través del aire al:
                                <ul>
                                    <li>Estornudar</li>
                                    <li>Toser</li>
                                    <li>Inhalar gotitas infectadas</li>
                                </ul>
                            </li>
                            <li>También se puede transmitir por contacto directo con la persona infectada. </li>
                        </ul>

                    </p>
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>

        </div>
  