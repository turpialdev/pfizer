<!DOCTYPE html>
<html>
     <?php include 'head.php' ?>
    <body>
    <div class="parallax-window smaller-window module" data-parallax="scroll" data-image-src="img/piel-banner.jpg" >
     <?php include 'mainnav.php' ?>
        <div class="container title">
         <h1 class="heading-interno">PIEL Y ARTICULACIONES</h1>
        </div>
    </div>
    <?php include 'sintomasnav.php' ?>
<div  class="container-fluid white pad-30">
        <div id="vida"></div>
        <div class="container">
          <ol class="breadcrumb">
            <li><a href="piel.php">Piel y Articulaciones</a></li>
            <li class="active">Artritis Psoriásica</li>
          </ol>
            
                <!-- <h1>DIFTERIA</h1> -->
            <div class="row info">
                <div class="col-md-6 que-es-vacunas justificar menos_espacio">
                    <h2>¿Qué es la Artritis Psoriásica?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                       El término artritis psoriásica se refiere a un tipo de artritis inflamatoria que aparece en los individuos con psoriasis. 
                       Esta es una enfermedad crónica se caracteriza por dolor y rigidez de las articulaciones, que pueden llegar a agrandarse y 
                       deformarse. 
                    </p>
                    <p>Es una enfermedad autoinmune, es decir, que el sistema inmunológico del cuerpo ataca a sus propias células y tejidos sanos.
                    </p>
                    <p>Se controla debido a que el dolor no es la inquietud más importante en los pacientes con APsO, a menudo la destrucción 
                      articular puede pasar inadvertida, por lo tanto, es importante contar con un diagnóstico y tratamiento tan pronto como sea 
                      posible para ayudar a proteger sus articulaciones y evitar que continúen dañándose.</p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
                <div class="col-md-6">
                    <img class="img-2" src="img/piel-intermedia-psoriasica.jpg" />
                </div>
            </div>
            <div class="row info hidden-xs">
              <div class="col-md-12">
                  <div class="col-md-5">
                     <div class="row">
                        <div class= "col-md-7 botonera boton_piel41 pf-on">
                          <figure>
                            <img src="img/medical51.png" width="100px" height="100px" />
                          </figure>
                        </div>
                        <div class= "col-md-7 botonera boton_piel42 apagado pf-off">
                          <figure>
                            <img src="img/question1.png" width="100px" height="100px" />
                          </figure>
                        </div>
                      </div>
                  </div>
              <div class="col-md-6 piel_info info_piel1">
               <h2>Diagnóstico</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                     <ul>
                       <li>Pruebas Diagnósticas:
                          <ul>
                            <li class="lista-sin-estilo">Varias pruebas empleadas para ayudar a diagnosticar APsO son similares a las herramientas 
                              que emplean los médicos para evaluar 
                              la artritis reumatoide, estas incluyen:
                              <ul>
                                <li>Una prueba de factor reumatoide y una de anticuerpos antinucleares, las cuales pueden ayudar a los médicos a 
                                  descartar otras enfermedades 
                                  como artritis reumatoide.</li>
                                <li>Pruebas de sangre como velocidad de sedimentación globular (VSG) y proteína C reactiva (PCR), las cuales miden 
                                  el grado de inflamación en su cuerpo.</li>
                                <li>Radiografías de las articulaciones afectadas para ayudar a su médico a determinar el grado del daño, si existe, y 
                                  medir el progreso de su enfermedad.</li>
                                <li>Exploración de las articulaciones dolorosas, dedos en forma de salchicha, piel, etc.</li>
                                <li>EI índice de Actividad de la Enfermedad (DAS, en inglés) mide la actividad de la enfermedad en sus articulaciones. 
                                  El DAS facilita a los médicos el diagnóstico de la APsO y evalúa que tan activa está.</li>
                              </ul>
                            </li> 
                          </ul>
                       </li>

                     </ul>
                      
                    </p>
            </div>
            <div class="col-md-6 piel_info info_piel2">
                    <h2>¿Quién puede padecerla?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                      <ul>
                        <li>Casi 30% de las personas con psoriasis pueden desarrollar APsO, por lo tanto, los individuos que padecen o han padecido 
                          psoriasis son quienes tienen mayor tendencia a adquirirla.</li>
                        <li>Del 5% al 10% de los pacientes con psoriasis presentan molestias articulares asociadas con afección en las uñas.</li>
                        <li>La incidencia de APsO es similar entre hombre y mujeres, al igual que en psoriasis.</li>
                      </ul>
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
               </div>
            </div>
             <div class="visible-xs">
            <div class="boton-wide boton_piel41">
              <figure>
                <img src="img/medical51.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
                 <h2>Diagnóstico</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                     <ul>
                       <li>Pruebas Diagnósticas:
                          <ul>
                            <li class="lista-sin-estilo">Varias pruebas empleadas para ayudar a diagnosticar APsO son similares a las herramientas 
                              que emplean los médicos para evaluar 
                              la artritis reumatoide, estas incluyen:
                              <ul>
                                <li>Una prueba de factor reumatoide y una de anticuerpos antinucleares, las cuales pueden ayudar a los médicos a 
                                  descartar otras enfermedades 
                                  como artritis reumatoide.</li>
                                <li>Pruebas de sangre como velocidad de sedimentación globular (VSG) y proteína C reactiva (PCR), las cuales miden 
                                  el grado de inflamación en su cuerpo.</li>
                                <li>Radiografías de las articulaciones afectadas para ayudar a su médico a determinar el grado del daño, si existe, y 
                                  medir el progreso de su enfermedad.</li>
                                <li>Exploración de las articulaciones dolorosas, dedos en forma de salchicha, piel, etc.</li>
                                <li>EI índice de Actividad de la Enfermedad (DAS, en inglés) mide la actividad de la enfermedad en sus articulaciones. 
                                  El DAS facilita a los médicos el diagnóstico de la APsO y evalúa que tan activa está.</li>
                              </ul>
                            </li> 
                          </ul>
                       </li>

                     </ul>
                      
                    </p>
              </div>
            </div>
            <div class="boton-wide boton_piel42">
              <figure>
                <img src="img/question1.png" width="100%" height="100%" />
              </figure>
            </div>
            <div class="container">
              <div class="col-xs-12">
               <h2>¿Quién puede padecerla?</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p>
                      <ul>
                        <li>Casi 30% de las personas con psoriasis pueden desarrollar APsO, por lo tanto, los individuos que padecen o han padecido 
                          psoriasis son quienes tienen mayor tendencia a adquirirla.</li>
                        <li>Del 5% al 10% de los pacientes con psoriasis presentan molestias articulares asociadas con afección en las uñas.</li>
                        <li>La incidencia de APsO es similar entre hombre y mujeres, al igual que en psoriasis.</li>
                      </ul>
                    </p>
              </div>
            </div>
            
            
            </div>
           
            <div class="row ">
                <div class="col-md-12 que-es-vacunas">
                    <h2>Se requiere de un equipo de trabajo</h2>
                    <!-- <h5 class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5> -->
                    <p class="otroparrafo">
                     La asociación de un dermatólogo para los síntomas de su piel con un reumatólogo para los síntomas de sus 
                     articulaciones, es un paso importante en el manejo de su enfermedad. 
                    </p>
                    <p class="otroparrafo">
                      Si usted está recibiendo un DMARDs y sus síntomas no mejoran, es importante comentárselo a su médico tan pronto sea posible.
                    </p>
                    <p class="otroparrafo">
                      Recuerde, sin un tratamiento apropiado, la APsO puede continuar impidiéndole que usted disfrute muchas actividades, se requiere 
                      compromiso de su parte para controlar los síntomas y mejorar su calidad de vida.
                    </p>
                    <p class="otroparrafo">
                      Manteniendo una fluida comunicación con su médico, usted podrá colaborar en el desarrollo de un plan de tratamiento que lo ayude 
                      a alcanzar sus metas terapéuticas y a volver a realizar las actividades que disfruta.
                    </p>
                    
                   <!-- <a href="#" target="_blank" class="button ">Ver más</a> -->
                </div>
            </div>
        </div>
    </div>
        <?php include 'footer.php' ?>
   
    <script type="text/javascript">
        function init() {
         window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("nav");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();
$('.dropdown-toggle').dropdown();
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });
});
</script>
<script>
     $( ".boton_piel41" ).on( "click", function() {
         $('.info_piel1').css('display', 'block');
         $('.info_piel2').css('display', 'none');
        
       
         
         
      });
     $( ".boton_piel42" ).on( "click", function() {
         $('.info_piel1').css('display', 'none');
         $('.info_piel2').css('display', 'block');
      
        
         
      });
    

    </script>
    <script>
  $(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            interval: false
        });
    });
});​
</script>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>

<script>
$(document).ready(function(){

    $('.carousel-indicators li').on('click', function(){

        $('.carousel-indicators li').each(function(){

            $(this).removeClass("apagado");

        });

        $(this).addClass("apagado");
    });

    $( "body" ).on('click', '.pf-off', function(e) {

        $(this).removeClass( "apagado" );
        $(".pf-on").addClass( "apagado" );

        $(this).removeClass( "pf-off" );
        $(".pf-on").addClass( "pf-off" );
        $(".pf-on").removeClass( "pf-on" );
        $(this).addClass( "pf-on" );

    });
});



</script>
</body>
</html>